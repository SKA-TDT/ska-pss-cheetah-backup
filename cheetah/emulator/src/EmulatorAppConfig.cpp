/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/emulator/EmulatorAppConfig.h"
#include "cheetah/version.h"


namespace ska {
namespace cheetah {
namespace emulator {


EmulatorAppConfig::EmulatorAppConfig(std::string const& app_name, std::string const& description)
    : BasicAppConfig(app_name, description)
    , _stream_type(Stream::SkaMid)
{
    add(_emulator_config);
}

EmulatorAppConfig::~EmulatorAppConfig()
{
}

ska::cheetah::emulator::Config& EmulatorAppConfig::emulator_config()
{
    return _emulator_config;
}

std::string EmulatorAppConfig::version() const
{
    return std::string(cheetah::version) + "\n" + BasicAppConfig::version();
}

void EmulatorAppConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("stream", boost::program_options::value<std::string>()->default_value("ska_mid")->notifier([&](std::string const& value)
                                                                                                {
                                                                                                    if(value == "ska_mid") {
                                                                                                        _stream_type=Stream::SkaMid;
                                                                                                    }
                                                                                                    else if(value == "ska_low") {
                                                                                                        _stream_type=Stream::SkaLow;
                                                                                                    }
                                                                                                    else {
                                                                                                        throw panda::Error("unknown stream type");
                                                                                                    }
                                                                                                }
                                                                                               ), "type of UDP stream to generate ska_low or ska_mid")
    ("list-generators", boost::program_options::bool_switch()->notifier([this](bool b)
                        {
                            if(b) {
                                for(auto const& gen : _generator_keys) {
                                    std::cout << "\t" << gen << "\n";
                                }
                            }
                            return false;
                        }), "display a list of generators available");
}

void EmulatorAppConfig::set_generator_list(std::vector<std::string> const& generator_names)
{
    _generator_keys = generator_names;
}

std::vector<std::string> const& EmulatorAppConfig::generator_list() const
{
    return _generator_keys;
}

EmulatorAppConfig::Stream EmulatorAppConfig::stream_type() const
{
    return _stream_type;
}

} // namespace emulator
} // namespace cheetah
} // namespace ska
