/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/** @addtogroup apps
 * @{
 * @section app_emulator UDP packet emulation
 * @brief cheetah_emulator A stand alone UDP packet stream generator
 * @details
 *    Will generate a continuous stream of generated data (many options available)
 *    that are streamed over UDP.
 *
 *    for help on how to use see:
 *    @verbatim
       cheetah_emulator --help
      @endverbatim
 *
 * @} */ // end group

#include "cheetah/rcpt_low/PacketGenerator.h"
#include "cheetah/rcpt/PacketGenerator.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/emulator/EmulatorAppConfig.h"
#include "cheetah/emulator/Factory.h"
#include "cheetah/emulator/Config.h"
#include "panda/BasicAppConfig.h"
#include "panda/Log.h"
#include <memory>
#include <ostream>

template<typename PacketGeneratorType>
class EmulatorLauncher
{
        typedef typename PacketGeneratorType::SampleDataType NumericalRep;
        typedef ska::cheetah::emulator::Factory<PacketGeneratorType, NumericalRep> FactoryType;

    public:
        template<typename EmulatorConfig, typename StreamConfig>
        inline static
        int run(EmulatorConfig const& config, StreamConfig&& stream_config) {
            ska::cheetah::generators::GeneratorFactory<NumericalRep> generator_factory(config.generators_config());
            FactoryType factory(config, generator_factory);
            std::unique_ptr<typename FactoryType::EmulatorType> emulator(factory.create(config.generator(), std::forward<StreamConfig>(stream_config)));
            PANDA_LOG << "emulator using generator: '" << config.generator() << "'";

            // start the emulator
            return emulator->run();
        }
};

int main(int argc, char** argv)
{
    ska::cheetah::emulator::EmulatorAppConfig app_config("cheetah_emulator", "UDP emulation for cheetah Beamformed Time-Frequecy data");
    int rv;
    try {

        ska::cheetah::emulator::Config& config = app_config.emulator_config();
        ska::cheetah::generators::GeneratorFactory<int8_t> generator_factory(config.generators_config());
        app_config.set_generator_list(generator_factory.available());

        // parse command line options
        if( (rv=app_config.parse(argc, argv)) ) return rv;

        // setup the emulator
        auto stream_type=app_config.stream_type();
        typedef ska::cheetah::emulator::EmulatorAppConfig::Stream Stream;
        if(stream_type == Stream::SkaLow)
        {
            typedef ska::cheetah::rcpt_low::PacketGenerator<ska::cheetah::generators::TimeFrequencyGenerator<int8_t>> StreamType;
            rv = EmulatorLauncher<StreamType>::run(config, config.ska_low_config());
        }
        if(stream_type == Stream::SkaMid)
        {
            typedef ska::cheetah::rcpt::PacketGenerator<ska::cheetah::generators::TimeFrequencyGenerator<uint8_t>> StreamType;
            rv = EmulatorLauncher<StreamType>::run(config, ska::cheetah::data::DimensionSize<ska::cheetah::data::Frequency>(config.number_of_channels()));
        }

        return rv;
    }
    catch(std::exception const& e) {
        std::cerr << "Emulator: " << e.what() << std::endl;;
    }
    catch(...) {
        std::cerr << "Emulator: unknown exception caught" << std::endl;
    }
    return 1;
}
