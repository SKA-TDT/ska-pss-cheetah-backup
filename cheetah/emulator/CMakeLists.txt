set(module_emulator_lib_src_cpu
    src/Config.cpp
    src/EmulatorAppConfig.cpp
    PARENT_SCOPE
)

add_executable(cheetah_emulator src/emulator_main.cpp)
install(TARGETS cheetah_emulator DESTINATION ${BINARY_INSTALL_DIR})
target_link_libraries(cheetah_emulator ${CHEETAH_LIBRARIES})
TEST_UTILS()

add_subdirectory(test)
