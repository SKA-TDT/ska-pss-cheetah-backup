include_directories(${GTEST_INCLUDE_DIR})

link_directories(${GTEST_LIBRARY_DIR})

set(
   gtest_brdz_src
    src/gtest_brdz.cpp
)
add_executable(gtest_brdz ${gtest_brdz_src} )
target_link_libraries(gtest_brdz ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_brdz gtest_brdz --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
