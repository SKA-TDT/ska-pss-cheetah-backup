/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_BRDZ_CONFIG_H
#define SKA_CHEETAH_BRDZ_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/brdz/cuda/Config.h"
#include "cheetah/data/Birdie.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace brdz {

/**
 * @brief
 *   Configuration for the BRDZ module
 *
 */

class Config : public cheetah::utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief      Configuration for cuda implementation of Brdz
         *
         * @return     cuda::Brdz configuration
         */
         cuda::Config const& cuda_config() const;

        /**
         * @brief      Get the birdie list
         *
         * @return     list of birdie frequencies and widths
         */
         std::vector<data::Birdie> const& birdies() const;

        /**
         * @brief      Set the birdie list via a move
         *
         * @param[in]  birds  Container of data::Birdie instances
         *
         * @tparam     Container The type of container (must have value type of data::Birdie)
         */
        template <typename Container>
        void birdies(Container&& birds);

        /**
         * @brief      Set the birdie list via a copy
         *
         * @param      birds          Container of data::Birdie instances
         *
         * @tparam     Container  The type of container (must have value type of data::Birdie)
         */
        template <typename Container>
        void birdies(Container const& birds);

        /**
         * @brief      Set the birdie list from an iterator
         *
         * @param[in]  beg   The start iterator
         * @param[in]  end   The end iterator
         *
         * @tparam     Iterator  The type of iterator (must have value type of data::Birdie)
         */
        template <typename Iterator>
        void birdies(Iterator beg, Iterator end);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        cuda::Config _cuda_config;
        std::vector<data::Birdie> _birdies;
    };

} // namespace brdz
} // namespace cheetah
} // namespace ska

#include "cheetah/brdz/detail/Config.cpp"

#endif // SKA_CHEETAH_BRDZ_CONFIG_H
