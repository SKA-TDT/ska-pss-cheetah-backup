#ifndef SKA_CHEETAH_TDAS_CUDA_TDAS_H
#define SKA_CHEETAH_TDAS_CUDA_TDAS_H

#include "cheetah/tdas/cuda/Config.h"
#include "cheetah/tdas/Config.h"
#include "cheetah/tdrt/Tdrt.h"
#include "cheetah/tdao/Tdao.h"
#include "cheetah/pwft/Pwft.h"
#include "cheetah/hrms/Hrms.h"
#include "cheetah/fft/Fft.h"
#include "cheetah/brdz/Brdz.h"
#include "cheetah/dred/Dred.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/detail/DmTimeSlice.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace tdas {
namespace cuda {

/**
 * @brief    CUDA/Thrust implementation of the Tdas module
 *
 * @details  The Tdas module supports time domain acceleration search.
 *
 * @tparam     T     The internal value type to use for processing (float or double)
 */
template <typename T>
class Tdas
    : public utils::AlgorithmBase<Config, tdas::Config>
{
    private:
        typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
        typedef data::DmTime<DmTrialsType> DmTimeType;
        typedef data::detail::DmTimeSlice<DmTimeType> DmTimeSliceType;

    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability; // minimum device requirements

    public:

        /**
         * @brief      Construct a new Tdas instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Tdas(Config const& config, tdas::Config const& algo_config);
        Tdas(Tdas const&) = delete;
        Tdas(Tdas&&) = default;
        ~Tdas();

        /**
         * @brief      Search a DmTimeSlice for significant periodic signals
         *             at a range of accelerations
         *
         * @param      gpu   The gpu to process on
         * @param      data  The DmTimeSlice to search
         *
         * @return     A shared pointer to a list of candidate signals
         */
        std::shared_ptr<data::Ccl> process(panda::PoolResource<cheetah::Cuda>& gpu,
            DmTimeSliceType const& data);

        /**
         * @brief      Version of process method for async calls
         */
        std::shared_ptr<data::Ccl> operator()(panda::PoolResource<cheetah::Cuda>& gpu,
            std::shared_ptr<DmTimeSliceType> const& data);
};


} // namespace cuda
} // namespace tdas
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_TDAS_CUDA_TDAS_H
