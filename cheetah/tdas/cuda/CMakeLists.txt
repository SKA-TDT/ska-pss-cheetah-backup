set(module_cuda_lib_src_cpu
    src/Config.cpp
    PARENT_SCOPE
    )

set(module_cuda_lib_src_cuda
  src/Tdas.cu
  PARENT_SCOPE
  )


add_subdirectory(test)
