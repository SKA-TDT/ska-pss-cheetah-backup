/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_SIMPLESIFT_SIFT_H
#define SKA_CHEETAH_SIFT_SIMPLESIFT_SIFT_H

#include "cheetah/sift/simple_sift/Config.h"
#include "cheetah/sift/Config.h"
#include "cheetah/data/Candidate.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Scl.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace sift {
namespace simple_sift {

/**
 * @brief    simple_sift algorithm for the Sift module
 */
class Sift
{
    public:
        typedef cheetah::Cpu Architecture;
        typedef cheetah::Cpu ArchitectureCapability;
        typedef simple_sift::Config Config;

        typedef panda::PoolResource<Architecture> ResourceType;

    public:

        /**
         * @brief      Construct a new Sift instance
         *
         * @param      config       The implementation configuration
         * @param      algo_config  The algorithm configuration
         */
        Sift(sift::Config const& config);
        Sift(Sift const&) = delete;
        Sift(Sift&&) = default;

        /**
         * @brief      Sift a candidate list to aggregate like signals.
         *
         * @param      cpu   Use the cpu for processing
         * @param      data  The Ccl list to sift
         *
         * @return     A shared pointer to a list of sifted candidate signals
         */
        std::shared_ptr<data::Scl> operator()(ResourceType& cpu, data::Ccl& ccl) const;
        std::shared_ptr<data::Scl> operator()(ResourceType& cpu, std::shared_ptr<data::Ccl> const& ccl) const;

    private:
        Config const& _config;

};

} //simple_sift
} //sift
} //cheetah
} //ska

#include "cheetah/sift/simple_sift/detail/Sift.cpp"

#endif //SKA_CHEETAH_SIFT_SIMPLESIFT_SIFT_H
