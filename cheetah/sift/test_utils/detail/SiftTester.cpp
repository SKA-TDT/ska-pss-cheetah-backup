/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sift/test_utils/SiftTester.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Scl.h"
#include "cheetah/utils/Architectures.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace sift {
namespace test {

template<typename SiftAlgo>
void SiftTesterTraits<SiftAlgo>::SiftHandler::operator()(std::shared_ptr<data::Scl> data)
{
    ASSERT_TRUE(data.get() != nullptr);
    _data = data;
    static_cast<BaseT&>(*this)();
}

template<typename SiftAlgo>
typename SiftTesterTraits<SiftAlgo>::Api& SiftTesterTraits<SiftAlgo>::api(PoolType& pool)
{
    if(!_api)
    {
        _config.pool(pool);
        _config.template config<typename SiftAlgo::Config>().active(true);
        _api.reset(new Api(_config, _handler));
    }
    return *_api;
}

template<typename SiftAlgo>
typename SiftTesterTraits<SiftAlgo>::SiftHandler& SiftTesterTraits<SiftAlgo>::handler()
{
    return _handler;
}

template<typename SiftAlgo>
typename SiftTesterTraits<SiftAlgo>::TestConfig& SiftTesterTraits<SiftAlgo>::config()
{
    return _config;
}

template <typename SiftAlgo>
struct ExecutionTest
{
    inline static void test(SiftAlgo& api)
    {
        data::Ccl::CandidateType::MsecTimeType candidate1_period_val(0.050 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate1_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate1_dm_val = 5 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate1_width_val(0.001 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate1(candidate1_period_val, candidate1_pdot_val, candidate1_dm_val, candidate1_width_val, 10., 1);

        data::Ccl::CandidateType::MsecTimeType candidate1_harm_period_val(0.100 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate1_harm_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate1_harm_dm_val = 5 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate1_harm_width_val(0.001 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate1_harm(candidate1_harm_period_val, candidate1_harm_pdot_val, candidate1_harm_dm_val, candidate1_harm_width_val, 5., 2);

        data::Ccl::CandidateType::MsecTimeType candidate2_period_val(0.033 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate2_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate2_dm_val = 95 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate2_width_val(0.0005 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate2(candidate2_period_val, candidate2_pdot_val, candidate2_dm_val, candidate2_width_val, 100., 3);

        data::Ccl::CandidateType::MsecTimeType candidate2_harm_period_val(0.231 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate2_harm_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate2_harm_dm_val = 95 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate2_harm_width_val(0.0005 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate2_harm(candidate2_harm_period_val, candidate2_harm_pdot_val, candidate2_harm_dm_val, candidate2_harm_width_val, 10., 4);

        data::Ccl::CandidateType::MsecTimeType candidate3_period_val(0.027467 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate3_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate3_dm_val = 29 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate3_width_val(0.00005 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate3(candidate3_period_val, candidate3_pdot_val, candidate3_dm_val, candidate3_width_val, 12., 5);

        data::Ccl::CandidateType::MsecTimeType candidate4_period_val(0.10982 * boost::units::si::seconds);
        data::Ccl::CandidateType::SecPerSecType candidate4_pdot_val = 0.00000000000001;
        data::Ccl::CandidateType::Dm candidate4_dm_val = 95 * data::parsecs_per_cube_cm;
        data::Ccl::CandidateType::MsecTimeType candidate4_width_val(0.0009 * boost::units::si::seconds);
        data::Ccl::CandidateType candidate4(candidate4_period_val, candidate4_pdot_val, candidate4_dm_val, candidate4_width_val, 7., 6);

        std::shared_ptr<data::Ccl> ccl(new data::Ccl());;
        ccl->push_back(candidate1);
        ccl->push_back(candidate1_harm);
        ccl->push_back(candidate2);
        ccl->push_back(candidate2_harm);
        ccl->push_back(candidate3);
        ccl->push_back(candidate4);

        api(ccl);


//        ASSERT_EQ((*scl)[0].period(), ccl[0].period());
//        ASSERT_EQ((*scl)[1].period(), ccl[1].period());
//        ASSERT_EQ((*scl)[2].period(), ccl[2].period());
//        ASSERT_EQ((*scl)[3].period(), ccl[3].period());

    }
};

template <typename TestTraits>
SiftTester<TestTraits>::SiftTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
SiftTester<TestTraits>::~SiftTester()
{
}

template<typename TestTraits>
void SiftTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void SiftTester<TestTraits>::TearDown()
{
}

POOL_ALGORITHM_TYPED_TEST_P(SiftTester, test_execution)
{
    TypeParam traits;
    ExecutionTest<typename TypeParam::Api>::test(traits.api(pool));
    ASSERT_TRUE(traits.handler().wait(std::chrono::seconds(5))) << "timed out waiting for handler to be called";

    auto const& scl = *traits.handler().data();
    ASSERT_EQ(scl.size(), std::size_t(4));
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(SiftTester, test_execution);

} // namespace test
} // namespace sift
} // namespace cheetah
} // namespace ska
