/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_GENERATORS_TIMEFREQUENCYGENERATOR_H
#define SKA_CHEETAH_GENERATORS_TIMEFREQUENCYGENERATOR_H

#include "cheetah/data/TimeFrequency.h"

namespace ska {
namespace cheetah {
namespace generators {

/**
 * @brief
 *
 * @details
 *
 */

template<typename T>
class TimeFrequencyGenerator
{
    public:
        typedef typename data::TimeFrequency<Cpu, T>::FrequencyType FrequencyType;
        typedef typename data::TimeFrequency<Cpu, T>::TimeType TimeType;
        typedef typename data::TimeFrequency<Cpu, T>::TimePointType TimePointType;
        typedef data::TimeFrequency<Cpu, T> DataType;

    public:
        TimeFrequencyGenerator();
        virtual ~TimeFrequencyGenerator() = 0;
        virtual void next(DataType& data) = 0;

};


} // namespace generators
} // namespace cheetah
} // namespace ska
#include "detail/TimeFrequencyGenerator.cpp"

#endif // SKA_CHEETAH_GENERATORS_TIMEFREQUENCYGENERATOR_H
