/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
#define SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
#include "cheetah/pipeline/SinglePulseFactory.h"
#include "cheetah/pipeline/Dedispersion.h"
#include "cheetah/pipeline/AccelerationSearchAlgoConfig.h"
#include "cheetah/pipeline/detail/FldoHandler.h"
#include "cheetah/psbc/Psbc.h"
#include "cheetah/sift/Sift.h"
#include "cheetah/fldo/Fldo.h"
#include <panda/DataSwitch.h>
#include <memory>

namespace ska {
namespace cheetah {
namespace pipeline {

/**
 * @class AccelerationSearch
 * @brief
 *    The acceleration search pipeline
 *
 * @details
 * TODO description/ascii art description of the pipeline
 *
 * Define an AccelerationSearchTraits class to customise the AccelerationPipeline.
 *
 * #Traits Requirements
 *
 * ##The Dedispersion Pipeline
 *   The traits class must provide the following static method in order to generate a suitable dedisperion pipeline.
 *   @code
 *   static
 *   Dedispersion<NumericalT>* create_dedispersion_pipeline( CheetahConfig<NumericalT> const& cheetah_config
 *                                                        , BeamConfig<NumericalT> const& beam_config
 *                                                        , DmHandlerT dm_handler
 *                                                        )
 *  @endcode
 *  A default method using the cheetah::SinglePulse pipeline is available by inheriting from the DefaultAccelerationSearchTraits class.
 *
 * ##The Acceleration Search Pipeline
 * The AccelerationSearchTraits class must provide a static method to create the acceleration search part of the pipeline (i.e post-Dedispersion).
 * This method will return a data type based on whether you are configuring it for a time-domain (Tdas) or frequency-domain (Fdas) acceleration search.
 * The AccelerationSearchAlgoConfig provides acces to both the Fdas and Tdas configs.
 * The templated PostAccelerationSearchHandler is the handler for the Sift module, which is called upon completion of the acceleration search pipeline.
 * @code
 * template<typename PostAccelerationSearchHandler>
 * static
 * MyAlgoType* create_acceleration_search_algo(AccelerationSearchAlgoConfig const& configuration, PostAccelerationSearchHandler const& handler);
 * @endcode
 *
 * #Traits optional methods:
 *
 * ##The FLDO handler
 *    Post folding (fldo) processing. By default fldo output is passed to the pipelines data export module under the channel name "ocld".
 *    An alternative fldo handler can be specified by adding a static
 *    @code
 *         NewHandlerType* create_fldo_handler(DataExport<NumericT>&, CheetahConfig<NumericalT> const&, BeamConfig<NumericalT> const&);
 *    @endcode
 *    to the AccelerationSearchTraits class.
 */

template<typename NumericalT>
struct DefaultAccelerationSearchTraits
{
    /**
     * @brief Default dedispersion is the cheetsh SinglePulse pipeline
     */
    template<typename DmHandlerT>
    static inline
    Dedispersion<NumericalT>* create_dedispersion_pipeline( CheetahConfig<NumericalT> const& cheetah_config
                                                          , BeamConfig<NumericalT> const& beam_config
                                                          , DmHandlerT dm_handler
                                                          )
    {
        SinglePulseFactory<NumericalT> sp_factory(cheetah_config);
        return sp_factory.create(beam_config, dm_handler);
    }
};

template<typename NumericalT, typename AccelerationSearchTraitsT>
class AccelerationSearch : public PipelineHandler<NumericalT>
{
    private:
        typedef PipelineHandler<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;
        typedef typename panda::is_pointer_wrapper<typename Dedispersion<NumericalT>::DmTrialType>::type DmTrialsType;

    public:
        typedef AccelerationSearchTraitsT Traits;
        typedef NumericalT value_type;

    public:
        AccelerationSearch(CheetahConfig<NumericalT> const& config, BeamConfig<NumericalT> const&);
        virtual ~AccelerationSearch();

        /**
         * @brief called whenever data is available for processing
         */
        void operator()(TimeFrequencyType&) override;

    public:
        /**
         * @brief  template for testing if type T has a create_fldo_handler function
         * @details  Use with panda::HasMethod. e.g.
         * @code
         *    static bool has_method = panda::HasMethod<T, has_create_fldo_handler_t>::value;
         * @endcode
         */
        template<typename T>
        using has_create_fldo_handler_t = decltype(std::declval<T&>().create_fldo_handler(std::declval<DataExport<value_type>&>()
                                                                                        , std::declval<CheetahConfig<NumericalT> const&>()
                                                                                        , std::declval<BeamConfig<NumericalT> const&>()));
    private:
        typedef detail::FldoHandler<AccelerationSearch<NumericalT, AccelerationSearchTraitsT>> FldoHandler;
        friend FldoHandler;

        struct SiftHandler {
            public:
                SiftHandler(AccelerationSearch&);
                void operator()(std::shared_ptr<data::Scl>) const;

            private:
                AccelerationSearch& _pipeline;
        };

        typedef sift::Sift<SiftHandler> SiftType;
        typedef typename std::remove_pointer<decltype(std::declval<AccelerationSearchTraitsT>().create_acceleration_search_algo(
                                                                                                    std::declval<AccelerationSearchAlgoConfig const&>()
                                                                                                  , std::declval<SiftType&>()
                                                                                                )
                                                     )>::type AccelerationSearchAlgoType;
    public:
        /**
         * @brief access to the dedispersion pipeline object
         */
        Dedispersion<NumericalT> const& dedispersion_pipeline() const;

        /**
         * @brief access the fldo handler object
         */
        typename FldoHandler::FldoHandlerType const& fldo_handler() const;

        /**
         * @brief access the acceleration_search pipeline object
         */
        AccelerationSearchAlgoType const& acceleration_search_pipeline() const;

    private:
        CheetahConfig<NumericalT> const& _config;
        BeamConfig<NumericalT> const& _beam_config;
        FldoHandler _fldo_handler;
        fldo::Fldo<FldoHandler, NumericalT> _fldo;
        SiftHandler _sift_handler;
        SiftType _sift;
        std::unique_ptr<AccelerationSearchAlgoType> _acceleration_search;
        psbc::Psbc<typename decltype(_acceleration_search)::element_type> _psbc;
        ska::panda::DataSwitch<DmTrialsType> _dm_switch; // need to be able to switch pipelines on/off at runtime

        std::unique_ptr<Dedispersion<NumericalT>> _dedisperser;
        std::vector<std::shared_ptr<TimeFrequencyType>> _tf_data;

};

} // namespace pipeline
} // namespace cheetah
} // namespace ska
#include "detail/AccelerationSearch.cpp"

#endif // SKA_CHEETAH_PIPELINE_ACCELERATIONSEARCH_H
