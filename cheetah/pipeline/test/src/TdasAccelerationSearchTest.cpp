/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/test/TdasAccelerationSearchTest.h"
#include "cheetah/pipeline/AccelerationSearch.h"
#include "cheetah/pipeline/TdasAccelerationSearchTraits.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include <memory>
#include <chrono>
#include <thread>

namespace ska {
namespace cheetah {
namespace pipeline {
namespace test {

TdasAccelerationSearchTest::TdasAccelerationSearchTest()
    : ::testing::Test()
{
}

TdasAccelerationSearchTest::~TdasAccelerationSearchTest()
{
}

void TdasAccelerationSearchTest::SetUp()
{
}

void TdasAccelerationSearchTest::TearDown()
{
}

template<typename NumericalT>
struct TdasAccelerationSearchTests
{
    static void run()
    {
        // Search ~600 seconds of data at standard resoluton
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef typename TimeFrequencyType::DataType DataType;


        // Configure components
        CheetahConfig<NumericalT> config;

        // Configure high-density low DMs
        ddtr::DedispersionConfig dd_config_low;
        config.sps_config().dedispersion_config(dd_config_low);
        dd_config_low.dm_start(ddtr::DedispersionConfig::Dm(0.0 * data::parsecs_per_cube_cm));
        dd_config_low.dm_end(ddtr::DedispersionConfig::Dm(100.0 * data::parsecs_per_cube_cm));
        dd_config_low.dm_step(ddtr::DedispersionConfig::Dm(0.1 * data::parsecs_per_cube_cm));

/*
        // Configure low-density high DMs
        ddtr::DedispersionConfig dd_config_high;
        config.sps_config().dedispersion_config(dd_config_high);
        dd_config_high.dm_start(ddtr::DedispersionConfig::Dm(200.0 * data::parsecs_per_cube_cm));
        dd_config_high.dm_end(ddtr::DedispersionConfig::Dm(300.0 * data::parsecs_per_cube_cm));
        dd_config_high.dm_step(ddtr::DedispersionConfig::Dm(1.0 * data::parsecs_per_cube_cm));
*/

        // Set sps priority
        config.sps_config().set_priority(0);

        // Set the size of the dedispersion buffer
        config.sps_config().set_dedispersion_samples(1<<18);

        // Set the psbc config
        config.psbc_config().dump_time(1 * data::seconds);

        // Set tdas priority
        config.acceleration_search_config().tdas_config().set_priority(1);

        // Set up tdas general parameters
        config.acceleration_search_config().tdas_config().minimum_size(1<<19);
        config.acceleration_search_config().tdas_config().size(1<<23);
        config.acceleration_search_config().tdas_config().number_of_harmonic_sums(4);
        config.acceleration_search_config().tdas_config().dm_trials_per_task(200);

        // Set up CUDA implementation specific configuration arguments
        auto& cuda_algo_config = config.acceleration_search_config().tdas_config().cuda_config();
        cuda_algo_config.tdao_config().significance_threshold(7.0);
        cuda_algo_config.tdao_config().minimum_frequency(0.1 * data::hz);
        cuda_algo_config.tdao_config().maximum_frequency(2000.0 * data::hz);
        cuda_algo_config.dred_config().oversmoothing_factor(5);
        // Birdies configuration would go here

        // Set up acceleration parameters
        auto& acc_config = config.acceleration_search_config().tdas_config().acceleration_list_generator();
        BeamConfig<NumericalT> beam_config;
        acc_config.acc_lo(-10.0);
        acc_config.acc_hi(10.0);
        acc_config.tolerance(1.25);

        // select the sift algo
        config.sift_config().template config<sift::simple_sift::Config>().active(true);

        // select the fldo algo
        config.fldo_config().template config<fldo::cuda::Config>().active(true);

        typedef AccelerationSearch<NumericalT, TdasAccelerationSearchTraits<NumericalT>> AccelerationSearchType;
        AccelerationSearchType pipeline(config, beam_config);

        // Set up noise parameters for data to be passed through the pipeline
        generators::GaussianNoiseConfig noise_config;
        generators::GaussianNoise<DataType> noise(noise_config);
        noise_config.mean(96.0);
        noise_config.std_deviation(10.0);

        // Start epoch
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(50000.0));

        double tsamp_us = 64.0;
        double f_low = 1.2;
        double f_high = 1.8;
        std::size_t total_nsamps = std::size_t(1.0 / (tsamp_us * 1e-6));
        data::DimensionSize<data::Time> number_of_samples(1000);
        data::DimensionSize<data::Frequency> number_of_channels(1024);
        std::size_t loop_count = total_nsamps/number_of_samples + 10;
        for (std::size_t ii=0; ii<loop_count; ++ii)
        {
            auto tf = std::make_shared<TimeFrequencyType>(number_of_samples, number_of_channels);
            auto f1 =  typename TimeFrequencyType::FrequencyType(f_high * boost::units::si::giga * boost::units::si::hertz);
            auto f2 =  typename TimeFrequencyType::FrequencyType(f_low * boost::units::si::giga * boost::units::si::hertz);
            auto delta = (f2 - f1)/ (double)number_of_channels;
            tf->set_channel_frequencies_const_width( f1, delta );
            tf->sample_interval(typename TimeFrequencyType::TimeType(tsamp_us * boost::units::si::micro * data::seconds));
            tf->start_time(epoch);
            epoch += std::chrono::duration<double>(tf->sample_interval().value()*number_of_samples);
            noise.next(*tf);
            pipeline(*tf);
        }

    }
};

TEST_F(TdasAccelerationSearchTest, run_tdas_test_uint8)
{
    TdasAccelerationSearchTests<uint8_t>::run();
}

/*
TEST_F(TdasAccelerationSearchTest, run_tdas_test_uint16)
{
    TdasAccelerationSearchTests<uint16_t>::run();
}
*/

} // namespace test
} // namespace pipeline
} // namespace cheetah
} // namespace ska
