/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/test/PipelineHandlerFactoryTest.h"
#include "cheetah/pipeline/PipelineHandlerFactory.h"
#include "cheetah/pipeline/CheetahConfig.h"
#include "cheetah/pipeline/BeamConfig.h"

namespace ska {
namespace cheetah {
namespace pipeline {
namespace test {


PipelineHandlerFactoryTest::PipelineHandlerFactoryTest()
    : ::testing::Test()
{
}

PipelineHandlerFactoryTest::~PipelineHandlerFactoryTest()
{
}

void PipelineHandlerFactoryTest::SetUp()
{
}

void PipelineHandlerFactoryTest::TearDown()
{
}

TEST_F(PipelineHandlerFactoryTest, test_create)
{
    pipeline::CheetahConfig<uint8_t> config;
    BeamConfig<uint8_t> beam_config;
    pipeline::PipelineHandlerFactory factory(config);
    for(auto const& handler : factory.available() ) {
        std::cout << "testing handler '" << handler << "'" << std::endl;
        try {
            auto type = factory.create(handler, beam_config);
            ASSERT_NE( nullptr, type ) << handler;
            delete type;
        } catch (panda::Error) {}

        std::cout << "testing timed handler '" << handler << "'" << std::endl;
        try {
            auto timed_type = factory.create_timed(handler, beam_config);
            ASSERT_NE( nullptr, timed_type ) << handler;
            delete timed_type;
        } catch (panda::Error) {}
    }
}

TEST_F(PipelineHandlerFactoryTest, test_tdas_config)
{
    pipeline::CheetahConfig<uint8_t> config;
    BeamConfig<uint8_t> beam_config;
    pipeline::PipelineHandlerFactory factory(config);

    ASSERT_THROW(factory.create("Tdas", beam_config), panda::Error);

    // select the sift algo
    config.sift_config().template config<sift::simple_sift::Config>().active(true);
    // select the fldo algo
    config.fldo_config().template config<fldo::cuda::Config>().active(true);

    pipeline::PipelineHandlerFactory newFactory(config);

    ASSERT_NO_THROW(auto type = newFactory.create("Tdas", beam_config); delete type);
}

} // namespace test
} // namespace pipeline
} // namespace cheetah
} // namespace ska
