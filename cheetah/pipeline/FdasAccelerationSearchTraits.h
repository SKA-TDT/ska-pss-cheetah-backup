/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_FDASACCELERATIONSEARCHTRAITS_H
#define SKA_CHEETAH_PIPELINE_FDASACCELERATIONSEARCHTRAITS_H
#include "cheetah/pipeline/AccelerationSearch.h"
#include "cheetah/pipeline/AccelerationSearchAlgoConfig.h"
#include "cheetah/fdas/Fdas.h"

namespace ska {
namespace cheetah {
namespace pipeline {

/**
 * @brief
 *    AccelrationSearchAlgoTraits to create a frequency domain accelerated Search
 *
 */

template<typename NumericalT, typename SiftHandlerT>
struct FdasAccelerationSearchTraits : public DefaultAccelerationSearchTraits<NumericalT>
{

    static inline
    fdas::Fdas<SiftHandlerT>* create_acceleration_search_algo(AccelerationSearchAlgoConfig const& acceleration_search_config, SiftHandlerT& sift_handler)
    {
        return new fdas::Fdas<SiftHandlerT>(acceleration_search_config.fdas_config(), sift_handler);
    }


};

} // namespace pipeline
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_PIPELINE_FDASACCELERATIONSEARCHTRAITS_H
