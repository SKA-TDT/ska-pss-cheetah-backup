/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_FLDOHANDLER_H
#define SKA_CHEETAH_PIPELINE_FLDOHANDLER_H

#include <panda/TypeTraits.h>
#include <memory>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace pipeline {
namespace detail {

/*
 * @brief Wrapper class for managing thr fldo handler used in AccelerationSearch pipeline
 * @details Implementation specific to AccelerationSearch class
 */
template<typename AccelerationSearchType, typename Enable=void>
struct FldoHandler {
    public:
        typedef DataExport<typename AccelerationSearchType::value_type> FldoHandlerType;

    public:
        FldoHandler(AccelerationSearchType& pipeline)
            : _out(pipeline.out())
        {
        }

        void operator()(std::shared_ptr<data::Ocld> const& data)
        {
            _out.send(panda::ChannelId("ocld"), data);
        }

        FldoHandlerType const& handler() const
        {
            return _out;
        }

    private:
        FldoHandlerType& _out;
};

template<typename AccelerationSearchType>
struct FldoHandler<AccelerationSearchType, typename std::enable_if<panda::HasMethod<typename AccelerationSearchType::Traits, AccelerationSearchType::template has_create_fldo_handler_t>::value>::type>
{
        typedef typename AccelerationSearchType::Traits Traits;

    public:
        typedef typename std::remove_pointer<typename AccelerationSearchType::template has_create_fldo_handler_t<Traits>>::type FldoHandlerType;

        FldoHandler(AccelerationSearchType& pipeline)
            : _fldo_handler(Traits::create_fldo_handler(pipeline.out(), pipeline._config, pipeline._beam_config))
        {
        }

        void operator()(std::shared_ptr<data::Ocld> const& data) const
        {
            _fldo_handler->operator()(data);
        }

        FldoHandlerType const& handler() const {
            return *_fldo_handler;
        }

    private:
        std::unique_ptr<FldoHandlerType> _fldo_handler;
};

} // namespace detail
} // namespace pipeline
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_PIPELINE_FLDOHANDLER_H
