/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/** @addtogroup apps
 * @{
 * @section app_launcher Pipeline Launch
 * @brief cheetah_pipeline The Executable to launch a variety of pipelines
 * @details
 *    This is the main cheetah executable and enables you to launch pipelines with a variety of options.
 *    It allows you to select:
 *    - the type of pipeline to run
 *    - the data sources (UDP packet stream data, sigproc files, generated data)
 *    - the data sink
 *
 *    for help on how to use see:
 *    @verbatim
       cheetah_pipeline --help
      @endverbatim
 *
 * @} */ // end group

#include "cheetah/pipeline/CheetahConfig.h"
#include "cheetah/pipeline/BeamLauncher.h"
#include "cheetah/utils/TerminateException.h"
#include "cheetah/rcpt/UdpStream.h"
#include "cheetah/rcpt_low/UdpStream.h"
#include "cheetah/sigproc/SigProcFileStream.h"
#include "cheetah/psrdada/SigProcDadaStream.h"
#include "cheetah/pipeline/PipelineHandlerFactory.h"
#include "panda/MixInTimer.h"

using namespace ska::cheetah;

int main(int argc, char** argv) {

    int rv = 1; // program return value

    typedef uint8_t NumericalT;

    try {
        // -- configuration setup --
        pipeline::CheetahConfig<NumericalT> config;
        pipeline::PipelineHandlerFactory pipeline_factory(config);
        config.set_pipeline_handlers(pipeline_factory.available());

        // -- parse the command line --
        if( (rv=config.parse(argc, argv)) ) return rv;

        // -- create computation unit to run --
        std::function<pipeline::PipelineHandlerFactory::HandlerType*(pipeline::BeamConfig<NumericalT> const&)> runtime_handler_factory;
        if(config.time_handler_invocation()) {
            runtime_handler_factory = [&](pipeline::BeamConfig<NumericalT> const& beam_config) {
                 return pipeline_factory.create_timed(config.pipeline_name(), beam_config);
            };
        }
        else {
            runtime_handler_factory = [&](pipeline::BeamConfig<NumericalT> const& beam_config) {
                return pipeline_factory.create(config.pipeline_name(), beam_config);
            };
        }

        // -- create and launch a suitable pipeline --

        std::string const& stream_name = config.stream_name();
        if(stream_name == "sigproc") {
            pipeline::BeamLauncher<sigproc::SigProcFileStream, NumericalT> beam( config.beams_config(), [&](pipeline::BeamConfig<NumericalT> const& config) -> sigproc::Config const& { return config.sigproc_config(); }, runtime_handler_factory);
            rv = beam.exec();
            config.pool_manager().wait();
        }
#ifdef ENABLE_PSRDADA
        else if(stream_name == "psrdada") {
            pipeline::BeamLauncher<psrdada::SigProcDadaStream, NumericalT> beam( config.beams_config(), [&](pipeline::BeamConfig<NumericalT> const& config) -> psrdada::Config const& { return config.psrdada_config(); }, runtime_handler_factory);
            rv = beam.exec();
            config.pool_manager().wait();
        }
#endif
        else if(stream_name == "udp") {
            pipeline::BeamLauncher<rcpt::UdpStream, NumericalT> beam( config.beams_config(), [&](pipeline::BeamConfig<NumericalT> const& config) -> rcpt::Config const& { return config.rcpt_config(); }, runtime_handler_factory);
            rv = beam.exec();
            config.pool_manager().wait();
        }

        else if(stream_name == "udp_low") {
            pipeline::BeamLauncher<rcpt_low::UdpStream, NumericalT> beam( config.beams_config(), [&](pipeline::BeamConfig<NumericalT> const& config) -> rcpt_low::Config const& { return config.rcpt_low_config(); }, runtime_handler_factory);
            rv = beam.exec();
        }

        else {
             std::cerr << "unknown source '" << stream_name << "'" << std::endl;
        }
    }
    catch(utils::TerminateException&) {
        return 0; // terminate with success
    }
    catch(std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return rv;
}

