/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt_low/BeamFormerPacketInspector.h"
#include <panda/Log.h>
#include <algorithm>


namespace ska {
namespace cheetah {
namespace rcpt_low {
template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket> const& BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::packet() const { return _packet; }

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::BeamFormerPacketInspector(BeamFormerPacket<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket> const & packet)
    : _packet(packet)
{
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::~BeamFormerPacketInspector()
{
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
typename BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::PacketNumberType BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::sequence_number() const
{
    PacketNumberType value = ((PacketNumberType)_packet.packet_count())*(number_of_channels_low/((PacketNumberType)_packet.number_of_channels()))+(((PacketNumberType)(_packet.first_channel_number()))/((PacketNumberType)_packet.number_of_channels()));
    return value; // * _scale_factor;
}

template<typename PacketDataType, unsigned TimeSamplesPerPacket, unsigned ChannelsPerPacket>
bool BeamFormerPacketInspector<PacketDataType, TimeSamplesPerPacket,  ChannelsPerPacket>::ignore() const
{
    return (_packet.magic_word()==25146554)?false:true;

}

} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
