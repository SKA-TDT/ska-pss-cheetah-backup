/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RCPT_LOW_UDPSTREAM_H
#define SKA_CHEETAH_RCPT_LOW_UDPSTREAM_H

#include "cheetah/rcpt_low/Config.h"
#include "cheetah/rcpt_low/BeamFormerDataTraits.h"
#include "cheetah/utils/ModifiedJulianClock.h"
#include <panda/PacketStream.h>
#include <panda/ResourceManager.h>
#include <boost/units/systems/si/time.hpp>

namespace ska {
namespace cheetah {
namespace rcpt_low {

/**
 * @brief
 *    The UDP stream from the BeamFormer
 *
 * @details
 * The stream maintains a single thread to fill packets which are then delegated to
 * any number of other threads for processing.
 *
 */

template<typename Producer>
class UdpStreamTmpl : public panda::PacketStream<UdpStreamTmpl<Producer>, panda::ConnectionTraits<panda::Udp>, BeamFormerDataTraitsLow, Producer>
{
        typedef panda::ConnectionTraits<panda::Udp> ConnectionTraits;
        typedef panda::PacketStream<UdpStreamTmpl<Producer>, ConnectionTraits, BeamFormerDataTraitsLow, Producer> BaseT;
        typedef BeamFormerDataTraitsLow::PacketInspector PacketInspector;

    public:
        typedef boost::units::quantity<ska::cheetah::data::MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<ska::cheetah::data::Seconds, double> TsampType;
        typedef typename ska::cheetah::data::TimeFrequency<Cpu, uint8_t>::TimeType TimeType;

    public:
        UdpStreamTmpl(Config const& config);
        ~UdpStreamTmpl();

        /**
         * @brief post construction initialisation (called by Producer)
         */
        void init();

        // initialise the chunk (size etc.)
        template<typename DataType>
        std::shared_ptr<DataType> get_chunk(unsigned sequence_number, PacketInspector const& p);

    private:
        data::DimensionSize<data::Frequency> _n_channels;
        data::DimensionSize<data::Time> _n_samples;
        unsigned _n_time_samples_per_packet;
        unsigned _n_channels_per_packet;
        TsampType _tsamp;
        static ska::cheetah::utils::ModifiedJulianClock::time_point _tstart;
};

class UdpStream : public UdpStreamTmpl<UdpStream>
{
    public:
        UdpStream(Config const& config);
        ~UdpStream();

};

} // namespace rcpt_low
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt_low/detail/UdpStream.cpp"

#endif // SKA_CHEETAH_RCPT_LOW_UDPSTREAM_H
