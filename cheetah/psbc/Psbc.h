/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PSBC_PSBC_H
#define SKA_CHEETAH_PSBC_PSBC_H

#include "cheetah/psbc/Config.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/Units.h"
#include <vector>
#include <mutex>
#include <memory>

namespace ska {
namespace cheetah {
namespace psbc {

/**
 * This typedefs are specific to pipelines. Need to find a different
 * way to handle this. Technically the Psbc handler could detemine the
 * internal psbc types.
 */
typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
typedef data::DmTime<DmTrialsType> DmTimeType;

/**
 * @brief A class for buffering DmTrials objects
 *
 * @details The Psbc class uses DmTimes as a container
 *          to store DmTrials shared pointers for being passed
 *          to Fdas and Tdas
 *
 */

template<typename Handler>
class Psbc
{
    public:
        typedef typename DmTimeType::ValueType DataType;
        typedef DmTrialsType::TimeType Seconds; // This should not come from DmTrialsType it should be independent

    public:
        /**
         * @brief      Construct a new instance
         *
         * @param      config     The Psbc configuration
         * @param      handler    The pipeline handler for Psbc to call
         */
        Psbc(Config const& config, Handler& handler);
        Psbc(Psbc const&) = delete;
        Psbc(Psbc&&) = default;
        ~Psbc();

        /**
         * @brief add a DmTrials shared pointer to the underlying container
         */
        void operator()(DataType const&);

        /**
         * @brief      Determines if blocks in the buffer are contiguous in time.
         *
         * @return     True if contiguous, False otherwise.
         */
        bool is_contiguous() const;

    private:
        Seconds _duration;
        Seconds _dump_time;
        std::shared_ptr<DmTimeType> _data;
        Handler& _handler;
        std::unique_ptr<std::mutex> _mutex;
};


} // namespace psbc
} // namespace cheetah
} // namespace ska
#include "cheetah/psbc/detail/Psbc.cpp"

#endif // SKA_CHEETAH_PSBC_PSBC_H
