#include "cheetah/data/test_utils/DmTrialsGeneratorUtil.h"
#include "cheetah/psbc/test/PsbcTest.h"
#include "cheetah/psbc/Psbc.h"
#include "cheetah/psbc/Config.h"
#include "cheetah/utils/NullHandler.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DmTrials.h"

namespace ska {
namespace cheetah {
namespace psbc {
namespace test {

PsbcTest::PsbcTest()
    : ::testing::Test()
{
}

PsbcTest::~PsbcTest()
{
}

void PsbcTest::SetUp()
{
}

void PsbcTest::TearDown()
{
}

TEST_F(PsbcTest, test_contiguity)
{
    typedef data::DmTrials<cheetah::Cpu,float> DmTrialsType;
    typedef typename DmTrialsType::DmType Dm;
    typedef typename DmTrialsType::TimeType Seconds;

    Config config;
    utils::NullHandler handler;
    Psbc<decltype(handler)> buffer(config,handler);
    data::test::DmTrialsGeneratorUtil<DmTrialsType> trials_generator;
    config.dump_time(Seconds(10.0 * data::seconds));
    for (std::size_t block_idx=0; block_idx<10; ++block_idx)
    {
        auto trial = trials_generator.generate(Seconds(0.000064*data::seconds),10,3);
        buffer(trial);
    }
    ASSERT_TRUE(buffer.is_contiguous());
}

} // namespace test
} // namespace psbc
} // namespace cheetah
} // namespace ska
