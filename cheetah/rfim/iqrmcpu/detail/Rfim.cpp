/* The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/iqrmcpu/Rfim.h"
#include "panda/Log.h"
#include "panda/TypeTraits.h"
#include <cmath>
#include <algorithm>



namespace ska {
namespace cheetah {
namespace rfim {
namespace iqrmcpu {

namespace {

template<typename T>
static inline double Lerp(T v0, T v1, T t)
{
    return (1 - t)*v0 + t*v1;
}

template<typename T>
static inline std::vector<T> quantile(const std::vector<T>& in_data, const std::vector<T>& probs)
{
    if (in_data.empty())
    {
        return std::vector<T>();
    }

    if (1 == in_data.size())
    {
        return std::vector<T>(1, in_data[0]);
    }

    std::vector<T> data = in_data;
    std::sort(data.begin(), data.end());
    std::vector<T> quantiles;

    for (size_t i = 0; i < probs.size(); ++i)
    {
        T poi = Lerp<T>(-0.5, data.size() - 0.5, probs[i]);

        size_t left = std::max(int64_t(std::floor(poi)), int64_t(0));
        size_t right = std::min(int64_t(std::ceil(poi)), int64_t(data.size() - 1));

        T datLeft = data.at(left);
        T datRight = data.at(right);

        T quantile = Lerp<T>(datLeft, datRight, poi - left);

        quantiles.push_back(quantile);
    }

    return quantiles;
}

// Produces a suitable type for recording rfim flags
// default is to use a new structure

template<typename DataAdapterType, typename TimeFrequencyType, typename Enable=void>
struct FlaggedDataTypeHelper {
    typedef data::RfimFlaggedData< const TimeFrequencyType> FlaggedDataType;
    typedef FlaggedDataType ReturnType;
    template<typename DataType>
    ReturnType flagged_data(DataAdapterType& , DataType const& data) {
        return FlaggedDataType(data);
    }
};

// unless a flags structure already exists
template<typename DataAdapterType, typename TimeFrequencyType>
struct FlaggedDataTypeHelper<DataAdapterType, TimeFrequencyType, typename std::enable_if<std::is_convertible<typename panda::is_pointer_wrapper<typename DataAdapterType::ReturnType>::type, data::RfimFlaggedData<TimeFrequencyType>>::value>::type> {
    typedef data::RfimFlaggedData<TimeFrequencyType> FlaggedDataType;
    typedef FlaggedDataType& ReturnType;
    template<typename DataType>
    ReturnType flagged_data(DataAdapterType& adapter, DataType const&) {
        return static_cast<FlaggedDataType&>(*adapter.data());
    }
};

} // namespace



template<typename RfimTraits>
Rfim<RfimTraits>::Rfim(Config const& config)
    : _config(config)
{
}

template<typename RfimTraits>
Rfim<RfimTraits>::~Rfim()
{
}

template<typename RfimTraits>
template<typename DataType>
inline void Rfim<RfimTraits>::operator()(DataType const& data, DataAdapter& adapter)
{
    std::size_t data_size=data.number_of_channels() * data.number_of_spectra();
    if(data_size == 0) return;

    // compute per channel std for the TF block
    FlaggedDataTypeHelper<DataAdapter, DataType> flagged_data_helper;
    typename FlaggedDataTypeHelper<DataAdapter, DataType>::ReturnType flagged_data = flagged_data_helper.flagged_data(adapter, data);

    // Compute IQR range
    std::size_t flagged_data_channels = flagged_data.template dimension<data::Frequency>();
    std::vector<bool> flags(flagged_data_channels, false);
    std::vector<bool> flags_per_lag(flagged_data_channels, false);
    std::vector<float> diff_vector(flagged_data_channels);

    for (std::size_t ii= 0; ii<= 2*_config.max_lag(); ++ii)
    {

	    //Rotate the vector
	    std::size_t jj = 0;

        auto const& channel_stats = flagged_data.channel_stats();
        if (ii <= _config.max_lag())
        {
            std::size_t ind=0;
            for (std::size_t kk=0; kk < channel_stats.size(); ++kk)
            {
                if (kk <= flagged_data_channels -( _config.max_lag() - ii) - 1)
                    diff_vector[kk] = std::sqrt(channel_stats[kk].variance) - std::sqrt(channel_stats[kk + (_config.max_lag() - ii)].variance);
                else
                {
                    diff_vector[kk] = std::sqrt(channel_stats[kk].variance) - std::sqrt(channel_stats[ind].variance);
                    ++ind;
                }
            }
        }
        else
        {
            std::size_t ind=0;
            for (std::size_t kk=0; kk < channel_stats.size(); ++kk)
            {
                if (kk < ii)
                {
                    diff_vector[kk] = std::sqrt(channel_stats[kk].variance) - std::sqrt(channel_stats[channel_stats.size() - 1 -ind].variance);
                    ++ind;
                }
                else
                    diff_vector[kk] = std::sqrt(channel_stats[kk].variance) - std::sqrt(channel_stats[kk - ii].variance);
            }
        }

	    std::vector<float> quant = quantile<float>(diff_vector, {0.25, 0.5, 0.75});

	    // Generate a difference vector for different lags
	    float std_dev = std::abs((quant[2] - quant[0]) / 1.349);

        std::generate(flags_per_lag.begin(), flags_per_lag.end(),
                      [&]()
                      {
                          bool flag = diff_vector[jj] - quant[1] > _config.nsigma()*std_dev;
                          ++jj;
                          return flag;
                      });

        //Logical OR on the an initial mask
        for (std::size_t kk=0; kk < flags.size(); ++kk)
        {
            flags[kk] = flags_per_lag[kk] | flags[kk];
        }
    }

    // Write new flags via the adapter
    float fraction = 0.0;

    for (std::size_t ii=0; ii < flags.size(); ++ii)
    {
        if (ii < _config.edge_channels() || ii > (flags.size()- _config.edge_channels()))
        {
            adapter.mark_bad(data.channel(ii));
            continue;
        }
        if (flags[ii] == true)
        {
            adapter.mark_bad(data.channel(ii));
            ++fraction;
        }
    }

    PANDA_LOG << "rfim::iqrm : Number of channels flagged: " << fraction/(float)flags.size();

}
} // namespace iqrmcpu
} // namespace rfim
} // namespace cheetah
} // namespace ska
