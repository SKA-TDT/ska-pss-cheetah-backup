/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/iqrmcpu/Config.h"


namespace ska {
namespace cheetah {
namespace rfim {
namespace iqrmcpu {

Config::Config()
    : utils::Config("rfim_iqrmcpu")
    , _active(false)
    , _max_lag(3)
    , _nsigma(3.0)
    , _edge_channels(0)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active), "use algorithm for rfi clipping")
    ("max_lag", boost::program_options::value<std::size_t>(&_max_lag), "widest RFI event in frequency to check for (as numbero of channels)")
    ("nsigma", boost::program_options::value<float>(&_nsigma), "Set the threshold for flagging in units of standard deviation")
    ("edge_channels", boost::program_options::value<std::size_t>(&_edge_channels), "Number of channels to flag on band edges");
}

bool Config::active() const
{
    return _active;
}

std::size_t Config::max_lag() const
{
    return _max_lag;
}

float Config::nsigma() const
{
    return _nsigma;
}

std::size_t Config::edge_channels() const
{
    return _edge_channels;
}

} // namespace iqrmcpu
} // namespace rfim
} // namespace cheetah
} // namespace ska
