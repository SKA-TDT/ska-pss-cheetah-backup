/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test/CandidateTest.h"
#include "cheetah/data/Candidate.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {


CandidateTest::CandidateTest() : ::testing::Test()
{
}

CandidateTest::~CandidateTest()
{
}

void CandidateTest::SetUp()
{
}

void CandidateTest::TearDown()
{
}

// test_input_data
// Sets the input data and re-read them
//
TEST_F(CandidateTest, test_input_data)
{
    typedef Candidate<Cpu, double> CandidateType;

    typename CandidateType::SecPerSecType pdot_val = 0.000001;     /**< set pulsar_candidate derivative period  */
    typename CandidateType::Dm dm_val = 10. * parsecs_per_cube_cm;    /**< set pulsar_candidate dispersion measure */
    std::size_t ident_val = 0;      /**< set pulsar_candidate ident              */

    //set the pulsar_candidate period to 1.24 ms
    CandidateType::MsecTimeType period_val(0.00124 * boost::units::si::seconds);
    // generate a pulsar_candidate with the specified period, pdot, dm, ident
    CandidateType pulsar_cand(period_val, pdot_val, dm_val) ;
    // real check
    ASSERT_EQ(pdot_val, pulsar_cand.pdot());
    ASSERT_EQ(period_val, pulsar_cand.period());
    ASSERT_EQ(ident_val, pulsar_cand.ident());
}


// test_set_parameter
// Create a zero-sized Candidate object, sets data members and re-read them
//
TEST_F(CandidateTest, test_set_parameter)
{
    typedef Candidate<Cpu, double> CandidateType;
    typename CandidateType::SecPerSecType pdot_val = 0.000001;     /**< set pulsar_candidate derivative period  */
    typename CandidateType::Dm dm_val = 10. * parsecs_per_cube_cm;    /**< set pulsar_candidate dispersion measure */
    std::size_t ident_val = 5;      /**< set pulsar_candidate ident              */

    //set the pulsar_candidate period to 1.24 ms
    CandidateType::MsecTimeType period_val(0.00124 * boost::units::si::seconds);
    CandidateType::MsecTimeType width_val(0.0002 * boost::units::si::seconds);
    // create a pulsar_candidate with default values
    CandidateType pulsar_cand ;

    // assign specific values
    pulsar_cand.period(period_val);
    pulsar_cand.pdot(pdot_val);
    pulsar_cand.width(width_val);
    pulsar_cand.dm(dm_val);
    pulsar_cand.ident(ident_val);

    // real check
    ASSERT_EQ(pdot_val, pulsar_cand.pdot());
    ASSERT_EQ(period_val, pulsar_cand.period());
    ASSERT_EQ(dm_val, pulsar_cand.dm());
    ASSERT_EQ(ident_val, pulsar_cand.ident());
    ASSERT_EQ(width_val, pulsar_cand.width());
}

// test_candidate_copy
// Create a Candidate object, do the copy and compare the
// members values.
//
TEST_F(CandidateTest, test_candidate_copy)
{
    typedef Candidate<Cpu, double> CandidateType;
    typename CandidateType::SecPerSecType pdot_val = 0.000001;     /**< set pulsar_candidate derivative period  */
    typename CandidateType::Dm dm_val = 10. * parsecs_per_cube_cm;    /**< set pulsar_candidate dispersion measure */
    double sigma_val = 20;           /**< the PulsaCandidate signal-to-noise     */
    std::size_t ident_val = 5;      /**< set pulsar_candidate ident              */

    //set the pulsar_candidate period to 1.24 ms
    CandidateType::MsecTimeType period_val(0.00124 * boost::units::si::seconds);
    //set the pulsar_candidate pulse width 
    CandidateType::MsecTimeType width_val(0.0002 * boost::units::si::seconds);
    // create a pulsar_candidate with configured values
    CandidateType pulsar_cand(period_val, pdot_val, dm_val, width_val, sigma_val, ident_val);

    // do the copy 
    CandidateType pulsar_cand_copy(pulsar_cand);

    // compare the two candidates
    ASSERT_EQ(pulsar_cand_copy.pdot(), pulsar_cand.pdot());
    ASSERT_EQ(pulsar_cand_copy.period(), pulsar_cand.period());
    ASSERT_EQ(pulsar_cand_copy.dm(), pulsar_cand.dm());
    ASSERT_EQ(pulsar_cand_copy.ident(), pulsar_cand.ident());
    ASSERT_EQ(pulsar_cand_copy.width(), pulsar_cand.width());
    ASSERT_EQ(pulsar_cand_copy.sigma(), pulsar_cand.sigma());
}
} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
