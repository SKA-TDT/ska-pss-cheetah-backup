/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test/TimeFrequencyStatsTest.h"
#include "cheetah/generators/GaussianNoise.h"


namespace ska {
namespace cheetah {
namespace data {
namespace test {


TimeFrequencyStatsTest::TimeFrequencyStatsTest()
    : ::testing::Test()
{
}

TimeFrequencyStatsTest::~TimeFrequencyStatsTest()
{
}

void TimeFrequencyStatsTest::SetUp()
{
}

void TimeFrequencyStatsTest::TearDown()
{
}

TEST_F(TimeFrequencyStatsTest, test_stat_calculation)
{
    typedef uint8_t NumericalT;
    generators::GaussianNoiseConfig generator_config;
    generator_config.mean(5.0);
    generator_config.std_deviation(1.0);
    generators::GaussianNoise<NumericalT> generator(generator_config);

    std::size_t number_of_channels = 4100;
    std::size_t number_of_spectra = 4096;

    TimeFrequency<Cpu, NumericalT> tf1((data::DimensionSize<data::Time>(number_of_spectra))
                                   ,data::DimensionSize<data::Frequency>(number_of_channels));

    // fill data
    generator.next(tf1);

    //Generate Stats block
    TimeFrequencyStats<decltype(tf1)> stats(std::make_shared<TimeFrequency<Cpu, NumericalT>>(tf1));

    // Check statistics
    auto const& channel_stats = stats.channel_stats();
    ASSERT_EQ(channel_stats.size(), number_of_channels);
    for(auto const& stat : channel_stats) {
        ASSERT_NEAR(stat.mean, 5.0, 1.0);
        ASSERT_NEAR(stat.variance, 1.0, 0.25);
    }

    auto const& spectrum_stats = stats.spectrum_stats();
    ASSERT_EQ(spectrum_stats.size(), number_of_spectra);
    for(auto const& stat : spectrum_stats) {
        ASSERT_NEAR(stat.mean, 5.0, 1.0);
        ASSERT_NEAR(stat.variance, 1.0, 0.25);
    }

}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
