/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/test/SpCclTest.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/SpCandidate.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/Architecture.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace data {
namespace test {


SpCclTest::SpCclTest() : ::testing::Test()
{
}

SpCclTest::~SpCclTest()
{
}

void SpCclTest::SetUp()
{
}

void SpCclTest::TearDown()
{
}

/**
 * Test construction of SpCcl<uint8_t> object
 */
TEST_F(SpCclTest, test_tf_blocks)
{
    typedef typename SpCcl<uint8_t>::TimeFrequencyType TimeFrequencyType;

    //Generate two blocks of TF data and push to the block list
    typename SpCcl<uint8_t>::BlocksType blocks;
    {
        std::shared_ptr<TimeFrequencyType> b1(new TimeFrequencyType(data::DimensionSize<data::Time>(1000), data::DimensionSize<data::Frequency>(10)));
        std::shared_ptr<TimeFrequencyType> b2(new TimeFrequencyType(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(10)));
        blocks.emplace_back(b1);
        blocks.emplace_back(b2);
    }

    //Create new SpCcl<uint8_t> instance
    SpCcl<uint8_t> cand_list(blocks);

    //Retreive tf_blocks
    auto const& blocks_ref = cand_list.tf_blocks();

    ASSERT_EQ(blocks_ref.size(), std::size_t(2));
    ASSERT_EQ(blocks_ref[0]->number_of_spectra(), std::size_t(1000));
    ASSERT_EQ(blocks_ref[1]->number_of_spectra(), std::size_t(100));
}

TEST_F(SpCclTest, test_candidate_vector_behaviour)
{

    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFrequencyType;

    std::size_t idx;

    //Create new SpCcl<uint8_t> instance
    SpCcl<uint8_t> cand_list;

    ASSERT_EQ(cand_list.tf_blocks().size(),std::size_t(0));

    //set single pulse candidate dispersion measure
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;

    for (idx=0; idx<10; ++idx)
    {
        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);
    }

    // real check
    ASSERT_EQ(cand_list.size(), std::size_t(10));

    for (idx=0; idx<10; ++idx)
    {
        auto const& cand = cand_list[idx];
        ASSERT_EQ(cand.dm(), dm);
        ASSERT_EQ(cand.tstart(), tstart);
        ASSERT_EQ(cand.width(), width);
        ASSERT_EQ(cand.sigma(), sigma);
        ASSERT_EQ(cand.ident(), idx);
    }

    SpCcl<uint8_t> cand_list2;

    for (idx=0; idx<10; ++idx)
    {
        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list2.emplace_back(std::move(candidate));
    }

    // real check
    ASSERT_EQ(cand_list.size(), std::size_t(10));

    for (idx=0; idx<10; ++idx)
    {
        auto const& cand = cand_list2[idx];
        ASSERT_EQ(cand.dm(), dm);
        ASSERT_EQ(cand.tstart(), tstart);
        ASSERT_EQ(cand.width(), width);
        ASSERT_EQ(cand.sigma(), sigma);
        ASSERT_EQ(cand.ident(), idx);
    }
}

TEST_F(SpCclTest, test_candidate_dm_range)
{

    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFrequencyType;

    std::size_t idx;

    //Create new SpCcl<uint8_t> instance
    SpCcl<uint8_t> cand_list;

    ASSERT_EQ(cand_list.tf_blocks().size(),std::size_t(0));

    //set single pulse candidate dispersion measure
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;

    for (idx=0; idx<10; ++idx)
    {
        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        dm += SpCandidateType::Dm(1.0 * parsecs_per_cube_cm);
        cand_list.push_back(candidate);
    }

    ASSERT_EQ(cand_list.dm_range().first, SpCandidateType::Dm(12.0 * parsecs_per_cube_cm));
    ASSERT_EQ(cand_list.dm_range().second, SpCandidateType::Dm(21.0 * parsecs_per_cube_cm));

    // Test removing candidates with the lowest Dm
    cand_list.remove_if([](SpCandidateType const& s)
                        {
                            return s.dm() == SpCandidateType::Dm(12.0 * parsecs_per_cube_cm);
                        });
    ASSERT_EQ(cand_list.dm_range().first, SpCandidateType::Dm(13.0 * parsecs_per_cube_cm));
    ASSERT_EQ(cand_list.dm_range().second, SpCandidateType::Dm(21.0 * parsecs_per_cube_cm));

    // Test removing candidates with the highest Dm
    cand_list.remove_if([](SpCandidateType const& s)
                        {
                            return s.dm() == SpCandidateType::Dm(21.0 * parsecs_per_cube_cm);
                        });
    ASSERT_EQ(cand_list.dm_range().first, SpCandidateType::Dm(13.0 * parsecs_per_cube_cm));
    ASSERT_EQ(cand_list.dm_range().second, SpCandidateType::Dm(20.0 * parsecs_per_cube_cm));

    // Test removing candidates neither highest or lowest
    cand_list.remove_if([](SpCandidateType const& s)
                        {
                            return s.dm() == SpCandidateType::Dm(17.0 * parsecs_per_cube_cm);
                        });
    ASSERT_EQ(cand_list.dm_range().first, SpCandidateType::Dm(13.0 * parsecs_per_cube_cm));
    ASSERT_EQ(cand_list.dm_range().second, SpCandidateType::Dm(20.0 * parsecs_per_cube_cm));

}

TEST_F(SpCclTest, test_equalto_operator_no_data)
{
/**
 *@brief  Test for when there are no candidate, the object should return just iterator equal to the end iterator and not do any extraction
 */
    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list;
    CandidateWindow window;
    ASSERT_NO_THROW(cand_list.data_begin(window));
    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();
    SpCcl<uint8_t>::ConstDataIterator it_end2 = cand_list.data_end();

    ASSERT_TRUE(it_end == it_end2);
    ASSERT_FALSE(it_end != it_end2);
    ASSERT_TRUE(it_begin == it_end);
}

TEST_F(SpCclTest, test_emplace_back)
{
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    float sigma = 20.0;

    // just add a single candidate
    SpCcl<uint8_t> cand_list;
    SpCandidateType candidate(dm, tstart, width, sigma);
    cand_list.emplace_back(std::move(candidate));
    ASSERT_EQ(cand_list.size(), 1U);
    auto const& cand = cand_list[0];
    ASSERT_EQ(cand.dm(), dm);
    ASSERT_EQ(cand.tstart(), tstart);
    ASSERT_EQ(cand.width(), width);
    ASSERT_EQ(cand.sigma(), sigma);

    //  add an out of order candidate
    SpCandidateType::MsecTimeType tstart2(1.0 * boost::units::si::seconds);
    SpCandidateType candidate2(dm, tstart2, width, sigma);
    cand_list.emplace_back(std::move(candidate2));
    ASSERT_EQ(cand_list.size(), 2U);
    auto const& cand0 = cand_list[0];
    auto const& cand1 = cand_list[1];
    ASSERT_EQ(cand0.tstart(), tstart2);
    ASSERT_EQ(cand1.tstart(), tstart);
}

TEST_F(SpCclTest, test_emplace_back_constructor_list)
{
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    float sigma = 20.0;

    typedef typename std::remove_const<SpCcl<uint8_t>::BlocksType::value_type::element_type>::type TimeFrequencyType;
    data::DimensionSize<data::Time> number_of_spectra(4U);
    data::DimensionSize<data::Frequency> number_of_channels(10U);

    auto f1 =  data::TimeFrequency<Cpu, uint8_t>::FrequencyType(10.0 * boost::units::si::hertz);
    auto f2 =  data::TimeFrequency<Cpu, uint8_t>::FrequencyType(20.0 * boost::units::si::hertz);
    auto delta = (f2 - f1)/ (double)(number_of_channels-1);
    boost::units::quantity<MilliSeconds, double> delay =
        (4.15 * 1000000.0 * boost::units::si::milli * boost::units::si::seconds) * ( dm * ( (1/(f1 * f1)) - (1/(f2 * f2)) )).value();

    std::shared_ptr<TimeFrequencyType> tf = std::make_shared<TimeFrequencyType>(number_of_spectra, number_of_channels);
    tf->set_channel_frequencies_const_width( f2, -1.0 * delta );

    SpCcl<uint8_t> cand_list({tf});
    cand_list.emplace_back(dm, tstart, width, sigma);

    ASSERT_EQ(cand_list.size(), 1U);
    auto const& cand = cand_list[0];
    ASSERT_EQ(cand.dm(), dm);
    ASSERT_EQ(cand.tstart(), tstart);
    ASSERT_DOUBLE_EQ(cand.tend().value(), (tstart + delay).value());
    ASSERT_EQ(cand.width(), width);
    ASSERT_EQ(cand.sigma(), sigma);
}

TEST_F(SpCclTest, test_sort_behaviour)
{

    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFrequencyType;

    std::size_t idx;

    //Create new SpCcl<uint8_t> instance
    SpCcl<uint8_t> cand_list;

    SpCcl<uint8_t> cand_list2;

    ASSERT_EQ(cand_list.tf_blocks().size(),std::size_t(0));

    //set single pulse candidate dispersion measure
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(2.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;

    for (idx=0; idx<10; ++idx)
    {

        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back(candidate);

        cand_list2.emplace(std::move(candidate));

        tstart += SpCandidateType::MsecTimeType(1.0*boost::units::si::seconds);
    }

    // real check
    ASSERT_EQ(cand_list.size(), std::size_t(10));

    SpCandidateType::MsecTimeType ststart(2.0 * boost::units::si::seconds);
    for (idx=0; idx<10; ++idx)
    {
        SCOPED_TRACE(idx);
        auto const& cand = cand_list[idx];
        auto const& cand2 = cand_list2[idx];
        ASSERT_EQ(cand.dm(), dm);
        ASSERT_EQ(cand2.dm(), dm);
        ASSERT_EQ(cand.tstart(), ststart);
        ASSERT_EQ(cand2.tstart(), ststart);
        ASSERT_EQ(cand.width(), width);
        ASSERT_EQ(cand2.width(), width);
        ASSERT_EQ(cand.sigma(), sigma);
        ASSERT_EQ(cand2.sigma(), sigma);
        ASSERT_EQ(cand.ident(), idx);
        ASSERT_EQ(cand2.ident(), idx);
        ststart += SpCandidateType::MsecTimeType(1.0*boost::units::si::seconds);
    }
}

TEST_F(SpCclTest, test_disordered_input_sort_behaviour)
{

    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFrequencyType;
    data::DimensionSize<data::Time> number_of_spectra(4U);
    data::DimensionSize<data::Frequency> number_of_channels(10U);

    auto f1 =  data::TimeFrequency<Cpu, uint8_t>::FrequencyType(10.0 * boost::units::si::hertz);
    auto f2 =  data::TimeFrequency<Cpu, uint8_t>::FrequencyType(20.0 * boost::units::si::hertz);
    auto delta = (f2 - f1)/ (double)(number_of_channels-1);


    std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>> tf = std::make_shared<data::TimeFrequency<Cpu, uint8_t>>(number_of_spectra, number_of_channels);
    tf->set_channel_frequencies_const_width( f2, -1.0 * delta );
    ASSERT_EQ(tf->low_high_frequencies().first, f1);
    ASSERT_EQ(tf->low_high_frequencies().second, f2);

    //Create new SpCcl<uint8_t> instance
    SpCcl<uint8_t> cand_list({tf});
    ASSERT_EQ(cand_list.tf_blocks().size(),std::size_t(1));
    ASSERT_EQ(cand_list.tf_blocks()[0]->low_high_frequencies().first, f1);

    SpCcl<uint8_t> cand_list2({tf});
    ASSERT_EQ(cand_list2.tf_blocks().size(),std::size_t(1));

    //set single pulse candidate dispersion measure
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    boost::units::quantity<MilliSeconds, double> delay =
        (4.15 * 1000000.0 * boost::units::si::milli * boost::units::si::seconds) * ( dm * ( (1/(f1 * f1)) - (1/(f2 * f2)) )).value();

    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(11.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;

    for (std::size_t idx=0; idx<10; ++idx)
    {

        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        cand_list.push_back_calculate_duration(candidate);
        cand_list2.emplace_calculate_duration(std::move(candidate));
        tstart -= SpCandidateType::MsecTimeType(1.0*boost::units::si::seconds);
    }

    // real check
    ASSERT_EQ(cand_list.size(), std::size_t(10));

    SpCandidateType::MsecTimeType ststart(2.0 * boost::units::si::seconds);
    for (std::size_t idx=0; idx<10; ++idx)
    {
        SCOPED_TRACE(idx);
        auto const& cand = cand_list[idx];
        auto const& cand2 = cand_list2[idx];
        ASSERT_EQ(cand.dm(), dm);
        ASSERT_EQ(cand2.dm(), dm);
        ASSERT_EQ(cand.tstart(), ststart);
        ASSERT_DOUBLE_EQ(cand.tend().value(), (ststart + delay).value());
        ASSERT_EQ(cand2.tstart(), ststart);
        ASSERT_DOUBLE_EQ(cand2.tend().value(), (ststart + delay).value());
        ASSERT_EQ(cand.width(), width);
        ASSERT_EQ(cand2.width(), width);
        ASSERT_EQ(cand.sigma(), sigma);
        ASSERT_EQ(cand2.sigma(), sigma);
        ASSERT_EQ(cand.ident(), 9-idx);
        ASSERT_EQ(cand2.ident(), 9-idx);
        ststart += SpCandidateType::MsecTimeType(1.0*boost::units::si::seconds);
    }
}


TEST_F(SpCclTest, test_candidates_extraction)
{
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));
    TimeFrequency<Cpu, uint8_t> tf2(data::DimensionSize<data::Time>(200),data::DimensionSize<data::Frequency>(1024));
    TimeFrequency<Cpu, uint8_t> tf3(data::DimensionSize<data::Time>(300),data::DimensionSize<data::Frequency>(1024));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf2.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf3.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);
    std::fill(tf2.begin(),tf2.end(),1);
    std::fill(tf3.begin(),tf3.end(),2);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf2));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf3));

    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(1 * boost::units::si::milli * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(5.0 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;
    //set the tend;
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);

    for (std::uint32_t idx=0; idx<30; ++idx)
    {
        SpCandidateType candidate(dm, tstart, width, sigma, idx);
        candidate.duration_end(tend);
        cand_list.push_back(candidate);
        tstart += SpCandidateType::MsecTimeType(10*boost::units::si::seconds);
    }

    // Run Data iterator instance here to extract candidates

    CandidateWindow window( 1000 * data::milliseconds, 1000 * data::milliseconds);



    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin( window );
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    unsigned count=0;
    while(it_begin != it_end)
    {
        // Check output
        auto slice = *it_begin;
        auto start_time = it_begin.start_time();
        ASSERT_EQ(static_cast<std::size_t>(slice.size<data::Frequency>()),tf1.number_of_channels());
        ASSERT_EQ(static_cast<std::size_t>(slice.size<data::Time>()), 4); // 1ms before and after for windowing, 2 ms wide candidates = 4ms
        double ts = (pss::astrotypes::units::duration_cast<boost::units::quantity<MilliSeconds, double>>(start_time - tf1.start_time())).value();
        double cts = (cand_list[count].tstart() - window.ms_before()).value();
        ASSERT_DOUBLE_EQ(ts, cts);
        ASSERT_NO_THROW(++it_begin);
        ++count;
    }

    ASSERT_EQ(count,cand_list.size());
}

TEST_F(SpCclTest, test_candidate_extraction_begin_range)
{
    /*
     * @brief: Test case where the candidate is either at the beginning of the block
     *         or the end of the block. The test should resize the candidate window
     *         before extracting the slice of data.
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1*boost::units::si::seconds));
    float sigma=20.0;

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    unsigned n=0;
    std::generate(tf1.begin(),tf1.end(),[&](){return ++n;});

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to the beginning
    SpCandidateType::MsecTimeType tstart3(0.1 * boost::units::si::seconds);
    // set the end time
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);



    SpCandidateType candidate3(dm,tstart3,width,sigma, 3);
    candidate3.duration_end(tend);
    cand_list.push_back(candidate3);

    CandidateWindow window(150 * data::milliseconds,150 * data::milliseconds);

    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    //unsigned ind=0;
    auto tf_it  = tf1.begin();
    auto sl_it = (*it_begin).begin();


    for (uint32_t ii=0; ii < 5120 ; ++ii )
    {
        ASSERT_EQ(*sl_it,*tf_it);
        ++tf_it;
        ++sl_it;
    }

    unsigned count=0;
    while(it_begin != it_end)
    {
        ASSERT_NO_THROW(++it_begin);
        ++count;
    }

    ASSERT_EQ(count,cand_list.size());
}

TEST_F(SpCclTest, test_candidate_extraction_end_range)
{
    /*
     * @brief: Test case where the candidate is either at the beginning of the block
     *         or the end of the block. The test should resize the candidate window
     *         before extracting the slice of data.
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1*boost::units::si::seconds));
    float sigma=20.0;

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to the end
    SpCandidateType::MsecTimeType tstart(99.9 * boost::units::si::seconds);
    // set end time
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);

    SpCandidateType candidate(dm, tstart, width, sigma, 1);
    candidate.duration_end(tend);
    cand_list.push_back(candidate);

    CandidateWindow window(2000 * data::milliseconds,2000 * data::milliseconds);

    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    auto tf_it  = tf1.end();
    tf_it = tf_it - 2048;
    auto sl_it = (*it_begin).begin();

    for (uint32_t ii=0; ii < 2048; ++ii )
    {
        ASSERT_EQ(*sl_it,*tf_it);
        ++tf_it;
        ++sl_it;
    }
    unsigned count=0;
    while(it_begin != it_end)
    {
        ASSERT_NO_THROW(++it_begin);
        ++count;
    }

    ASSERT_EQ(count,cand_list.size());
}

TEST_F(SpCclTest, test_candidate_extraction_overlap)
{
    /*
     * @brief: Test when there are two candidates within the window of the first candidate.
     *         The expected behaviour is that the extractor should ignore the candidate i.e should not repeat the same data
     *         and move on to the next one.
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(1024));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1*boost::units::si::seconds));
    float sigma=20.0;

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1 ms
    SpCandidateType::MsecTimeType width(0.001 * boost::units::si::seconds);
    //set the single pulse start time to the beginning, middle and the end
    SpCandidateType::MsecTimeType tstart(30 * boost::units::si::seconds);
    SpCandidateType::MsecTimeType tstart2(29 * boost::units::si::seconds);
    SpCandidateType::MsecTimeType tstart3(31 * boost::units::si::seconds);
    // set tend
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);


    SpCandidateType candidate(dm, tstart, width, sigma, 1);
    SpCandidateType candidate2(dm,tstart2,width,sigma, 2);
    SpCandidateType candidate3(dm,tstart3,width,sigma, 3);
    candidate.duration_end(tend);
    candidate2.duration_end(tend);
    candidate3.duration_end(tend);
    cand_list.push_back(candidate);
    cand_list.push_back(candidate2);
    cand_list.push_back(candidate3);

    CandidateWindow window(1000 * data::milliseconds,1000 * data::milliseconds);

    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    unsigned count=0;
    while(it_begin != it_end)
    {
        ASSERT_NO_THROW(++it_begin);
        count++;
    }
    ASSERT_EQ(count,cand_list.size()-2);

}

TEST_F(SpCclTest, test_candidate_spanning_multiple_blocks)
{
    /*
     * @brief: Tests whether a candidate that spans multiple blocks is extracted properly.
     *         checks that the data extracted match with the candidate
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));
    TimeFrequency<Cpu, uint8_t> tf2(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));
    TimeFrequency<Cpu, uint8_t> tf3(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf2.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf3.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);
    std::fill(tf2.begin(),tf2.end(),1);
    std::fill(tf3.begin(),tf3.end(),2);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf2));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf3));

    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(1 * boost::units::si::milli * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(5 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;
    //set end time
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);



    SpCandidateType candidate(dm, tstart, width, sigma, 1);
    candidate.duration_end(tend);
    cand_list.push_back(candidate);

    CandidateWindow window(2000 * data::milliseconds,200000 * data::milliseconds);

    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    // Check if we get all the data where the candidate exists
    auto tf_it  = tf1.begin();
    tf_it = tf_it + 48;
    auto sl_it1 = (*it_begin).begin();
    for (uint32_t ii=0; ii < 1552; ++ii)
    {
        ASSERT_EQ(*sl_it1,*tf_it);
        ++sl_it1;
        ++tf_it;
    }
    ++it_begin;
    auto sl_it2 = (*it_begin).begin();
    auto tf_it2 = tf2.begin();
    for (uint32_t ii=0; ii < 1600; ++ii)
    {
        ASSERT_EQ(*sl_it2,*tf_it2);
        ++sl_it2;
        ++tf_it2;
    }
    ++it_begin;
    auto sl_it3 = (*it_begin).begin();
    auto tf_it3 = tf3.begin();
    for (uint32_t ii=0 ; ii < 112; ++ii)
    {
        ASSERT_EQ(*sl_it3,*tf_it3);
        ++sl_it3;
        ++tf_it3;
    }

    ASSERT_NO_THROW(++it_begin);

}

TEST_F(SpCclTest, test_candidate_start_beyond_end)
{
    /*
     * @brief: Tests whether a candidate that starts beyond the end of block throws an exception.
     *
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));

    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(1 * boost::units::si::milli * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(105 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;
    //set end time
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);



    SpCandidateType candidate(dm, tstart, width, sigma, 1);
    candidate.duration_end(tend);
    cand_list.push_back(candidate);

    CandidateWindow window(2000 * data::milliseconds,2000 * data::milliseconds);

    //SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    //SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    // Check if we exception is thrown
    ASSERT_ANY_THROW(cand_list.data_begin(window));

}

TEST_F(SpCclTest, test_no_candidates)
{
/**
 *@brief  Test for when there are no candidate, the object should return just iterator equal to the end iterator and not do any extraction
 */
    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list;
    CandidateWindow window;
    ASSERT_NO_THROW(cand_list.data_begin(window));
    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();
    while (it_begin != it_end)
    {
        ASSERT_NO_THROW(++it_begin);
    }

}

TEST_F(SpCclTest, test_offset_time)
{
    typedef SpCcl<uint8_t> SpCclType;
    typedef SpCclType::BlocksType BlocksType;
    typedef BlocksType::value_type::element_type TimeFrequencyType;

    TimeFrequencyType::TimeType sample_interval(64 * boost::units::si::milli * boost::units::si::seconds);

    BlocksType tf_v;
    typename std::remove_const<TimeFrequencyType>::type tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));
    tf1.sample_interval(sample_interval);
    tf_v.push_back(std::make_shared<TimeFrequencyType>(tf1));

    for(data::DimensionIndex<data::Time> offset(0); offset < tf1.number_of_spectra(); ++offset) {
        SpCclType cand_list(tf_v, offset);
        ASSERT_DOUBLE_EQ(pss::astrotypes::units::duration_cast<TimeFrequencyType::TimeType>(cand_list.offset_time()).value(), (offset * sample_interval).value());
    }
}

TEST_F(SpCclTest, test_candidate_start_multiple_blocks_behind)
{
    /*
     * @brief: Tests whether a candidate that has the window spanning multiple blocks behind the actual start time is extracted properly.
     *         checks that the data extracted match with the candidate
     */
    typedef SpCandidate<Cpu, float> SpCandidateType;
    typedef SpCcl<uint8_t>::BlocksType::value_type::element_type TimeFreqType;
    TimeFrequency<Cpu, uint8_t> tf1(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));
    TimeFrequency<Cpu, uint8_t> tf2(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));
    TimeFrequency<Cpu, uint8_t> tf3(data::DimensionSize<data::Time>(100),data::DimensionSize<data::Frequency>(16));

    tf1.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf2.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));
    tf3.sample_interval(data::TimeFrequency<Cpu, uint8_t>::TimeType(1000 * boost::units::si::milli * boost::units::si::seconds));

    SpCcl<uint8_t>::BlocksType tf_v;
    // fill data
    std::fill(tf1.begin(),tf1.end(),0);
    std::fill(tf2.begin(),tf2.end(),1);
    std::fill(tf3.begin(),tf3.end(),2);

    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf1));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf2));
    tf_v.push_back(std::make_shared<TimeFrequency<Cpu, uint8_t>>(tf3));

    // Generate SpCcl instance
    SpCcl<uint8_t> cand_list(tf_v);

    // Generate Candidates
    typename SpCandidateType::Dm dm(12.0 * parsecs_per_cube_cm);
    //set the single pulse candidate width to 1.24 ms
    SpCandidateType::MsecTimeType width(1 * boost::units::si::milli * boost::units::si::seconds);
    //set the single pulse start time to 2.23 seconds
    SpCandidateType::MsecTimeType tstart(250 * boost::units::si::seconds);
    //set the candidate significance
    float sigma = 20.0;
    //set end time
    SpCandidateType::MsecTimeType tend(2000 * boost::units::si::milli * boost::units::si::seconds);



    SpCandidateType candidate(dm, tstart, width, sigma, 1);
    candidate.duration_end(tend);
    cand_list.push_back(candidate);

    CandidateWindow window(200000 * data::milliseconds,10000 * data::milliseconds);

    SpCcl<uint8_t>::ConstDataIterator it_begin = cand_list.data_begin(window);
    SpCcl<uint8_t>::ConstDataIterator it_end = cand_list.data_end();

    // Check if we get all the data where the candidate exists
    auto tf_it  = tf1.begin();
    tf_it = tf_it + 800;
    auto sl_it1 = (*it_begin).begin();
    for (uint32_t ii=0; ii < 800; ++ii)
    {
        ASSERT_EQ(*sl_it1,*tf_it);
        ++sl_it1;
        ++tf_it;
    }
    ++it_begin;
    auto sl_it2 = (*it_begin).begin();
    auto tf_it2 = tf2.begin();
    for (uint32_t ii=0; ii < 1600; ++ii)
    {
        ASSERT_EQ(*sl_it2,*tf_it2);
        ++sl_it2;
        ++tf_it2;
    }
    ++it_begin;
    auto sl_it3 = (*it_begin).begin();
    auto tf_it3 = tf3.begin();
    for (uint32_t ii=0 ; ii < 992; ++ii)
    {
        ASSERT_EQ(*sl_it3,*tf_it3);
        ++sl_it3;
        ++tf_it3;
    }

    ASSERT_NO_THROW(++it_begin);

}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
