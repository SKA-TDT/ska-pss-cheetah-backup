/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/cuda/test/TimeFrequencyTest.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/FrequencyTime.h"
#include "panda/Copy.h"
#include <random>

namespace ska {
namespace cheetah {
namespace data {
namespace cuda {
namespace test {

template <typename T>
TimeFrequencyTest<T>::TimeFrequencyTest()
    : ::testing::Test()
{
}

template <typename T>
TimeFrequencyTest<T>::~TimeFrequencyTest()
{
}

template <typename T>
void TimeFrequencyTest<T>::SetUp()
{
}

template <typename T>
void TimeFrequencyTest<T>::TearDown()
{
}

template <class Arch, typename NumericalRepT, typename OtherNumericalRepT>
class DataTypes
{
    public:
        typedef Arch Architecture;
        typedef NumericalRepT FirstType;
        typedef NumericalRepT SecondType;
};

typedef ::testing::Types<DataTypes<Cuda, uint8_t, uint8_t>,
                        DataTypes<Cuda, uint8_t, uint16_t>,
                        DataTypes<Cuda, uint8_t, float>,
                        DataTypes<Cuda, uint16_t, uint8_t>,
                        DataTypes<Cuda, uint16_t, uint16_t>,
                        DataTypes<Cuda, uint16_t, float>,
                        DataTypes<Cuda, float, uint8_t>,
                        DataTypes<Cuda, float, uint16_t>,
                        DataTypes<Cuda, float, float>,
                        DataTypes<Cpu, uint8_t, uint8_t>,
                        DataTypes<Cpu, uint8_t, uint16_t>,
                        DataTypes<Cpu, uint8_t, float>,
                        DataTypes<Cpu, uint16_t, uint8_t>,
                        DataTypes<Cpu, uint16_t, uint16_t>,
                        DataTypes<Cpu, uint16_t, float>,
                        DataTypes<Cpu, float, uint8_t>,
                        DataTypes<Cpu, float, uint16_t>,
                        DataTypes<Cpu, float, float>> TimeFrequencyTypes;

TYPED_TEST_CASE(TimeFrequencyTest, TimeFrequencyTypes);

TYPED_TEST(TimeFrequencyTest, test_copy)
{
    typedef typename TypeParam::Architecture Arch;
    typedef typename TypeParam::FirstType NumericalRep;
    typedef typename TypeParam::SecondType OtherNumericalRep;

    typedef TimeFrequency<Cpu, NumericalRep> TfDataType;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> nspectra(8, 1024);
    std::uniform_int_distribution<int> nchans(8, 32);

    data::DimensionSize<data::Time> number_of_spectra(nspectra(mt));
    data::DimensionSize<data::Frequency> number_of_channels(nchans(mt));

    TfDataType tf_host_data(number_of_spectra, number_of_channels);

    srand((unsigned) time(0));
    std::generate(tf_host_data.begin(), tf_host_data.end(), [&]() { return (rand()%255); });

    auto f1 =  typename data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(100.0 * boost::units::si::hertz);
    auto f2 =  typename data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(200.0 * boost::units::si::hertz);
    typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(50000.0));
    auto delta = (f2 - f1)/ (double)(number_of_channels-1);

    tf_host_data.set_channel_frequencies_const_width( f1, delta);
    tf_host_data.start_time(epoch);

    TimeFrequency<Arch, NumericalRep> input_data(tf_host_data);
    TimeFrequency<Cuda, OtherNumericalRep> output_data(input_data);
    TimeFrequency<Cpu, OtherNumericalRep> output_data_copy(output_data);
    TimeFrequency<Cpu, NumericalRep> input_data_copy(tf_host_data);

    for(unsigned i=0; i < input_data_copy.number_of_spectra(); ++i)
    {
        // iterate over channels
        typename TimeFrequency<Cpu, NumericalRep>::Spectra input_spectrum = input_data_copy.spectrum(i);
        auto input_channel_it = input_spectrum.begin();
        typename TimeFrequency<Cpu,OtherNumericalRep>::Spectra output_spectrum = output_data_copy.spectrum(i);
        auto output_channel_it = output_spectrum.begin();
        while(input_channel_it != input_spectrum.end())
        {
            ASSERT_EQ(*input_channel_it, *output_channel_it);
            ++input_channel_it;
            ++output_channel_it;
        }
    }
}


TYPED_TEST(TimeFrequencyTest, test_cornerturn)
{
    typedef typename TypeParam::Architecture Arch;
    typedef typename TypeParam::FirstType NumericalRep;
    typedef typename TypeParam::SecondType OtherNumericalRep;
    typedef FrequencyTime<Cpu, NumericalRep> FtDataType;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> nspectra(8, 1024);
    std::uniform_int_distribution<int> nchans(8, 32);

    data::DimensionSize<data::Time> number_of_spectra(nspectra(mt));
    data::DimensionSize<data::Frequency> number_of_channels(nchans(mt));

    FtDataType tf_host_data(number_of_spectra, number_of_channels);
    srand((unsigned) time(0));
    std::generate(tf_host_data.begin(), tf_host_data.end(), [&]() { return (rand()%255); });

    auto f1 =  typename data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(100.0 * boost::units::si::hertz);
    auto f2 =  typename data::TimeFrequency<Cpu, NumericalRep>::FrequencyType(200.0 * boost::units::si::hertz);
    typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(50000.0));
    auto delta = (f2 - f1)/ (double)(number_of_channels-1);

    tf_host_data.set_channel_frequencies_const_width( f1, delta);
    tf_host_data.start_time(epoch);

    FrequencyTime<Arch, NumericalRep> input_data(tf_host_data);
    TimeFrequency<Cuda, OtherNumericalRep> output_data(input_data);
    TimeFrequency<Cpu, OtherNumericalRep> output_data_copy(output_data);
    TimeFrequency<Cpu, NumericalRep> input_data_copy(input_data);

    for(unsigned i=0; i < input_data_copy.number_of_spectra(); ++i)
    {
        // iterate over channels
        typename TimeFrequency<Cpu, NumericalRep>::Spectra input_spectrum = input_data_copy.spectrum(i);
        auto input_channel_it = input_spectrum.begin();
        typename TimeFrequency<Cpu,OtherNumericalRep>::Spectra output_spectrum = output_data_copy.spectrum(i);
        auto output_channel_it = output_spectrum.begin();
        while(input_channel_it != input_spectrum.end())
        {
            ASSERT_EQ(*input_channel_it, *output_channel_it);
            ++input_channel_it;
            ++output_channel_it;
        }
    }
}

} // namespace test
} // namespace cuda
} // namespace data
} // namespace cheetah
} // namespace ska