#include "cheetah/fft/cuda/Fft.cuh"
#include "cheetah/fft/test_utils/FftTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {
namespace test {

template<typename NumericalT>
struct CudaTraitsBase
    : public fft::test::FftTesterTraits<fft::cuda::Fft, NumericalT>
{
        typedef fft::test::FftTesterTraits<fft::cuda::Fft, NumericalT> BaseT;
        typedef Fft::Architecture Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        template<typename DataType>
        typename DataType::Allocator allocator(panda::PoolResource<Arch>&) {
            return typename DataType::Allocator();
        }
};

template<typename NumericalT>
struct CudaTraits : CudaTraitsBase<NumericalT>
{
};

template<>
struct CudaTraits<float> : CudaTraitsBase<float>
{
    static double accuracy() { return 5.0e-5; }
};

} // namespace test
} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits<float>, cuda::test::CudaTraits<double>> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, FftTester, CudaTraitsTypes);

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska
