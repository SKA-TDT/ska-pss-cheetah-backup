#ifndef SKA_CHEETAH_FFT_FFTPLAN_H
#define SKA_CHEETAH_FFT_FFTPLAN_H

#include "cheetah/fft/FftType.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/cuda_utils/cuda_cufft.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"

namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {

/**
 * Implementation of FftPlan for cuFFT plans
 */
class FftPlan
{
    public:
        /**
         * @brief      Construct an uninitialised cufft plan.
         */
        FftPlan();

        /**
         * @brief      Destroys the undetlying cufft plan if allocated.
         */
        ~FftPlan();

        /**
         * @brief      Get (or create) a cufft plan.
         *
         * @details    The plan method will first check if its underlying plan matches
         *             the provided arguments. If it matches, a reference to the existing
         *             plan is returned. If the arguments do not match the configuration of
         *             the exiting plan, the existing plan is destroyed and a new one is created.
         *
         * @param[in]  fft_type  The fft type (R2C, C2R or C2C)
         * @param[in]  size      The size of the transform
         * @param[in]  batch     The number of transforms to batch
         *
         * @tparam     T         Base data type being transformed (float or double)
         *
         * @return     A cufftHanle representing the current plan
         */
        template <typename T>
        cufftHandle const& plan(FftType fft_type, std::size_t size, std::size_t batch);

    private:
        /**
         * @brief      Test if current _plan instance matches the input parameters
         **/
        bool valid(cufftType cufft_type, std::size_t size, std::size_t batch) const;

        /**
         * @brief      Destroy the current plan and set all parameters to their defaults
         */
        void destroy_plan();

        /**
         * @brief      Convert from a cheetah FffType to the corresponding cufftType
         */
        template <typename T>
        cufftType convert_to_cufft_type(FftType fft_type) const;

    private:
        cufftHandle _plan;
        cufftType _cufft_type;
        std::size_t _size;
        std::size_t _batch;
};

} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/cuda/detail/FftPlan.cu"

#endif // SKA_CHEETAH_FFT_FFTPLAN_H
