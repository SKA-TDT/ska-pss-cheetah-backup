#ifndef SKA_CHEETAH_FFT_CUDA_FFT_H
#define SKA_CHEETAH_FFT_CUDA_FFT_H

#include "cheetah/fft/FftType.h"
#include "cheetah/fft/cuda/FftPlan.cuh"
#include "cheetah/fft/cuda/Config.h"
#include "cheetah/fft/Config.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/utils/AlgorithmBase.h"

#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {

/**
 * @brief      A cuda-specific implementation of the fft module
 *
 * @details    This class provides and interface to Nvidia's cuFFT library
 *             wrapping plan generation and execution and providing a single
 *             overloaded method to simplify the process of performing FFTs.
 *
 *             It should be noted that for performance reasons it is best to
 *             perform repetitions of the same transform size and type as this
 *             will not result in reallocation of the FftPlan object and regeneration
 *             of a cuFFT plan.
 *
 *             It is therefore recommended that separate instantiations should be
 *             created for forward and back transforms and for repeated transforms
 *             of the same size the object should persist between transforms.
 *
 *             Note that cuFFT rescales such that iFFT(FFT(A)) = size(A)*A for complex
 *             transforms and  iFFT(FFT(A)) = sqrt(size(A))*A for a real to complex followed
 *             by complex to real transform.
 */

class Fft: utils::AlgorithmBase<Config, fft::Config>
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapability;
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        /**
         * @brief      Construct and Fft instance
         *
         * @param[in]  config    A configuration object for the Fft instance
         */
        Fft(fft::Config const& algo_config);
        Fft(Fft const&) = delete;
        Fft(Fft&&) = default;

        /**
         * @brief      Perform a real-to-complex 1D FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *             output size of the transform. For this reason it is most performant to
         *             reuse an output object of the correct size for each call to this method.
         *             The method also updates meta data associated with its output based on
         *             metadata associated with its input.
         *
         * @param[in]      gpu          A PoolResource object of architecture type Cuda.
         *                              The object contains information about the selected GPU
         *                              and the current context.
         * @param[in]      input        A real TimeSeries instance to be transformed
         * @param[out]     output       A complex FrequencySeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& gpu,
            data::TimeSeries<cheetah::Cuda, T, InputAlloc> const& input,
            data::FrequencySeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type, OutputAlloc>& output);

        /**
         * @brief      Perform a complex-to-real 1D FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      gpu          A PoolResource object of architecture type Cuda.
         *                              The object contains information about the selected GPU
         *                              and the current context.
         * @param[in]      input        A complex FrequencySeries instance to be transformed
         * @param[out]     output       A real TimeSeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& gpu,
            data::FrequencySeries<cheetah::Cuda,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
            data::TimeSeries<cheetah::Cuda,T,OutputAlloc>& output);

        /**
         * @brief      Perform a complex-to-complex 1D forward FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      gpu          A PoolResource object of architecture type Cuda.
         *                              The object contains information about the selected GPU
         *                              and the current context.
         * @param[in]      input        A complex TimeSeries instance to be transformed
         * @param[out]     output       A complex FrequencySeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& gpu,
            data::TimeSeries<cheetah::Cuda, thrust::complex<T>, InputAlloc> const& input,
            data::FrequencySeries<cheetah::Cuda,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,OutputAlloc>& output);

        /**
         * @brief      Perform a complex-to-complex 1D inverse FFT
         *
         * @details     The output object will be resized appropriately to match the expected
         *              output size of the transform. For this reason it is most performant to
         *              reuse an output object of the correct size for each call to this method.
         *              The method also updates meta data associated with its output based on
         *              metadata associated with its input.
         *
         * @param[in]      gpu          A PoolResource object of architecture type Cuda.
         *                              The object contains information about the selected GPU
         *                              and the current context.
         * @param[in]      input        A complex FrequencySeries instance to be transformed
         * @param[out]     output       A complex TimeSeries instance for the transform output
         *
         * @tparam     T            The base value type for the transform (float or double)
         * @tparam     InputAlloc   The allocator type of the input
         * @tparam     OutputAlloc  The allocator type of the output
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& gpu,
            data::FrequencySeries<cheetah::Cuda, thrust::complex<T>, InputAlloc> const& input,
            data::TimeSeries<cheetah::Cuda,typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,OutputAlloc>& output);

    private:
        FftPlan _plan;
};

} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/cuda/detail/Fft.cu"

#endif // SKA_CHEETAH_FFT_CUDA_FFT_H
