/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/altera/detail/FftWorker.h"
#include "cheetah/fft/altera/Fft.h"
#include "panda/Error.h"
#include "panda/Log.h"
#ifdef ENABLE_OPENCL
#include "panda/arch/altera/Aocx.h"
#include "panda/arch/altera/Program.h"
#endif // ENABLE_OPENCL


namespace ska {
namespace cheetah {
namespace fft {
namespace altera {


#ifdef ENABLE_OPENCL
FftWorker::FftWorker(fft::Config const& config, panda::PoolResource<Fpga> const& device)
    : _config(config)
    , _device(device)
    , _first_queue(new panda::altera::CommandQueue(_device))
    , _fetch_queue(new panda::altera::CommandQueue(_device))
    , _fetch_mwt_queue(new panda::altera::CommandQueue(_device))
    , _fft_queue(new panda::altera::CommandQueue(_device))
    , _transpose_queue(new panda::altera::CommandQueue(_device))
    , _transpose_mwt_queue(new panda::altera::CommandQueue(_device))
    , _data_queue(new panda::altera::CommandQueue(_device))
    , _last_queue(new panda::altera::CommandQueue(_device))
{
    std::string filename;
    const std::string str = _device.device_name();
    filename=_cxft.eight_million_point_image(_device.device_id());
    PANDA_LOG << "\nRabbit CXFT image: " << filename << " found for the device " << str;
    ska::panda::altera::Aocx aocx(filename);
    ska::panda::altera::Program program(aocx,_device);
    _first_kernel.reset(new panda::altera::Kernel("first_stage", program));
    _fetch_kernel.reset(new panda::altera::Kernel("fetch", program));
    _fetch_mwt_kernel.reset(new panda::altera::Kernel("fetch_MWT", program));
    _fft_kernel.reset(new panda::altera::Kernel("fft1d", program));
    _transpose_kernel.reset(new panda::altera::Kernel("transpose", program));
    _transpose_mwt_kernel.reset(new panda::altera::Kernel("transpose_MWT", program));
    _last_kernel.reset(new panda::altera::Kernel("last_stage", program));
}
#endif // ENABLE_OPENCL

} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska
