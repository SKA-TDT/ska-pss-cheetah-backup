/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FFT_ALTERA_FFTWORKER_H
#define SKA_CHEETAH_FFT_ALTERA_FFTWORKER_H

#ifdef ENABLE_OPENCL
#include "cheetah/fft/Config.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/CommandQueue.h"
#include "rabbit/cxft/Cxft.h"


namespace ska {
namespace cheetah {
namespace fft {
namespace altera {

class FftWorker
{
    public:
        typedef typename cheetah::Fpga Architecture;
        template<typename T>
        using Complex = typename data::ComplexTypeTraits<Architecture, T>::type;

    public:
        FftWorker(fft::Config const& config, panda::PoolResource<Fpga> const& device);

        //Copy constructors removed.
        FftWorker(FftWorker const&) = delete;
        FftWorker(FftWorker&&) = delete;

        template <typename T, typename InputAlloc, typename OutputAlloc>
        void operator()(data::TimeSeries<cheetah::Fpga , T, InputAlloc> const& input,
                        data::FrequencySeries<cheetah::Fpga, Complex<T>, OutputAlloc>& output);

    private:
        fft::Config const& _config;
        panda::PoolResource<panda::altera::OpenCl> const& _device;
        std::unique_ptr<panda::altera::Kernel> _fetch_kernel;
        std::unique_ptr<panda::altera::Kernel> _fetch_mwt_kernel;
        std::unique_ptr<panda::altera::Kernel> _fft_kernel;
        std::unique_ptr<panda::altera::Kernel> _transpose_kernel;
        std::unique_ptr<panda::altera::Kernel> _transpose_mwt_kernel;
        std::unique_ptr<panda::altera::Kernel> _last_kernel;
        std::unique_ptr<panda::altera::Kernel> _first_kernel;
        std::unique_ptr<panda::altera::CommandQueue> _first_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fetch_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fetch_mwt_queue;
        std::unique_ptr<panda::altera::CommandQueue> _fft_queue;
        std::unique_ptr<panda::altera::CommandQueue> _transpose_queue;
        std::unique_ptr<panda::altera::CommandQueue> _transpose_mwt_queue;
        std::unique_ptr<panda::altera::CommandQueue> _data_queue;
        std::unique_ptr<panda::altera::CommandQueue> _last_queue;
        ska::rabbit::cxft::Cxft _cxft;
};


} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/altera/detail/FftWorker.cpp"

#endif // ENABLE_OPENCL
#endif // SKA_CHEETAH_FFT_ALTERA_FFTWORKER_H
