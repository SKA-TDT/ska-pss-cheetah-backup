/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/altera/detail/FftWorker.h"
#include "cheetah/fft/altera/Fft.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include "panda/Copy.h"
#ifdef ENABLE_OPENCL
#include "panda/arch/altera/DevicePointer.h"
#include "panda/arch/altera/DeviceCopy.h"
#endif // ENABLE_OPENCL
#include <cmath>


namespace ska {
namespace cheetah {
namespace fft {
namespace altera {

#ifdef ENABLE_OPENCL
template <typename T, typename InputAlloc, typename OutputAlloc>
void FftWorker::operator()(data::TimeSeries<cheetah::Fpga, T, InputAlloc> const& input
                          , data::FrequencySeries<cheetah::Fpga, FftWorker::Complex<T>
                          , OutputAlloc>& output)
{
    auto const& twiddles=_cxft.eight_million_point().twiddles();
    if(input.size() > twiddles.size()){
        panda::Error e("Invalid FFT size! Kernel compiled for the size:  ");
        e << twiddles.size();
        throw e;
    }
    size_t half_fft_size=twiddles.size()/2, sqrt_half_fft=int(sqrt(half_fft_size));

    panda::altera::DevicePointer<Complex<T>> dev_in_ev(_device, half_fft_size, *_first_queue);
    panda::altera::DevicePointer<Complex<T>> dev_in_od(_device, half_fft_size, *_first_queue);
    panda::altera::DevicePointer<Complex<T>> dev_temp(_device
                                                     , half_fft_size,*_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_sig_ev(_device
                                                       , half_fft_size, *_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_sig_od(_device
                                                       , half_fft_size, *_data_queue);
    panda::altera::DevicePointer<Complex<T>> dev_twd(_device
                                                    , twiddles.size(), *_last_queue);
    panda::copy(twiddles.cbegin(), twiddles.cend(), dev_twd.begin());

    /*
     * @details: first stage separates even & odd samples of data,
     * explicit finishing of the _first_queue makes even & odd input buffers available to the FFT kernels
     */
    (*_first_kernel)(*_first_queue, 1, 1, static_cast<cl_mem>(input.begin())
                    , (&*dev_in_ev), (&*dev_in_od), twiddles.size());
    (*_first_queue).finish();

    /*
     * @details: FFT kernels for 4M samples called for four times (2 loops)
     * even FFT, odd FFT consisting of row operation+column operations
     * even samples j=0; odd samples j=1; i=0 row FFT; i=1 column FFT
     * explicit finishing of the _transpose_queue makes even & odd output buffers available to the last stage
     * multiwire archietecture makes this FFT faster
     */
    cl_int mangle_int=0, twidle_int=1, inverse_int=0;
    cl_uint rows_arg=sqrt_half_fft, columns_arg=sqrt_half_fft;
    cl_int log_rows_arg=log2(sqrt_half_fft), log_columns_arg=log2(sqrt_half_fft);
    int columns = (1 << log_columns_arg);
    int rows = (1 << log_rows_arg);
    float delta_const = -2.0f * (float)M_PI / (columns * rows);
    for (int j = 0; j < 2; ++j){
        for (int i = 0; i < 2; ++i){
            twidle_int = !i;
            (*_fetch_kernel)(*_fetch_queue, 1, 1
                            , i == 0 ? (j == 0 ?(&*dev_in_ev):(&*dev_in_od)) : (&*dev_temp)
                            , mangle_int, twidle_int
                            , log_rows_arg, log_columns_arg, rows_arg, columns_arg);
            (*_fetch_mwt_kernel)(*_fetch_mwt_queue,1, 1, log_rows_arg, log_columns_arg, twidle_int);
            (*_fft_kernel)(*_fft_queue,1, 1, inverse_int
                          , log_rows_arg, log_columns_arg, rows_arg, columns_arg);
            (*_transpose_mwt_kernel)(*_transpose_mwt_queue, 1, 1, log_rows_arg, log_columns_arg);
            (*_transpose_kernel)(*_transpose_queue, 1, 1
                                , i == 0 ? (&*dev_temp) : (j == 0 ? (&*dev_sig_ev) :(&*dev_sig_od))
                                , mangle_int, twidle_int, inverse_int
                                , log_rows_arg, log_columns_arg, rows_arg, columns_arg, delta_const);
        }
    }
    (*_transpose_mwt_queue).finish();
    (*_transpose_queue).finish();

    /*
     * @details: last stage implements twiddle multiplications for 8M data,
     * it doesn't compute one side of the spectrum for real to complex transform, saving resources
     */
    (*_last_kernel)(*&(_device.default_queue()), 1, 1
                   , (&*dev_sig_ev), (&*dev_sig_od), (&*dev_twd)
                   , static_cast<cl_mem>(output.begin()), half_fft_size);

}
#else // ENABLE_OPENCL
template <typename T, typename InputAlloc, typename OutputAlloc>
void FftWorker::operator()(data::TimeSeries<cheetah::Fpga , T, InputAlloc> const&,
                            data::FrequencySeries<cheetah::Fpga, FftWorker::Complex<T>, OutputAlloc>&)
{
}
#endif // ENABLE_OPENCL

} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska
