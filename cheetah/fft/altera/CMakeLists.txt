
if(ENABLE_OPENCL)
    set(altera_lib_src
        src/Fft.cpp
        src/FftWorker.cpp
    )
endif(ENABLE_OPENCL)

set(module_altera_lib_src_cpu
    src/Config.cpp
    ${altera_lib_src}
    PARENT_SCOPE
    )

if(ENABLE_OPENCL)
add_subdirectory(test)
endif(ENABLE_OPENCL)
