/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FFT_ALTERA_FFT_H
#define SKA_CHEETAH_FFT_ALTERA_FFT_H

#ifdef ENABLE_OPENCL
#include "cheetah/fft/altera/detail/FftWorker.h"
#endif // ENABLE_OPENCL

#include "cheetah/fft/altera/Config.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/utils/Architectures.h"
#include "panda/DeviceLocal.h"

namespace ska {
namespace cheetah {
namespace fft {
class Config;
namespace altera {

/**
 * @brief
 *      Intel FPGA OpenCL FFT interface
 * @details
 *      Creates a worker object per device available
 */

#ifdef ENABLE_OPENCL
class Fft: utils::AlgorithmBase<Config, fft::Config>
{

    public:
        typedef typename cheetah::Fpga Architecture;
        template<typename T>
        using ComplexT = typename data::ComplexTypeTraits<Architecture,T>::type;

    private:
        typedef panda::PoolResource<Architecture> ResourceType;

    public:
        Fft(fft::Config const& algo_config);
        Fft(Fft const&) = delete;
        Fft(Fft&&) = default;

        /**
         * @details Real to Complex specialization of the altera FFT
         */
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::TimeSeries<cheetah::Fpga,T,InputAlloc> const& input,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output);

        /*
         * commented FFT types for future scope: C2R, C2C_fwd, C2C_inv
         */
        /*
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, InputAlloc> const& input,
                    data::TimeSeries<cheetah::Fpga,T,OutputAlloc>& output);
                    */

        /*
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::TimeSeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
                    data::FrequencySeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output);
                    */

        /*
        template <typename T, typename InputAlloc, typename OutputAlloc>
        void process(ResourceType& fpga,
                    data::FrequencySeries<cheetah::Fpga, std::complex<T>, InputAlloc> const& input,
                    data::TimeSeries<cheetah::Fpga, ComplexT<T>, OutputAlloc>& output);
                    */

    private:
        struct WorkerFactory {
            public:
                WorkerFactory(fft::Config const&);
                FftWorker* operator()(panda::PoolResource<Architecture> const& device);

            private:
                fft::Config const& _config;
        };

    private:
        panda::DeviceLocal<panda::PoolResource<Architecture>, WorkerFactory> _workers;

};
#else // ENABLE_OPENCL

// Dummy class
class Fft
{
    public:
        typedef typename cheetah::Fpga Architecture;

    public:
        Fft(fft::Config const&) {};
};
#endif // ENABLE_OPENCL

} // namespace altera
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/altera/detail/Fft.cpp"

#endif // SKA_CHEETAH_FFT_ALTERA_CXFT_H
