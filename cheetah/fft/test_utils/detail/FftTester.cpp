/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/test_utils/FftTester.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "panda/Log.h"
#include "panda/TypeTraits.h"

#include <functional>

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

namespace {
    // Utilities to determine which process methods ar instantiated
    template<typename TraitsT
            , typename InputType
            , typename OutputType>
    class HasFftProcessMethod
    {
        private:
            template<typename Algo>
            using HasC2C_t = decltype(std::declval<Algo>().process(std::declval<typename TraitsT::DeviceType&>(),
                                                                   std::declval<InputType const&>(),
                                                                   std::declval<OutputType&>()
                                                                   ));
            typedef typename TraitsT::Algo Algorithm;

        public:
            static const bool value = panda::HasMethod<Algorithm, HasC2C_t>::value;
    };

    template<typename TypeParam, typename InputValueT, typename OutputValueT>
    using HasTime2Frequency = HasFftProcessMethod< TypeParam
                                                 , data::TimeSeries<typename TypeParam::Arch
                                                                   , InputValueT>
                                                 , data::FrequencySeries<typename TypeParam::Arch
                                                                   , OutputValueT>
                                                 >;
    template<typename TypeParam, typename InputValueT, typename OutputValueT>
    using HasFrequency2Time = HasFftProcessMethod< TypeParam
                                                 , data::FrequencySeries<typename TypeParam::Arch
                                                                   , InputValueT>
                                                 , data::TimeSeries<typename TypeParam::Arch
                                                                   , OutputValueT>
                                                 >;
    // --- helper tests
    struct TestDevice {};
    struct TestArch {};
    struct A {
    };
    struct B{
        void process( TestDevice&
                    , data::TimeSeries<TestArch, float> const&
                    , data::FrequencySeries<TestArch, std::complex<float>>&) {}
    };
    template<class T>
    struct TypeParam {
        typedef TestDevice DeviceType;
        typedef TestArch Arch;
        typedef T Algo;
    };
    static_assert(!HasTime2Frequency<TypeParam<A>, float, std::complex<float>>::value, "not detecting process method");
    static_assert(HasTime2Frequency<TypeParam<B>, float, std::complex<float>>::value, "not detecting process method");

    // -- Base type for Real to complex type tests
    template<class DerivedType, typename TypeParam, typename Enable=void>
    struct R2C_Base
    {
        static inline
        void exec(typename TypeParam::DeviceType& device)
        {
            DerivedType::run_test(device);
        }
    };

    template<class DerivedType, typename TypeParam>
    struct R2C_Base<DerivedType, TypeParam
                     , typename std::enable_if<
                                     !HasTime2Frequency<TypeParam
                                                       , typename TypeParam::NumericalRep
                                                       , typename TypeParam::ComplexT
                                                       >::value
                                              >::type>
    {
        static inline
        void exec(typename TypeParam::DeviceType&)
        {
            std::cout << "Real to Complex FT not defined for this device";
        }
    };

    template<typename TypeParam, class InputType, class OutputType
           , typename Enable=void>
    struct ProcessHelper
    {
        static inline
        bool exec(TypeParam& traits
                , typename TypeParam::DeviceType& device
                , InputType const& input
                , OutputType& output
                )
        {
            traits.api().process(device, input, output);
            return true;
        }
    };

    template<typename TypeParam, class InputType, class OutputType>
    struct ProcessHelper<TypeParam, InputType, OutputType
                        , typename std::enable_if<
                                       !HasFftProcessMethod<TypeParam, InputType, OutputType>::value>::type>
    {
        static inline
        bool exec(TypeParam&
                , typename TypeParam::DeviceType&
                , InputType const&
                , OutputType&
                )
        {
            return false;
        }
    };

    /*
     * @brief  attemps to invoke the process() method on the API
     * @details if method exists it will be executed
     * @returns true : if process method exists
     *          false: otherwise
     */
    template<typename TypeParam, class InputType, class OutputType>
    bool process(TypeParam& traits
                , typename TypeParam::DeviceType& device
                , InputType const& input
                , OutputType& output)
    {
        return ProcessHelper<TypeParam, InputType, OutputType>::exec(traits, device, input, output);
    }

} // namespace

template<typename Algorithm, typename NumericalT>
FftTesterTraits<Algorithm, NumericalT>::FftTesterTraits()
    : _api(_config)
{
}

template<typename Algorithm, typename NumericalT>
fft::Fft& FftTesterTraits<Algorithm, NumericalT>::api()
{
    return _api;
}

template <typename TestTraits>
FftTester<TestTraits>::FftTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
FftTester<TestTraits>::~FftTester()
{
}

template<typename TestTraits>
void FftTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void FftTester<TestTraits>::TearDown()
{
}

template<typename TypeParam>
struct FftR2C_ZeroTest : public R2C_Base<FftR2C_ZeroTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputTypeHost;
        typedef data::FrequencySeries<Cpu, ComplexT> OutputTypeHost;
        std::size_t fft_len=TypeParam::fft_trial_length;

        InputTypeHost data_in(data::TimeType(0.003 * data::seconds), fft_len);
        std::fill(data_in.begin(), data_in.end(), 0);
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));
        traits.api().process(device, input, output);
        ASSERT_EQ(output.size(), input.size()/2 + 1);
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));
        OutputTypeHost data_out(output);
        for(auto const& elm : data_out){
            ASSERT_FLOAT_EQ(elm.real(), 0.0);
            ASSERT_FLOAT_EQ(elm.imag(), 0.0);
        }

        // perform inverse transform
        InputType inverse(traits.template allocator<InputType>(device));
        if(! process(traits, device, output, inverse))
        {
            std::cout << "Inverse transform not availaable - skipping test";
            return;
        }

        InputTypeHost inverse_host(inverse);
        ASSERT_EQ(inverse_host.size(), data_in.size());
        auto inv_it=inverse_host.begin();
        std::size_t count=0;
        for(auto const& elm : data_in)
        {
            SCOPED_TRACE(std::to_string(count));
            ASSERT_FLOAT_EQ(elm, *inv_it);
            ++inv_it;
            ++count;
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_zero)
{
    FftR2C_ZeroTest<TypeParam>::exec(device);
}

/**
 * @details
 * insert a delta fn off size equal to the number of elements in the first position
 * we expect the impulse of magnitude equal to the size f this pulse and all in the real place
 */
template<typename TypeParam>
struct FftR2C_DeltaTest : public R2C_Base<FftR2C_DeltaTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputTypeHost;
        typedef data::FrequencySeries<Cpu, ComplexT> OutputTypeHost;
        std::size_t fft_len=TypeParam::fft_trial_length;

        InputTypeHost data_in(data::TimeType(0.003 * data::seconds), fft_len);
        *(data_in.begin())=1;
        std::fill(data_in.begin()+1, data_in.end(), 0);
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));
        traits.api().process(device, input, output);
        ASSERT_EQ(output.size(), input.size()/2 + 1);
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));
        OutputTypeHost data_out(output);
        std::size_t count=0;
        for(auto const& elm : data_out){
            SCOPED_TRACE(std::to_string(count));
            ASSERT_FLOAT_EQ(round(elm.real()), 1); // n.b. output types are not normalised by default
            ASSERT_FLOAT_EQ(round(elm.imag()), 0.0);
            ASSERT_TRUE(std::abs(elm.real()-1.0) < 1e-6);
            ASSERT_TRUE(std::abs(elm.imag()-0.0) < 1e-6);
            ++count;
        }

        // perform inverse transform (i.e a c2r transform)
        InputType inverse(traits.template allocator<InputType>(device));
        if(! process(traits, device, output, inverse))
        {
            std::cout << "Inverse transform not availaable - skipping test";
            return;
        }

        ASSERT_EQ(inverse.size(), 2*(output.size() - 1));
        ASSERT_EQ(inverse.sampling_interval().value(), (1.0f/(output.frequency_step().value() * inverse.size())));
        InputTypeHost inverse_host(inverse);
        ASSERT_EQ(inverse_host.size(), data_in.size());

        // verfiy inverse has returned the original data
        auto inv_it=inverse_host.begin();
        count=0;
        for(auto const& elm : data_in)
        {
            SCOPED_TRACE(std::to_string(count));
            ASSERT_TRUE(std::abs(elm*output.size() - *inv_it) < TypeParam::accuracy()) << "expected:" << elm*output.size() << " got:" << *inv_it << " accuracy:" << TypeParam::accuracy();
            ++inv_it;
            ++count;
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_c2r_delta)
{
    FftR2C_DeltaTest<TypeParam>::exec(device);
}


/**
 * @details
 * insert a delta fn of size equal to the number of elements at a shifted position
 * we expect the impulse of magnitude equal to the size f this pulse
 */
template<typename TypeParam>
struct FftR2C_ShiftTest : public R2C_Base<FftR2C_ShiftTest<TypeParam>, TypeParam>
{
    static inline
    void run_test(typename TypeParam::DeviceType& device)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::TimeSeries<Arch, T> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        typedef data::TimeSeries<Cpu, T> InputTypeHost;
        typedef data::FrequencySeries<Cpu, ComplexT> OutputTypeHost;
        std::size_t fft_len=TypeParam::fft_trial_length;

        InputTypeHost data_in(data::TimeType(0.003 * data::seconds), fft_len);
        *(data_in.begin())=0;
        *(data_in.begin()+1)=1;
        std::fill(data_in.begin()+3, data_in.end(), 0);
        InputType input(data_in, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));
        traits.api().process(device, input, output);
        ASSERT_EQ(output.size(), input.size()/2 + 1);
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));
        OutputTypeHost data_out(output);
        size_t count=0;
        for(auto const& elm : data_out){
            SCOPED_TRACE(std::to_string(count));
            ASSERT_FLOAT_EQ((T)1, (T)round(sqrt(elm.real()*elm.real()+elm.imag()*elm.imag())));
            ASSERT_TRUE(std::abs((T)(sqrt(elm.real()*elm.real()+elm.imag()*elm.imag()))-(T)1) < 1e-6);
        }
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_r2c_shift)
{
    FftR2C_ShiftTest<TypeParam>::exec(device);
}

template<typename TypeParam, typename Enable=void>
struct FftC2C_Fwd_Test
{
    /// TimeSeries<Complex> -> FrequencySeries<Complex>
    static inline
    void exec(typename TypeParam::DeviceType& device)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::TimeSeries<Arch, ComplexT> InputType;
        typedef data::FrequencySeries<Arch, ComplexT> OutputType;
        InputType input(0.066 * data::seconds, 1024, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));
        traits.api().process(device, input, output);
        ASSERT_EQ(output.size(), input.size());
        ASSERT_EQ(output.frequency_step().value(), (1.0f/(input.sampling_interval().value() * input.size())));
    }
};

template<typename TypeParam>
struct FftC2C_Fwd_Test<TypeParam
                     , typename std::enable_if<
                                     !HasTime2Frequency<TypeParam
                                                       , typename TypeParam::ComplexT
                                                       , typename TypeParam::ComplexT
                                                       >::value
                                              >::type>
{
    static inline
    void exec(typename TypeParam::DeviceType&)
    {
        std::cout << "Complex to Complex FT not defined for this device";
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_c2c_fwd)
{
    FftC2C_Fwd_Test<TypeParam>::exec(device);
}

template<typename TypeParam, typename Enable=void>
struct FftC2C_Inv_Test
{
    static inline
    void exec(typename TypeParam::DeviceType& device)
    {
        TypeParam traits;
        typedef typename TypeParam::NumericalRep T;
        typedef typename TypeParam::Arch Arch;
        typedef typename TypeParam::ComplexT ComplexT;

        typedef data::FrequencySeries<Arch, ComplexT> InputType;
        typedef data::TimeSeries<Arch, ComplexT> OutputType;
        InputType input(0.0035 * data::hz, 1024, traits.template allocator<InputType>(device));
        OutputType output(traits.template allocator<OutputType>(device));
        traits.api().process(device, input, output);
        ASSERT_EQ(output.size(), input.size());
        ASSERT_EQ(output.sampling_interval().value(), (1.0f/(input.frequency_step().value() * output.size())));
    }
};

template<typename TypeParam>
struct FftC2C_Inv_Test<TypeParam
                     , typename std::enable_if<
                                     !HasFrequency2Time<TypeParam
                                                       , typename TypeParam::ComplexT
                                                       , typename TypeParam::ComplexT
                                                       >::value
                                              >::type>
{
    static inline
    void exec(typename TypeParam::DeviceType&)
    {
        std::cout << "Complex to Complex Inverse FT not defined for this device";
    }
};

ALGORITHM_TYPED_TEST_P(FftTester, fft_c2c_inv)
{
    FftC2C_Inv_Test<TypeParam>::exec(device);
}

/// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
/// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(FftTester, fft_r2c_zero, fft_r2c_c2r_delta, fft_r2c_shift, fft_c2c_fwd, fft_c2c_inv);
} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska
