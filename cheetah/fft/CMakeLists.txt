SUBPACKAGE(cuda)
SUBPACKAGE(altera)

set(module_fft_lib_src_cuda
    ${lib_src_cuda}
    PARENT_SCOPE
    )

set(module_fft_lib_src_cpu
    src/Fft.cpp
    src/Config.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
    )

TEST_UTILS()
add_subdirectory(test)
