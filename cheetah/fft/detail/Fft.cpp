/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/Fft.h"
#include "panda/TupleUtilities.h"
#include <utility>

namespace ska {
namespace cheetah {
namespace fft {

/**
 *
 * Forward the FFT request to the relevant method of the first implementation that will accept it.
 * The algorithm is determined via an Architecture : Implementation map which must be specified in
 * the scope of the containing class. This provides compile-time resolution of the call chain.
 *
 */

template <typename Arch, typename InputType, typename OutputType, typename... Args>
void Fft::process(panda::PoolResource<Arch>& resource,
    InputType const& input,
    OutputType& output,
    Args&&... args)
{
    auto& algo = _implementations.get<Arch>();
    algo.process(resource, std::forward<InputType const&>(input), output, std::forward<Args>(args)...);
}

} // namespace fft
} // namespace cheetah
} // namespace ska
