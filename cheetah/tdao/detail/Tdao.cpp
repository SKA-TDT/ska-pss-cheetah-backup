/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/tdao/Tdao.h"
#include "panda/TupleUtilities.h"
#include <utility>

namespace ska {
namespace cheetah {
namespace tdao {


template <typename Arch, typename T, typename Alloc, typename... Args>
void Tdao::process(panda::PoolResource<Arch>& resource,
    data::PowerSeries<Arch,T,Alloc> const& input,
    data::Ccl& output,
    data::DedispersionMeasureType<float> const& dm,
    data::AccelerationType const& acc,
    std::size_t nharmonics,
    Args&&... args)
{
    auto& algo = _implementations.get<Arch>();
    algo.template process<T,Alloc,Args...>(resource,input,output,dm,acc,nharmonics,
        std::forward<Args>(args)...);
}

} // namespace tdao
} // namespace cheetah
} // namespace ska
