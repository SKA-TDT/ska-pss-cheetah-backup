#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/ddtr/test_utils/DdtrTester.h"
#include "cheetah/ddtr/cpu/Ddtr.h"
#include <memory>
#include <vector>


namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {
namespace test {

struct DdtrCpuTraits : public ddtr::test::DdtrTesterTraits<Cpu>
{
    typedef ddtr::DedispersionConfig::Dm Dm;
    typedef ddtr::test::DdtrTesterTraits<Cpu> BaseT;
    typedef typename BaseT::Arch Arch;
    void configure(ddtr::Config& config) override {
        //config.add_dm_range(Dm(0.0 * data::parsecs_per_cube_cm),Dm(1000.0 * data::parsecs_per_cube_cm), Dm(100.0 * data::parsecs_per_cube_cm));
        config.cpu_algo_config().active(true);
        config.dedispersion_samples(1<<14);
        PANDA_LOG << "Number of added dm trials: " << config.dm_trials().size();
    }
    private:
        std::vector<std::unique_ptr<ddtr::DedispersionConfig>> _configs; // keep configs in scope
};

} // namespace test
} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace ddtr {
namespace test {

typedef ::testing::Types<ddtr::cpu::test::DdtrCpuTraits> DdtrCpuTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cpu, DdtrTester, DdtrCpuTraitsTypes);

} // namespace test
} // namespace ddtr
} // namespace cheetah
} // namespace ska
