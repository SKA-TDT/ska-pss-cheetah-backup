/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_CPU_DDTR_H
#define SKA_CHEETAH_DDTR_CPU_DDTR_H

#include "cheetah/ddtr/cpu/Config.h"
#include "cheetah/ddtr/cpu/DedispersionPlan.h"
#include "cheetah/ddtr/cpu/detail/Worker.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {

template<typename DdtrTraits>
class Ddtr
{
    private:
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef typename DdtrTraits::TimeFrequencyType TimeFrequencyType;
        typedef typename DdtrTraits::BufferType BufferType;

    public:
        typedef cpu::DedispersionPlan<DdtrTraits> DedispersionPlan;
        typedef cpu::Config Config;
        typedef Cpu Architecture;

    public:
        Ddtr(ddtr::Config const& algo_config);
        Ddtr(Ddtr const&) = delete;
        Ddtr(Ddtr&&);
        ~Ddtr();

        /**
         * @brief dedispersion of time frequency data on CPU
         * @detail DmTialsHandler called when buffer is full.
         * @param data   A TimeFrequency block and cpu resource
         * @return DmTime sequence i.e. timeseries for each DM trial value
         */
        std::shared_ptr<DmTrialsType> operator()(panda::PoolResource<cheetah::Cpu>& cpu
                                               , BufferType const& data);


        /**
         * @brief set a new dedispersion plan
         */
        void plan(DedispersionPlan const& plan);

    private:
        static_assert(std::is_empty<Worker<DdtrTraits>>::value, "If you are adding state to Worker then you will need to use DeviceLocal for thread safety");
        Worker<DdtrTraits> _worker;
        std::shared_ptr<DedispersionPlan> _plan;
};


} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#include "cheetah/ddtr/cpu/detail/Ddtr.cpp"

#endif // SKA_CHEETAH_DDTR_CPU_DDTR_H
