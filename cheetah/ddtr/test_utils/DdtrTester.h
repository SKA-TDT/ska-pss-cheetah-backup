/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_TEST_DDTRTESTER_H
#define SKA_CHEETAH_DDTR_TEST_DDTRTESTER_H

#include "cheetah/generators/pulse_profile/ProfileManager.h"
#include "cheetah/ddtr/Ddtr.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "panda/ResourcePool.h"
#include "panda/test/TestResourcePool.h"
#include <gtest/gtest.h>
#include <vector>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace test {

/**
 * @brief
 * Generic functional test for the Ddtr algorithm
 *
 * @detail
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requirements it needs to run.
 *
 *  e.g.
 * @code
 * struct MyAlgoTraits1 : public DdtrTesterTraits<ResourceCapabilities, Architecture> {
 *      /// wrapper to execute the algorithm on requested provided device
 *      /// @return A dataype matching the tester requirements to verify the results
 *      ResultsType apply_algorithm(DeviceType&) const;
 * };
 * @endcode
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<MyAlgoTraits1, MyAlgoTraits2> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, DdtrTester, MyTypes);
 * @endcode
 *
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */


template<typename ArchitectureTag, typename NumericalT=uint8_t, typename TimeFrequencyT = data::TimeFrequency<cheetah::Cpu, NumericalT>>
struct DdtrTesterTraits : public utils::test::PoolAlgorithmTesterTraits<ArchitectureTag> {
    public:
        typedef utils::test::PoolAlgorithmTesterTraits<ArchitectureTag> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::PoolType PoolType;
        typedef NumericalT NumericalRep;
        typedef TimeFrequencyT TimeFrequencyType;

    protected:
        struct TestConfig : public ddtr::Config
        {
            typedef typename DdtrTesterTraits<ArchitectureTag>::PoolType PoolType;

            public:
                TestConfig() : _pool(nullptr) {}
                PoolType& pool() const { assert(_pool); return *_pool; }
                void set_pool(PoolType& pool) { _pool = &pool; }

            protected:
                PoolType* _pool;
        };

    public:
        typedef ddtr::Ddtr<TestConfig, NumericalRep> Api;
        typedef typename Api::DmTrialsType DmType;
        typedef typename std::vector<std::shared_ptr<DmType>> DmDataContainerType;

    public:
        DdtrTesterTraits();
        DdtrTesterTraits(DdtrTesterTraits const&) = delete;
        Api& api(PoolType& pool);
        ddtr::Config& config();

        //return true if the dm_handler has been called
        bool dm_handler_called() const;

        /// return true if the dm_handler has been called
        std::size_t dm_handler_call_count() const;
        generators::ProfileManager const& profile_manager() const { return _manager; }
        DmDataContainerType const& dm_data() const;

    protected:
        virtual void configure(ddtr::Config&) {}

    private:
        bool _dm_called;

    protected:
        TestConfig _config;

        DmDataContainerType _dm_data;
        std::size_t _dm_call_count;

        std::unique_ptr<Api> _api; // must be last member
        generators::ProfileManager _manager;
};


template <typename TestTraits>
class DdtrTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        DdtrTester();
        ~DdtrTester();


};

TYPED_TEST_CASE_P(DdtrTester);

} // namespace test
} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "cheetah/ddtr/test_utils/detail/DdtrTester.cpp"


#endif // SKA_CHEETAH_DDTR_TEST_DDTRTESTER_H
