/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/test_utils/DdtrTester.h"
#include "cheetah/ddtr/Ddtr.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/PulsarInjection.h"
#include "cheetah/generators/PulsarInjectionConfig.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/sigproc/SigProcWriter.h"
#include "unistd.h"
#include <thread>
#include <chrono>
#include <sstream>
#include <cmath>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace test {


template <typename TestTraits>
DdtrTester<TestTraits>::DdtrTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
DdtrTester<TestTraits>::~DdtrTester()
{
}

template<typename TestTraits>
void DdtrTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void DdtrTester<TestTraits>::TearDown()
{
}

template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::DdtrTesterTraits()
    : _dm_called(false)
    , _dm_call_count(0u)
{
}

template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
typename DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::Api& DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::api(PoolType& pool)
{
    typedef ddtr::DedispersionConfig::Dm Dm;
    if(!_api) {
        configure(_config); // call configuration method
        _config.add_dm_range(Dm(0.0 * data::parsecs_per_cube_cm),Dm(1000.0 * data::parsecs_per_cube_cm), Dm(100.0 * data::parsecs_per_cube_cm));
        _config.set_pool(pool);
        _api.reset(new Api(_config,
                     [this](std::shared_ptr<DmType> data)
                     {
                        PANDA_LOG << "DM handler called";
                        _dm_called = true;
                        _dm_data.push_back(data);
                        ++_dm_call_count;
                        PANDA_LOG << "dm_call_count:"<<_dm_call_count;
                     } ));
    }
    return *_api;
}

template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
typename DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::DmDataContainerType const& DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::dm_data() const
{
    return _dm_data;
}

template <class T>
void dump_host_buffer(T* buffer,size_t size, std::string filename)
{
    std::ofstream outfile;
    outfile.open(filename.c_str(),std::ifstream::out | std::ifstream::binary);
    outfile.write((char*)buffer, size*sizeof(T));
    outfile.close();
}

template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
ddtr::Config& DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::config()
{
    return _config;
}

template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
bool DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::dm_handler_called() const
{
    return _dm_called;
}


template<typename ArchitectureTag, typename NumericalT, typename TimeFreqT>
std::size_t DdtrTesterTraits<ArchitectureTag, NumericalT, TimeFreqT>::dm_handler_call_count() const
{
    return _dm_call_count;
}

POOL_ALGORITHM_TYPED_TEST_P(DdtrTester, test_handlers)
{
    /**
     * @brief Test if the dedispersion handler is called.
     *
     * @detail Once the agg buffer is filled with enough data, the Dm handler should be triggered and should return a true value
     */

    TypeParam traits;
    typedef typename TypeParam::NumericalRep NumericalRep;
    auto& api = traits.api(pool);

    // generate random noise data and pass to Ddtr
    // until we get a callback
    generators::GaussianNoiseConfig generator_config;
    generators::GaussianNoise<NumericalRep> generator(generator_config);

    typedef typename TypeParam::TimeFrequencyType DataType;

    // keep sending data until we get a callback
    std::size_t count=0;
    std::size_t chunk_samples=10000;
    for(int ii=0; ii < 4; ++ii)
    {
        std::shared_ptr<DataType> data = DataType::make_shared(pss::astrotypes::DimensionSize<pss::astrotypes::units::Time>(chunk_samples),
	                                  pss::astrotypes::DimensionSize<pss::astrotypes::units::Frequency>(10));
        data->sample_interval(typename DataType::TimeType(150.0 * boost::units::si::milli * boost::units::si::seconds));
        data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-30.0 * boost::units::si::mega * data::hz));
        PANDA_LOG_DEBUG << "Populating test vector with generator data";
        generator.next(*data);
        api(*data);
        ++count;
        PANDA_LOG_DEBUG << "In loop on count " << count;
    }

    while (!traits.dm_handler_called())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    ASSERT_TRUE(traits.dm_handler_called());

    // now we expect the buffer to flush data as it is destroyed. so the pool has to stay alive whilst this happns
}


POOL_ALGORITHM_TYPED_TEST_P(DdtrTester, test_algorithm)
{
    /**
     * @brief Test if the Dedispersion algorithm works on pulsed data without noise
     *
     * @detail Initialised time frequency data is dispersed at DM=500 with a 3 bin width pulse across 10 frequency channels.
     *         The range of DM trials is 100 to 1000 with steps of 100.
     *         The Trial number 5 should yield the maximum value of dedispersed pulse with value equal to number of channels
     */

    TypeParam traits;
    auto& api = traits.api(pool);

    // generate Pulsar and pass to Ddtr
    // until we get a callback


    typedef typename TypeParam::TimeFrequencyType DataType;
    // keep sending data until we get a callback

    std::size_t count=0;
    std::size_t chunk_samples=10000;
    std::size_t no_of_chans=10;
    for (int ii=0; ii<4; ++ii)
    {
        std::shared_ptr<DataType> data = DataType::make_shared(pss::astrotypes::DimensionSize<pss::astrotypes::units::Time>(chunk_samples),
                                          pss::astrotypes::DimensionSize<pss::astrotypes::units::Frequency>(no_of_chans));
        data->sample_interval(typename DataType::TimeType(150.0 * boost::units::si::milli * boost::units::si::seconds));
        data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-30.0 * boost::units::si::mega * data::hz));
        PANDA_LOG_DEBUG << "Populating test vector with dispersed pulsed data";

        //instantiate data with zeros
        std::fill(data->begin(), data->end(), 0);

        // Calculate delays and accordingly add 3 bin wide pulse at those positions
        float dm_constant = 4.1493775933609e3;
        std::vector<data::FrequencyType> const& channel_freqs = data->channel_frequencies();
        data::FrequencyType freq_top = *std::max_element(channel_freqs.begin(), channel_freqs.end());
        std::vector<float> delays;
        for (auto freq: channel_freqs)
        {
            delays.push_back(dm_constant * 500.0 * (1.0/(freq.value()*freq.value()) -  1.0/(freq_top.value()*freq_top.value())) / data->sample_interval().value());
        }

        for(std::size_t ii=0; ii<no_of_chans; ++ii)
        {
           typename DataType::Channel ts = data->channel(ii);
           std::size_t delay = std::size_t(round(delays[ii]));
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay)] = 1;
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay + 1)] = 1;
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay + 2)] = 1;
        }

        PANDA_LOG_DEBUG << "Calling API";
        api(*data);
        ++count;
        PANDA_LOG << "In loop on count " << count;
    }

    while(!traits.dm_handler_called())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    sync();
    pool.wait();
    auto const& dm_data = traits.dm_data();
    auto const& dm_trials = (*dm_data[0]);
    std::vector<float> max_values;
    std::vector<float> trial_nos;
    std::size_t cnt=0;

    for (auto it=dm_trials.cbegin();it!=dm_trials.cend();++it)
    {
       std::size_t max_value = *std::max_element(it->cbegin(),it->cend());
       trial_nos.push_back(cnt);
       max_values.push_back(max_value);
       ++cnt;
    }
    ASSERT_TRUE(*std::max_element(max_values.begin(),max_values.end())==10);
    ASSERT_TRUE(std::distance(max_values.begin(), std::max_element(max_values.begin(),max_values.end()))==5);
}

POOL_ALGORITHM_TYPED_TEST_P(DdtrTester, test_pulse_with_noise)
{
    /**
     * @brief Test if the Dedispersion algorithm works on pulsed data with noise
     *
     * @detail Initialised time frequency data is dispersed at DM=500 with a 3 bin width pulse across 10 frequency channels with noise.
     *         The range of DM trials is 100 to 1000 with steps of 100.
     *         The Trial number 5 should yield the maximum value of dedispersed pulsed data.
     */

    TypeParam traits;
    typedef typename TypeParam::NumericalRep NumericalRep;
    auto& api = traits.api(pool);

    // generate Pulsar and pass to Ddtr
    // until we get a callback
    generators::GaussianNoiseConfig generator_config;
    generators::GaussianNoise<NumericalRep> generator(generator_config);

    typedef typename TypeParam::TimeFrequencyType DataType;

    // keep sending data until we get a callback

    std::size_t count=0;
    std::size_t chunk_samples=10000;
    std::size_t no_of_chans=10;
    for (int ii=0; ii<4; ++ii)
    {
        std::shared_ptr<DataType> data = DataType::make_shared(pss::astrotypes::DimensionSize<pss::astrotypes::units::Time>(chunk_samples),
                                          pss::astrotypes::DimensionSize<pss::astrotypes::units::Frequency>(no_of_chans));
        data->sample_interval(typename DataType::TimeType(50.0 * boost::units::si::milli * boost::units::si::seconds));
        data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-30.0 * boost::units::si::mega * data::hz));
        PANDA_LOG_DEBUG << "Populating test vector with dispersed pulsed data";
        //instantiate data with noise
        generator.next(*data);
        // Calculate delays and accordingly add 3 bin wide pulse at those positions
        float dm_constant = 4.1493775933609e3;
        std::vector<data::FrequencyType> const& channel_freqs = data->channel_frequencies();
        data::FrequencyType freq_top = *std::max_element(channel_freqs.begin(), channel_freqs.end());
        std::vector<float> delays;
        for (auto freq: channel_freqs)
        {
            delays.push_back(dm_constant * 500.0 * (1.0/(freq.value()*freq.value()) -  1.0/(freq_top.value()*freq_top.value())) / data->sample_interval().value());
        }

        for(std::size_t ii=0;ii<no_of_chans;++ii)
        {
           typename DataType::Channel ts = data->channel(ii);
           std::size_t delay = std::size_t(round(delays[ii]));
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay)] += 100;  //Approx 4 sigma since sigma=24 ;
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay + 1)] += 100;
           ts[pss::astrotypes::DimensionIndex<pss::astrotypes::units::Time>(delay + 2)] += 100;
        }

        PANDA_LOG_DEBUG << "Calling API";
        api(*data);
        ++count;
        PANDA_LOG << "In loop on count " << count;
    }

    while(!traits.dm_handler_called())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    sync();
    pool.wait();
    auto const& dm_data = traits.dm_data();
    auto const& dm_trials = (*dm_data[0]);
    std::vector<float> max_values;
    std::vector<float> trial_nos;
    std::size_t cnt=0;

    for (auto it=dm_trials.cbegin(); it!=dm_trials.cend(); ++it)
    {
       std::size_t max_value = *std::max_element(it->cbegin(),it->cend());
       trial_nos.push_back(cnt);
       max_values.push_back(max_value);
       ++cnt;
    }
   ASSERT_TRUE(std::distance(max_values.begin(), std::max_element(max_values.begin(),max_values.end()))==5);

}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(DdtrTester, test_handlers, test_algorithm, test_pulse_with_noise);

} // namespace test
} // namespace ddtr
} // namespace cheetah
} // namespace ska
