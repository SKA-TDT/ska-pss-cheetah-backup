/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_CONFIG_H
#define SKA_CHEETAH_DDTR_CONFIG_H
#include "cheetah/ddtr/DedispersionTrialPlan.h"
#include "cheetah/ddtr/fpga/Config.h"
#include "cheetah/ddtr/cpu/Config.h"
#include "cheetah/ddtr/astroaccelerate/Config.h"
#include "panda/PoolSelector.h"
#include "panda/MultipleConfigModule.h"
#include <cstdlib>

namespace ska {
namespace cheetah {
namespace ddtr {

/**
 * @brief add new ddtr algos Config types here to be included in the Ddtr::Config object
 * @details access the config type you want with a call to the templated config method
 * @code
 *    ddtr::Config ddtr_config;
 *    ddtr::cpu::Config const& cpu_config =  ddtr_config.config<cpu::Config>();
 * @endcode
 */
typedef panda::MultipleConfigModule<DedispersionTrialPlan
                                  , cpu::Config
                                  , fpga::Config
                                  , astroaccelerate::Config
                                  > DdtrAlgoConfigs;

class Config : public DdtrAlgoConfigs
{
        typedef DdtrAlgoConfigs BaseT;

    public:
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;

    public:
        Config();
        ~Config();

        /**
         * @brief return the fpga algorithm configuration parameters
         */
        fpga::Config const& fpga_algo_config() const;
        fpga::Config& fpga_algo_config();

        /**
         * @brief return the cpu algorithm configuration parameters
         */
        cpu::Config const& cpu_algo_config() const;
        cpu::Config& cpu_algo_config();

        /**
         * @brief return the astroaccelerate algorithm configuration parameters
         */
        astroaccelerate::Config const& astroaccelerate_algo_config() const;
        astroaccelerate::Config& astroaccelerate_algo_config();

        /**
         *@brief number of samples to dedisperse
         */
        std::size_t dedispersion_samples() const;

        /**
         *@brief set number of samples to dedisperse
         */
        void dedispersion_samples(std::size_t);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        std::size_t             _dedispersion_samples;
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace ddtr
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DDTR_CONFIG_H
