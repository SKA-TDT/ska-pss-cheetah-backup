/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/fpga/Ddtr.h"
#include <limits>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace fpga {

template<typename DdtrTraits>
Ddtr<DdtrTraits>::WorkerFactory::WorkerFactory(ddtr::Config const& config)
    : _config(config)
{
}

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(ConfigType const& config)
    : _first_call(true)
    , _workers(WorkerFactory(config))
    , _config(config)
    , _dev_memory(std::numeric_limits<std::size_t>::max())
{
    auto const& pool = config.pool();
    if(pool.template free_resources<Architecture>().empty()) _dev_memory = 0;

#ifdef ENABLE_OPENCL
    for(auto const& fpga : pool.template free_resources<Architecture>())
    {
        _workers(*fpga); // construct a worker for each fpga device
        std::size_t device_memory = fpga->device_memory();
        if(device_memory < _dev_memory) {
            _dev_memory=device_memory;
        }
    }
    PANDA_LOG << "maximum device memory suitable for all devices in the pool is " << _dev_memory;
#endif // ENABLE_OPENCL
}

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(Ddtr&& other)
    : _first_call(other._first_call)
    , _dedispersion_handler(other._dedispersion_handler)
    , _workers(std::move(other._workers))
    , _config(std::move(other._config))
    , _dev_memory(std::move(other._dev_memory))
{
    /*
    _agg_buffer_filler_ptr->full_buffer_handler([this](BufferType buffer)
    {
        this->_pool.submit(*this, std::move(buffer));
    });
    */
}

template<typename DdtrTraits>
Ddtr<DdtrTraits>::~Ddtr()
{
    //_agg_buffer_filler_ptr.reset();
    //_pool.wait();
}

template<typename DdtrTraits>
void Ddtr<DdtrTraits>::operator()(panda::PoolResource<Architecture>& device, BufferType const& input_data)
{
    _dedispersion_handler(_workers(device)(input_data,_dm_factors, _max_delay));
}

template<typename DdtrTraits>
void Ddtr<DdtrTraits>::init(TimeFrequencyType const& data)
{
    DmListType dm_trials = this->_config.dm_trials();
    Dm max_dm = this->_config.max_dm();
    auto const& channel_freqs = data.channel_frequencies();
    auto freq_pair = data.low_high_frequencies();
    FrequencyType freq_top = freq_pair.second;
    FrequencyType freq_bottom = freq_pair.first;
    _max_delay = std::size_t((_config.dm_constant().value()
                    * (1.0/(freq_bottom*freq_bottom) -  1.0/(freq_top*freq_top))
                    * max_dm / data.sample_interval()).value()) + 1;

    for (auto freq: channel_freqs)
    {
        double factor = (_config.dm_constant().value() * (1.0/(freq*freq) -  1.0/(freq_top*freq_top)) / data.sample_interval()).value();
        _dm_factors.push_back(factor);
    }
    _dedispersion_samples = this->_config.dedispersion_samples();
    if (_dedispersion_samples < 2 * _max_delay)
    {
        PANDA_LOG_WARN << "Requested number of samples to dedisperse ("
                       << this->_config.dedispersion_samples()
                       << ") is less than twice the max dispersion delay ("
                       << 2 * _max_delay << "): Setting number of samples to dedisperse to "
                       << 2 * _max_delay;
        _dedispersion_samples = 2 * _max_delay;
    }

    //generate_dmtrials_metadata(data.sample_interval(), _dedispersion_samples - _max_delay);
    _agg_buffer_filler_ptr->resize(std::min(_dev_memory, this->_config.dedispersion_samples() * data.number_of_channels()));
    _agg_buffer_filler_ptr->set_overlap(_max_delay * data.number_of_channels());
}

template<typename DdtrTraits>
template<typename TimeFrequencyT>
void Ddtr<DdtrTraits>::operator()(TimeFrequencyT const& input_data)
{
    if(_first_call) {
        this->init(input_data);
        _first_call=false;
    }
    (*_agg_buffer_filler_ptr) << input_data;
}

} // namespace fpga
} // namespace ddtr
} // namespace cheetah
} // namespace ska
