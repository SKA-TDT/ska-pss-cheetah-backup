/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H
#define SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H

#include "cheetah/ddtr/DedispersionTrialPlan.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/DataChunk.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {

//still work to do to understand these variables. But as of now I am using the
// default values from the astroaccelerate Params.h file.
template <typename NumericalRep>
class DedispersionOptimizationParameters{
    public:
        static constexpr unsigned UNROLLS = 8;
        static constexpr unsigned SNUMREG = 8;
        static constexpr unsigned SDIVINT = 14;
        static constexpr unsigned SDIVINDM = 50;
        static constexpr float SFDIVINDM = 50.0f;
        static constexpr unsigned MIN_DMS_PER_SPS_RUN = 64;
};

template<typename NumericalRep, typename OptimizationParameterT=DedispersionOptimizationParameters<NumericalRep>>
class DedispersionStrategy
{
    public:
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        DedispersionStrategy(const data::TimeFrequency<Cpu,NumericalRep>& chunk, const ddtr::DedispersionTrialPlan& plan, std::size_t gpu_memory);
        ~DedispersionStrategy();

        /**
         * @brief Memory used for the GPU operations
         */
        std::size_t gpu_memory() const;

        /**
         * @brief The number of dm ranges
         */
        unsigned int range() const;

        /**
        * @brief An array containing lowest band of each dm range, specified by the user
        */
        std::vector<Dm> user_dm_low() const;
        /**
         * @brief An array containing highest band of each dm range, specified by the user
         */
        std::vector<Dm> user_dm_high() const;
        /**
         * @brief An array containing dm step of each dm range, specified by the user
         */
        std::vector<Dm>user_dm_step() const;

        /**
         * @brief
         */
        std::vector<int> in_bin() const;
        std::vector<int> out_bin() const;

        /**
         * @brief Value used to make sure that dms from dm_low to dm_high are used
         */
        unsigned int maxshift() const;

        /**
         * @brief An array containing the lowest bound of each dm range
         */
        std::vector<Dm> dm_low() const;

        /**
         * @brief An array containing the highest bound of each dm range
         */
        std::vector<Dm> dm_high() const;

        /**
         * @brief An array containing the step size of each dm range
         */
        std::vector<Dm> dm_step() const;

        /**
         * @brief An array containing a constant associated with each channel to perform dedispersion algorithm
         */
        std::vector<float> dmshifts() const;

        /**
         * @brief An array containing the number of dms for each range
         */
        std::vector<unsigned int> ndms() const ;

        /**
         * @brief The maximum number of dm
         */
        unsigned int max_ndms() const;

        /**
         * @brief The total number of dm
         */
        unsigned int total_ndms() const;

        /**
         * @brief The highest dm value
         */
        Dm max_dm() const;

        /**
         * @brief The number of time samples required to search for a dm in each dm range
         */
        std::vector<std::vector<int>> t_processed() const;

        /**
         * @brief The number of bits of the input data
         */
        static constexpr unsigned int nbits();

        /**
         * @brief The number of IF channels
         */
        unsigned int nifs() const;
        void nifs(unsigned int value);

        /**
         * @brief Time sample value
         */
        TimeType tsamp() const;

        /**
         * @brief Frequency of the first channel
         */
        FrequencyType fch1() const;

        /**
         * @brief Frequency channel width
         */
        FrequencyType foff() const;

        /**
         * @brief The number of time samples
         */
        unsigned int nsamp() const;

        /**
         * @brief The number of frequency channels
         */
        unsigned int nchans() const;

        /**
         * @brief The number of chunks the data are divided in
         */
        unsigned int num_tchunks() const;

        std::size_t memory_requirement_of_SPS();

        data::DimensionSize<data::Time> dedispersed_samples()
        {
            return data::DimensionSize<data::Time>(_dedispersed_time_samples);
        }

    private:
        /**
         * @brief Computes the dedispersion strategy
         *
         */
        void make_strategy(size_t gpu_memory);

    private:
        std::size_t _gpu_memory;
        unsigned int _range;
        std::vector<Dm> _user_dm_low;
        std::vector<Dm> _user_dm_high;
        std::vector<Dm> _user_dm_step;
        std::vector<int> _in_bin;
        std::vector<int> _out_bin;
        unsigned int _maxshift;
        std::vector<Dm> _dm_low;
        std::vector<Dm> _dm_high;
        std::vector<Dm> _dm_step;
        std::vector<unsigned int> _dmshifts;
        std::vector<unsigned int> _ndms;
        unsigned int _num_tchunks;
        unsigned int _max_ndms;
        unsigned int _total_ndms;
        Dm _max_dm;
        std::vector<std::vector<int>> _t_processed;
        unsigned int _nifs;
        TimeType _tsamp;
        unsigned int _nsamp;
        unsigned int _nchans;
        std::vector<float> _bin_frequencies;
        FrequencyType _fch1;
        FrequencyType _foff;
        size_t _SPS_mem_requirement;
        std::size_t _dedispersed_time_samples;
        DmConstantType _dm_constant;
};



}//astroacclerate
}//sps
}//cheetah
}//ska
#include "cheetah/ddtr/astroaccelerate/detail/DedispersionStrategy.cpp"

#endif // SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H
