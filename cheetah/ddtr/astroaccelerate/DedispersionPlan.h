/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONPLAN_H
#define SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONPLAN_H

#include "cheetah/data/TimeFrequency.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/ddtr/astroaccelerate/Config.h"
#include "cheetah/ddtr/astroaccelerate/DedispersionStrategy.h"
namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {

/**
 * @brief
 * @details
 */
template <typename DdtrTraits>
class DedispersionPlan
{
    public:
        typedef typename DdtrTraits::value_type NumericalT;
        typedef typename DdtrTraits::DedispersionHandler DedispersionHandler;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef typename DdtrTraits::TimeFrequencyType TimeFrequencyType;
        typedef ddtr::Config::Dm Dm;
        typedef typename DdtrTraits::BufferFillerType BufferFillerType;
        typedef typename DdtrTraits::BufferType BufferType;
        typedef std::vector<ddtr::Config::Dm> DmListType;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;
        typedef std::vector<FrequencyType> FrequencyListType;
        typedef typename ddtr::Config ConfigType;

    public:
        DedispersionPlan(ConfigType const& config, std::size_t memory=0);

        ~DedispersionPlan();

        data::DimensionSize<data::Time> reset(TimeFrequencyType const&);

        void reset(data::DimensionSize<data::Time> const& spectra);

        data::DimensionSize<data::Time> number_of_spectra() const;

        data::DimensionSize<data::Time> buffer_overlap() const;

        std::shared_ptr<data::DmTrialsMetadata> generate_dmtrials_metadata(TimeType sample_interval, std::size_t nspectra, std::size_t nsamples) const
        {
            return _algo_config.generate_dmtrials_metadata(sample_interval, nspectra, nsamples);
        }

        std::vector<Dm> const& dm_trials() const
        {
            return _algo_config.dm_trials();
        }

        std::vector<double> const& dm_factors() const
        {
            return _dm_factors;
        }

        std::shared_ptr<data::DmTrialsMetadata> dm_trial_metadata() const
        {
            return _dm_trial_metadata;
        }

        std::shared_ptr<DedispersionStrategy<NumericalT>> const& dedispersion_strategy() const
        {
            return _strategy;
        }

    private:
        ConfigType const& _algo_config;
        std::shared_ptr<DedispersionStrategy<NumericalT>> _strategy;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        std::size_t _memory;
        std::size_t _max_delay;
        std::size_t _dedispersion_samples;
        std::vector<double> _dm_factors;
        std::size_t _number_of_spectra;
};

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#include "cheetah/ddtr/astroaccelerate/detail/DedispersionPlan.cpp"
#endif // SKA_CHEETAH_DDTR_ASTROACCELERATE_DEDISPERSIONPLAN_H
