/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/astroaccelerate/DdtrWorker.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {


template<typename DdtrTraits>
DdtrWorker<DdtrTraits>::DdtrWorker(std::shared_ptr<DedispersionStrategy<NumericalRep>> const& strategy)
    : _strategy(strategy)
{
}

template<typename DdtrTraits>
DdtrWorker<DdtrTraits>::~DdtrWorker()
{
}

template<typename DdtrTraits>
template<typename BufferType>
std::shared_ptr<typename DdtrWorker<DdtrTraits>::DmTrialsType> DdtrWorker<DdtrTraits>::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType const& agg_buf)
{
    //PUSH_NVTX_RANGE("sps_astroaccelerate_DdtrCuda_operator",0);
    PANDA_LOG << "astroaccelerate:: invoked (on device "<< gpu.device_id() << ")";

    if (agg_buf.data_size() < (std::size_t) _strategy->maxshift())
    {
        const std::string msg("DdtrCuda: data buffer size < maxshift ");
        PANDA_LOG_ERROR << msg << "(" << agg_buf.data_size() << "<" << _strategy->maxshift() << ")";
        throw panda::Error(msg);
    }
    auto const& start_time = agg_buf.composition().front()->start_time(0);
    auto dm_trials_ptr = data::DmTrials<Cpu,float>::make_shared(_dm_trial_metadata, start_time);
    // prepare the DmTime metadata structure

    /*
    // run the Cuda kernel
    _ddtr.run_dedispersion_sps(
        gpu.device_id()
        ,tf_data
        ,*_dm_time
        ,sps_cands);

    POP_NVTX_RANGE; //astroaccelerate_run_dedispersion

    dm_handler(dm_time_ptr);
    */
    return dm_trials_ptr;
}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
