/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/astroaccelerate/Ddtr.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {


template<typename DdtrTraits>
struct Ddtr<DdtrTraits>::DdtrWorkerFactory
{
    DdtrWorkerFactory(std::shared_ptr<DedispersionPlan> const& plan)
        : _plan(plan)
    {
    }

    template<typename DeviceType>
    DdtrWorker<DdtrTraits>* operator()(DeviceType&)
    {
        return new DdtrWorker<DdtrTraits>(_plan->dedispersion_strategy());
    }

    private:
        std::shared_ptr<DedispersionPlan> _plan;
};

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(ddtr::Config const& config)
    : _plan(std::make_shared<DedispersionPlan>(config, 0))
    , _factory(new DdtrWorkerFactory(_plan))
    , _workers(*_factory)
{
}

template<typename DdtrTraits>
Ddtr<DdtrTraits>::Ddtr(Ddtr&& other)
    : _factory(std::move(other._factory))
    , _workers(std::move(other._workers))
{
}

template<typename DdtrTraits>
std::shared_ptr<typename Ddtr<DdtrTraits>::DmTrialsType> Ddtr<DdtrTraits>::operator()(panda::PoolResource<cheetah::Cuda>& gpu, BufferType const& data)
{
    return _workers(gpu)(gpu, data);
}

template<typename DdtrTraits>
std::size_t Ddtr<DdtrTraits>::set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data, panda::PoolResource<cheetah::Cuda> const& gpu)
{
    return _workers(gpu).set_dedispersion_strategy(min_gpu_memory, tf_data);spersion_strategy(min_gpu_memory, tf_data);
}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
