/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/astroaccelerate/DedispersionPlan.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate {

template <typename DdtrTraits>
DedispersionPlan<DdtrTraits>::DedispersionPlan(ConfigType const& config, std::size_t memory)
    : _algo_config(config)
    , _memory(memory)
{
}

template <typename DdtrTraits>
DedispersionPlan<DdtrTraits>::~DedispersionPlan()
{
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::reset(TimeFrequencyType const& data)
{
    _strategy = std::make_shared<DedispersionStrategy<NumericalT>>(data, _algo_config, _memory);
    //Dm max_dm = _strategy->max_dm();
    _max_delay = _strategy->maxshift();
    FrequencyListType const& channel_freqs = data.channel_frequencies();
    auto freq_pair = data.low_high_frequencies();
    FrequencyType freq_top = freq_pair.second;
    //FrequencyType freq_bottom = freq_pair.first;

    for (auto freq: channel_freqs)
    {
        double factor = (this->_algo_config.dm_constant().value() * (1.0/(freq*freq) -  1.0/(freq_top*freq_top)) / data.sample_interval()).value();
        PANDA_LOG_DEBUG << "Frequency: " << freq << "  Reference: " << freq_top << "  DM constant: "
                        << this->_algo_config.dm_constant() << "  Sampling interval: " << data.sample_interval() <<  "  DM factor: " << factor;
        _dm_factors.push_back(factor);
    }

    _dedispersion_samples = this->_algo_config.dedispersion_samples();
    if (_dedispersion_samples < 2 * _max_delay)
    {
        PANDA_LOG_WARN << "Requested number of samples to dedisperse ("
                        << this->_algo_config.dedispersion_samples()
                        << ") is less than twice the max dispersion delay ("
                        << 2 * _max_delay << "): Setting number of samples to dedisperse to "
                        << 2 * _max_delay;
        _dedispersion_samples = 2 * _max_delay;
    }
    //_dm_trial_metadata = this->_algo_config.generate_dmtrials_metadata(data.sample_interval(), _dedispersion_samples, _max_delay);
    return data::DimensionSize<data::Time>(_dedispersion_samples);
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::buffer_overlap() const
{
    return data::DimensionSize<data::Time>(_max_delay);
}

template <typename DdtrTraits>
void DedispersionPlan<DdtrTraits>::reset(data::DimensionSize<data::Time> const& spectra)
{
    _number_of_spectra = spectra;
}

template <typename DdtrTraits>
data::DimensionSize<data::Time> DedispersionPlan<DdtrTraits>::number_of_spectra() const
{
    return data::DimensionSize<data::Time>(_number_of_spectra);
}

} // namespace astroaccelerate
} // namespace ddtr
} // namespace cheetah
} // namespace ska
