/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace ddtr {
namespace astroaccelerate{

template<typename NumericalRep, typename OptimizationParameterT>
DedispersionStrategy<NumericalRep , OptimizationParameterT>::DedispersionStrategy(const data::TimeFrequency<Cpu,NumericalRep>& chunk, const ddtr::DedispersionTrialPlan& plan, std::size_t gpu_memory)
{
    _nsamp = chunk.number_of_spectra();
    _tsamp = chunk.sample_interval();
    _nchans = chunk.number_of_channels();
    auto t = chunk.low_high_frequencies();
    _fch1 = t.second;
    _foff = (t.second-t.first)/((double)_nchans);
    _dm_constant = plan.dm_constant();
    for(auto it = plan.begin_range(); it!=plan.end_range(); ++it)
    {
        _user_dm_low.push_back(it->dm_start());
            _user_dm_high.push_back(it->dm_end());
            _user_dm_step.push_back(it->dm_step());
        _in_bin.push_back(1);
        _out_bin.push_back(1);
    }
    _range = _user_dm_low.size();
    _SPS_mem_requirement = memory_requirement_of_SPS();
    _max_ndms = 0;
    make_strategy(gpu_memory);
}

template<typename NumericalRep, typename OptimizationParameterT>
DedispersionStrategy<NumericalRep , OptimizationParameterT>::~DedispersionStrategy()
{
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::range() const
{
    return _range;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::user_dm_low() const
{
    return _user_dm_low;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::user_dm_high() const
{
    return _user_dm_high;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::user_dm_step() const
{
    return _user_dm_step;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<int> DedispersionStrategy<NumericalRep , OptimizationParameterT>::in_bin() const
{
    return _in_bin;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<int> DedispersionStrategy<NumericalRep , OptimizationParameterT>::out_bin() const
{
    return _out_bin;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::maxshift() const
{
    return _maxshift;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::dm_low() const
{
    return _dm_low;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::dm_high() const
{
    return _dm_high;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm> DedispersionStrategy<NumericalRep , OptimizationParameterT>::dm_step() const
{
    return _dm_step;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<float> DedispersionStrategy<NumericalRep , OptimizationParameterT>::dmshifts() const
{
    return _dm_step;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<unsigned int> DedispersionStrategy<NumericalRep , OptimizationParameterT>::ndms() const
{
    return _ndms;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::max_ndms() const
{
    return _max_ndms;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::total_ndms() const
{
    return _total_ndms;
}

template<typename NumericalRep, typename OptimizationParameterT>
typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::Dm DedispersionStrategy<NumericalRep , OptimizationParameterT>::max_dm() const
{
    return _max_dm;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::vector<std::vector<int>> DedispersionStrategy<NumericalRep , OptimizationParameterT>::t_processed() const
{
    return _t_processed;
}

template<typename NumericalRep, typename OptimizationParameterT>
constexpr unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::nbits()
{
    return sizeof(NumericalRep)*8;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::nifs() const
{
    return _nifs;
}

template<typename NumericalRep, typename OptimizationParameterT>
void DedispersionStrategy<NumericalRep , OptimizationParameterT>::nifs(unsigned int value)
{
    _nifs = value;
}

template<typename NumericalRep, typename OptimizationParameterT>
typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::TimeType DedispersionStrategy<NumericalRep , OptimizationParameterT>::tsamp() const
{
    return _tsamp;
}

template<typename NumericalRep, typename OptimizationParameterT>
typename  DedispersionStrategy<NumericalRep , OptimizationParameterT>::FrequencyType DedispersionStrategy<NumericalRep , OptimizationParameterT>::fch1() const
{
    return _fch1;
}

template<typename NumericalRep, typename OptimizationParameterT>
typename DedispersionStrategy<NumericalRep , OptimizationParameterT>::FrequencyType DedispersionStrategy<NumericalRep , OptimizationParameterT>::foff() const
{
    return _foff;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::nsamp() const
{
    return _nsamp;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::nchans() const
{
    return _nchans;
}

template<typename NumericalRep, typename OptimizationParameterT>
unsigned int DedispersionStrategy<NumericalRep , OptimizationParameterT>::num_tchunks() const
{
    return _num_tchunks;
}

template<typename NumericalRep, typename OptimizationParameterT>
std::size_t DedispersionStrategy<NumericalRep , OptimizationParameterT>::memory_requirement_of_SPS()
{
    return((std::size_t) (5.5*sizeof(float) + 2*sizeof(ushort)));
}

// Most of the make_strategy code left unchanged. Remember the originla
// code is written for unsigned short. I have made changes where ever possible to
// using the Numericalrep template parameter.
template<typename NumericalRep, typename OptimizationParameterT>
void DedispersionStrategy<NumericalRep , OptimizationParameterT>::make_strategy(size_t const gpu_memory)
{

    _gpu_memory = gpu_memory;
    // This method relies on defining points when nsamps is a multiple of
    // nchans - bin on the diagonal or a fraction of it.

    unsigned int maxshift_high = 0;
    _maxshift = 0;
    float n;

    _dm_low.resize(_range,0);
    _dm_high.resize(_range,0);
    _dm_step.resize(_range,0);
    _ndms.resize(_range,0);
    _dmshifts.resize(_nchans,0);

    for (unsigned int c = 0; c < _nchans; c++)
    {
        _dmshifts[c] = std::abs((float)(_dm_constant.value() * ((1.0 / pow(_fch1.value()-_foff.value(), 2.0f )) - (1.0 / pow(_fch1.value(), 2.0f)))));
    }

    for (unsigned int i = 0; i < _range; i++)
    {
        modff(( ( (int) ( ( _user_dm_high[i] - _user_dm_low[i] ) / _user_dm_step[i] ) + OptimizationParameterT::SDIVINDM ) / OptimizationParameterT::SDIVINDM ), &n);
        _ndms[i] = (unsigned  int) ( (unsigned int) n * OptimizationParameterT::SDIVINDM );
        if (_max_ndms < _ndms[i])
            _max_ndms = _ndms[i];
        _total_ndms = _total_ndms + _ndms[i];
    }

    _dm_low[0] = _user_dm_low[0];
    _dm_high[0] = _ndms[0] * _user_dm_step[0];
    _dm_step[0] = _user_dm_step[0];

    float modulo = (float)(2 * OptimizationParameterT::SNUMREG * OptimizationParameterT::SDIVINT);
    for (unsigned int i = 1; i < _range; i++)
    {
        _dm_low[i] = _dm_high[i - 1];
        _dm_high[i] = _dm_low[i] + _ndms[i] * _user_dm_step[i];
        _dm_step[i] = _user_dm_step[i];

        if (_in_bin[i - 1] > 1) {
            _maxshift = (int) ceil(( ( _dm_low[i - 1].value() + _dm_step[i - 1].value() * _ndms[i - 1] ) * _dmshifts[_nchans - 1] ) / _tsamp.value());
            _maxshift = (int)ceil(((float)_maxshift / (float)_in_bin[i - 1] + modulo) / modulo);
            _maxshift = (int)(_maxshift * modulo * _in_bin[i - 1]);
            if (_maxshift > maxshift_high)
                maxshift_high = _maxshift;
        }
    }

    if (_in_bin[_range - 1] > 1) {
        _maxshift = (unsigned int) ceil(( ( _dm_low[_range - 1].value() + _dm_step[_range - 1].value() * _ndms[_range - 1] ) * _dmshifts[_nchans - 1] ) / _tsamp.value());
        _maxshift = (int)ceil(((float)_maxshift / (float)_in_bin[_range - 1] + modulo) / modulo);
        _maxshift = (unsigned int)(_maxshift * modulo * _in_bin[_range - 1]);
        if (_maxshift > maxshift_high)
            maxshift_high = _maxshift;
    }

    if (maxshift_high == 0) {
        maxshift_high = (unsigned int) ceil(( ( _dm_low[_range - 1].value() + _dm_step[_range - 1].value() * ( _ndms[_range - 1] ) ) * _dmshifts[_nchans - 1] ) / _tsamp.value());
    }
    _max_dm = ceil(_dm_high[_range - 1]);
    _maxshift = ( maxshift_high +  ( OptimizationParameterT::SNUMREG * sizeof(NumericalRep) * OptimizationParameterT::SDIVINT ) );

    if (_maxshift >= _nsamp) {
        PANDA_LOG_ERROR<<"ERROR!! Your maximum DM trial exceeds the number of samples you have.\nReduce your maximum DM trial \n";
        exit(1);
    }

    unsigned int max_tsamps;

    // Allocate memory to store the t_processed ranges:
    _t_processed.resize(_range);


    if (_nchans < _max_ndms ) {
        // This means that we can cornerturn into the allocated output buffer
        // without increasing the memory needed

        // Maximum number of samples we can fit in our GPU RAM is then given by:
        max_tsamps = (unsigned int) ( (gpu_memory) / ( sizeof(NumericalRep)*_nchans + sizeof(float)*(_max_ndms) +
                     (size_t)(_SPS_mem_requirement*OptimizationParameterT::MIN_DMS_PER_SPS_RUN )));

        // Check that we dont have an out of range maxshift:
        if (_maxshift  > max_tsamps)    {
            PANDA_LOG_ERROR<<"\nERROR!! Your GPU doesn't have enough memory for this number of dispersion trials. \n"
            <<"Reduce your maximum dm or increase the size of your dm step";
            exit(0);
        }

        // Next check to see if nsamp fits in GPU RAM:
        if (_nsamp < max_tsamps)    {
            // We have case 1)
            // Allocate memory to hold the values of nsamps to be processed
            unsigned int local_t_processed = (unsigned int) floor(( (float) ( _nsamp - _maxshift ) / (float) _in_bin[_range - 1] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
            local_t_processed = local_t_processed * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ) * _in_bin[_range - 1];
            for (unsigned int i = 0; i < _range; i++)    {
                _t_processed[i].resize(1);
                    _t_processed[i][0] = (int) floor(( (float) ( local_t_processed ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
                _t_processed[i][0] = _t_processed[i][0] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
            }
            _num_tchunks = 1;

        }
        else {
            // We have case 3)
            // Work out how many time samples we can fit into ram
            int samp_block_size = max_tsamps - _maxshift;
            //int samp_block_size = max_tsamps;

            // Work out how many blocks of time samples we need to complete the processing
            // upto nsamp-maxshift
            //int num_blocks = (int) floor(( (float) nsamp - ( *maxshift ) )) / ( (float) ( samp_block_size ) ) + 1;

            // Find the common integer amount of samples between all bins
            int local_t_processed = (int) floor(( (float) ( samp_block_size ) / (float) _in_bin[_range - 1] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
            unsigned int num_blocks;
                if(local_t_processed == 0) {
                    num_blocks = 0;

                } else {
                    local_t_processed = local_t_processed * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ) * _in_bin[_range - 1];
                    num_blocks = (int) floor(( (float) _nsamp - ((float)_maxshift) )) / ( (float) ( local_t_processed ) );
                }

            // Work out the remaining fraction to be processed
            int remainder =  _nsamp - ( num_blocks * local_t_processed ) - _maxshift;
            remainder = (int) floor((float) remainder / (float) _in_bin[_range - 1]) / (float) ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
            remainder = remainder * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ) * _in_bin[_range - 1];

            for (unsigned int i = 0; i < _range; i++)    {
                // Allocate memory to hold the values of nsamps to be processed
                _t_processed[i].resize(num_blocks+1);
                // Remember the last block holds less!
                for (unsigned j = 0; j < num_blocks ; j++) {
                    _t_processed[i][j] = (int) floor(( (float) ( local_t_processed ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ));
                    _t_processed[i][j] = _t_processed[i][j] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
                }
                // fractional bit
                _t_processed[i][num_blocks] = (int) floor(( (float) ( remainder ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ));
                _t_processed[i][num_blocks] = _t_processed[i][num_blocks] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
            }
            _num_tchunks = num_blocks + 1;
        }
    }
    else {
        // This means that we cannot cornerturn into the allocated output buffer
        // without increasing the memory needed. Set the output buffer to be as large as the input buffer:

        // Maximum number of samples we can fit in our GPU RAM is then given by:
        max_tsamps = (unsigned int) ( ( gpu_memory ) / ( _nchans * ( sizeof(float) + sizeof(unsigned short) )+ _SPS_mem_requirement*OptimizationParameterT::MIN_DMS_PER_SPS_RUN ));

        // Check that we dont have an out of range maxshift:
        if (_maxshift > max_tsamps) {
            PANDA_LOG_ERROR<<"\nERROR!! Your GPU doens't have enough memory for this number of dispersion trials."
            <<"\nReduce your maximum dm or increase the size of your dm step";
            exit(0);
        }

        // Next check to see if nsamp fits in GPU RAM:
        if (_nsamp < max_tsamps) {
            // We have case 2)
            // Allocate memory to hold the values of nsamps to be processed
            int local_t_processed = (int) floor(( (float) ( _nsamp - _maxshift ) / (float) _in_bin[_range - 1] ) / (float) ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ));
            local_t_processed = local_t_processed * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ) * _in_bin[_range - 1];
            for (unsigned int i = 0; i < _range; i++) {
                _t_processed[i].resize(1);
                _t_processed[i][0] = (int) floor(( (float) ( local_t_processed ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ));
                _t_processed[i][0] = _t_processed[i][0] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
            }
            _num_tchunks = 1;

        }
        else {
            // We have case 4)
            // Work out how many time samples we can fit into ram
            unsigned int samp_block_size = max_tsamps - _maxshift;

            // Work out how many blocks of time samples we need to complete the processing
            // upto nsamp-maxshift
            //int num_blocks = (int) floor(( (float) nsamp - (float) ( *maxshift ) ) / ( (float) samp_block_size ));

            // Find the common integer amount of samples between all bins
            unsigned int local_t_processed = (unsigned int) floor(( (float) ( samp_block_size ) / (float) _in_bin[_range - 1] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
            local_t_processed = local_t_processed * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG ) * _in_bin[_range - 1];

            // samp_block_size was not used to calculate remainder instead there is local_t_processed which might be different
            unsigned int num_blocks = (unsigned int) floor(( (float) _nsamp - (float) _maxshift ) / ( (float) local_t_processed ));

            // Work out the remaining fraction to be processed
            unsigned int remainder = _nsamp - ( num_blocks * local_t_processed ) - _maxshift;

            for (unsigned int i = 0; i < _range; i++)    {
                // Allocate memory to hold the values of nsamps to be processed
                _t_processed[i].resize(num_blocks + 1);
                // Remember the last block holds less!
                for (unsigned int j = 0; j < num_blocks; j++) {
                    _t_processed[i][j] = (int) floor(( (float) ( local_t_processed ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
                    _t_processed[i][j] = _t_processed[i][j] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
                }
                // fractional bit
                _t_processed[i][num_blocks] = (int) floor(( (float) ( remainder ) / (float) _in_bin[i] ) / (float) ( OptimizationParameterT::SDIVINT * 2 * OptimizationParameterT::SNUMREG ));
                _t_processed[i][num_blocks] = _t_processed[i][num_blocks] * ( OptimizationParameterT::SDIVINT * sizeof(NumericalRep) * OptimizationParameterT::SNUMREG );
            }
            _num_tchunks = num_blocks + 1;

        }
    }

    _dedispersed_time_samples = 0;
    for (unsigned t = 0; t < _num_tchunks; ++t)
    {
        _dedispersed_time_samples += (_t_processed[0][t] * _in_bin[0]);
    }
}


}//astroacclerate
}//sps
}//cheetah
}//ska
