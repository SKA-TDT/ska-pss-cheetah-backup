/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_DDTRMODULE_H
#define SKA_CHEETAH_DDTR_DDTRMODULE_H

#include "cheetah/ddtr/detail/Buffering.h"
#include "panda/ConfigurableTask.h"
#include "panda/Method.h"

namespace ska {
namespace cheetah {
namespace ddtr {

/**
 * @brief generate code for integration of DdtrAlgorithms
 * @details
 */

template<typename DdtrTraits, template<typename> class... DdtrAlgorithms>
class DdtrModule
{
    private:
        typedef CommonDedispersionPlan<DdtrTraits, DdtrAlgorithms<DdtrTraits>...> DedispersionPlanType;

        struct SetDedispersionHelper
        {
            template<typename Algo>
            void operator()(Algo& algo, DedispersionPlanType const& plan) {
                algo.plan(plan.template plan<Algo>());
            }
        };

        typedef panda::ConfigurableTask<typename DdtrTraits::Pool
                                      , typename DdtrTraits::DedispersionHandler
                                      , panda::Method<SetDedispersionHelper, DedispersionPlanType const&>
                                      , typename DdtrTraits::BufferType&&> TaskType;

        typedef typename DdtrTraits::Config ConfigType;

    public:
        template<typename DedispersionHandler>
        DdtrModule(ConfigType const& config, DedispersionHandler&& handler);

    private:
        struct ExtendedDedispersionPlan : public DedispersionPlanType
        {
            typedef DedispersionPlanType BaseT;
            ExtendedDedispersionPlan(ConfigType const& config, TaskType& task)
                : BaseT(config)
                , _task(task)
            {
            }

            void set_plans() override {
                _task(static_cast<BaseT const&>(*this));
            }

            private:
                TaskType& _task;

        };

    private:
        SetDedispersionHelper _plan_setter;

    protected:
        TaskType _task;
        Buffering<DdtrTraits, ExtendedDedispersionPlan> _buffer;
};


} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "cheetah/ddtr/detail/DdtrModule.cpp"

#endif // SKA_CHEETAH_DDTR_DDTRMODULE_H
