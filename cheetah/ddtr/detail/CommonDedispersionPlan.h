/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_COMMONDEDISPERSIONPLAN_H
#define SKA_CHEETAH_DDTR_COMMONDEDISPERSIONPLAN_H

#include "cheetah/ddtr/cpu/DedispersionPlan.h"
#include "cheetah/ddtr/cpu/Ddtr.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/data/Units.h"
#include "panda/ConfigurableTask.h"
#include "panda/Method.h"
#include <tuple>


namespace ska {
namespace cheetah {
namespace ddtr {

template<typename AlgoT>
struct PlanType {
    typedef typename AlgoT::DedispersionPlan type;
};



template<typename TraitsT, typename... AlgosT>
class CommonDedispersionPlan
{
        typedef typename TraitsT::TimeFrequencyType TimeFrequencyType;
        typedef std::tuple<typename AlgosT::Architecture ...> Architectures;
        typedef typename TraitsT::Config ConfigType;

    public:
        CommonDedispersionPlan(ConfigType const& config);
        virtual ~CommonDedispersionPlan();

        data::DimensionSize<data::Time> buffer_overlap() const;
        data::DimensionSize<data::Time> dedispersion_strategy(TimeFrequencyType const&);

        /**
         * @brief return the plan corresponding to the specified algorithm
         */
        template<typename AlgoT>
        typename PlanType<AlgoT>::type const& plan() const;

   protected:
        virtual void set_plans() = 0;

   private:
        std::tuple<typename PlanType<AlgosT>::type...> _plans;
};

} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "cheetah/ddtr/detail/CommonDedispersionPlan.cpp"
#endif // SKA_CHEETAH_DDTR_COMMONDEDISPERSIONPLAN_H
