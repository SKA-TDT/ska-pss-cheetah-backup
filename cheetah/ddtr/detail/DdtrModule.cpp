/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/utils/TaskConfigurationSetter.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace detail {

template<typename T, typename DdtrTraits>
struct CreateHelper
{
    template<typename DdtrAlgoFactory>
    static inline T create(DdtrAlgoFactory& algo_factory)
    {
        return T(algo_factory._config);
    }
};

template<typename DdtrTraits>
struct CreateHelper<cpu::Ddtr<DdtrTraits>, DdtrTraits>
{
    template<typename DdtrAlgoFactory>
    static inline cpu::Ddtr<DdtrTraits> create(DdtrAlgoFactory& algo_factory)
    {
        return cpu::Ddtr<DdtrTraits>(algo_factory._config);
    }
};

template<typename DdtrTraits>
struct DdtrAlgoFactory
{
    private:
        template<typename, typename>
        friend struct CreateHelper;
        typedef typename DdtrTraits::Config ConfigType;

    public:
        DdtrAlgoFactory(ConfigType const& config)
            : _config(config)
        {}

        void none_selected() {
            PANDA_LOG_WARN << "ddtr:: no algorithm has been activated. Please set at least one ddtr algorithm to active.";
        }

        template<typename T> T create() const
        {
            return CreateHelper<T, DdtrTraits>::create(*this);
        }

        template<typename Algo>
        bool active() const
        {
            return _config.template config<typename Algo::Config>().active();
        }

    private:
        ConfigType const& _config;
};

} // namespace detail

template<typename DdtrTraits, template<typename> class... DdtrAlgorithms>
template<typename Handler>
DdtrModule<DdtrTraits, DdtrAlgorithms...>::DdtrModule(ConfigType const& config, Handler&& handler)
    : _task(config.pool(), std::forward<Handler>(handler), _plan_setter)
    , _buffer([this](typename DdtrTraits::BufferType&& buffer)
              {
                  if(buffer.composition().empty())
                  {
                      PANDA_LOG_WARN << "received an empty buffer";
                      return;
                  }
                  _task.submit(std::move(buffer));
              }
              , ExtendedDedispersionPlan(config, _task)
              , config.dedispersion_samples()
              )
{
    detail::DdtrAlgoFactory<DdtrTraits> factory(config);
    utils::TaskConfigurationSetter<DdtrAlgorithms<DdtrTraits>...>::configure(_task, factory);
}

} // namespace ddtr
} // namespace cheetah
} // namespace ska
