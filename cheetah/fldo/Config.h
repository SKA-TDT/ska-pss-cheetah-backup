/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CONFIG_H
#define SKA_CHEETAH_FLDO_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/fldo/cpu/Config.h"
#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/data/Units.h"
#include "panda/ProcessingEngine.h"
#include "panda/ResourcePool.h"
#include "panda/arch/nvidia/Nvidia.h"
#include "panda/MultipleConfigModule.h"
#include "panda/PoolSelector.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace fldo {

/**
 * @brief
 *    Configuration details for the fldo module
 *
 * @details
 *    Contains common algorithm independent configuration options
 *    and access to the config objects of specific algorithms
 */

class Config : public panda::MultipleConfigModule<utils::Config
                                                , cpu::Config
                                                , cuda::Config
                                                 >
{
        typedef panda::MultipleConfigModule<utils::Config
                                           , cpu::Config
                                           , cuda::Config
                                           > BaseT;

    public:
        typedef data::TimeType TimeType;

    public:
        Config();

        /**
         * @brief return the cpu algorithm specific options
         */
        fldo::cpu::Config const& cpu_algo_config() const;
        fldo::cpu::Config& cpu_algo_config();

        /**
         * @brief return the CUDA algorithm specific options
         */
        fldo::cuda::Config const& cuda_algo_config() const;
        fldo::cuda::Config& cuda_algo_config();

    protected:
        void add_options(OptionsDescriptionEasyInit&) override;

};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FLDO_CONFIG_H
