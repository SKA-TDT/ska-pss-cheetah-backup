/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cpu/test/CpuTest.h"
#include "cheetah/fldo/cpu/Fldo.h"
#include "cheetah/fldo/test_utils/FldoTester.h"


namespace ska {
namespace cheetah {
namespace fldo {
namespace test {


CpuTest::CpuTest()
    : ::testing::Test()
{
}

CpuTest::~CpuTest()
{
}

void CpuTest::SetUp()
{
}

void CpuTest::TearDown()
{
}

/* white box testing stuff here (no device available)
TEST_F(CpuTest, test_something)
{
}
*/

struct Dummy;

template<typename NumericalT>
struct CpuTraits : public FldoTesterTraits<fldo::cpu::Fldo<fldo::FldoTraits<Dummy, Dummy, NumericalT>>>
{
};

typedef ::testing::Types<CpuTraits<uint8_t>, CpuTraits<uint16_t>> CpuTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cpu, FldoTester, CpuTraitsTypes);

} // namespace test
} // namespace fldo
} // namespace cheetah
} // namespace ska
