include_directories(${GTEST_INCLUDE_DIR})

link_directories(${GTEST_LIBRARY_DIR})

set(
    gtest_fldo_src
    src/ConfigTest.cpp
    src/gtest_fldo.cpp
)
add_executable(gtest_fldo ${gtest_fldo_src} )
target_link_libraries(gtest_fldo ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fldo gtest_fldo --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
