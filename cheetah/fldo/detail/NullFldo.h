/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_NULLFLDO_H
#define SKA_CHEETAH_FLDO_NULLFLDO_H


namespace ska {
namespace cheetah {
namespace fldo {

/**
 * @brief A do nothing Fldo algorithm. This allows us to run A pipeline and get intermediate products without
 *        actually doing any folding
 */

template<typename FldoTraits>
class NullFldo
{
    public:
        typedef cheetah::Cpu Architecture;
        typedef panda::PoolResource<Architecture> ResourceType;
        typedef cuda::Config Config;
        typedef FldoTraits Traits;

    private:
        typedef typename FldoTraits::TimeFrequencyType TimeFrequencyType;

    public:
        std::shared_ptr<data::Ocld> operator()(ResourceType&
                                             , std::vector<std::shared_ptr<TimeFrequencyType>> const&
                                             , data::Scl const&)
        {
            std::shared_ptr<data::Ocld> output = data::Ocld::make_shared();
            return output;
        }
};


} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FLDO_NULLFLDO_H
