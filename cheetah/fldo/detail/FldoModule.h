/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_FLDOMODULE_H
#define SKA_CHEETAH_FLDO_FLDOMODULE_H

#include "panda/ConfigurableTask.h"

namespace ska {
namespace cheetah {
namespace fldo {

template<typename ConfigT, typename HandlerT, typename NumericalRep>
class FldoTraits {
    public:
        typedef ConfigT ConfigType;
        typedef HandlerT Handler;
        typedef data::TimeFrequency<Cpu, NumericalRep> TimeFrequencyType;
};

/**
 * @brief Setup template paramterised Fldo algorithms for use as a module
 */
template<class FldoTraitsType, typename... FldoAlgos>
class FldoModule
{
        typedef typename FldoTraitsType::ConfigType Config;
        typedef typename FldoTraitsType::Handler Handler;

    protected:
        typedef typename FldoTraitsType::TimeFrequencyType TimeFrequencyType;

    public:
        FldoModule(Config const&, Handler&);

        /**
         * @brief call this operator to submit a job for folding
         * @details the handler passed in the constructor will be called when
         *          the job is complate
         */
        std::shared_ptr<panda::ResourceJob> operator()(std::vector<std::shared_ptr<TimeFrequencyType>>& tf_data
                                                     , data::Scl const& scl_data);

    private: // typedefs
        typedef panda::ConfigurableTask<typename Config::PoolType, Handler&
                                      , std::vector<std::shared_ptr<TimeFrequencyType>>
                                      , data::Scl const&> TaskType;

    private: // members
        TaskType _task;

};


} // namespace fldo
} // namespace cheetah
} // namespace ska

#include "cheetah/fldo/detail/FldoModule.cpp"

#endif // SKA_CHEETAH_FLDO_FLDOMODULE_H
