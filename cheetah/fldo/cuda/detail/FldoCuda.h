/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CUDA_FLDOCUDA_H
#define SKA_CHEETAH_FLDO_CUDA_FLDOCUDA_H

#ifdef ENABLE_CUDA

#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/cuda/detail/FldoUtils.h"
#include "cheetah/fldo/Types.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

/**
 * @brief
 *    The interface for the CUDA FLDO algorithm
 * @details
 */

template<typename NumericalT>
class FldoCuda
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapabilty; // minimum device requirements
        typedef panda::PoolResource<Architecture> ResourceType;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef NumericalT NumericalRep;
        typedef data::TimeType TimeType;
        typedef data::TimeFrequency<Cpu, NumericalRep> TimeFrequencyType;
        typedef boost::units::quantity<boost::units::si::time, int> ScanTimeType;
        typedef data::Candidate<Cpu, float> CandidateType;

    public:
        FldoCuda(fldo::Config const& config);

        /**
         * @brief performs the folding operation on the provided data
         */
        std::shared_ptr<data::Ocld> operator()(ResourceType& device
                                              , std::vector<std::shared_ptr<TimeFrequencyType>> const& data
                                              , data::Scl const& input_candidates);

    private:
        /**
         * @brief Sorts the candidates in period ascending order and builds a vector of CandidateRebin structures.
         */
        void rebin_candidates(std::vector<util::CandidateRebin>& rebin, TimeType const tsamp, data::Scl &scl_data);

        /**
         * @brief Loads data in device constant memory
         */
        void load_constant_devmem(std::vector<util::CandidateRebin>& rebin, std::vector<double> delta_freqs,
                                  data::Scl const &input_candidates, std::vector <int> &nbins);

        /**
         * @brief Allocates global device memory
         */
        void allocate_global_devmem(size_t ncand, std::vector<util::CandidateRebin> &rebin, NumericalRep **d_in,
                                    NumericalRep ** h_in, float ** d_outfprof, float **d_outprof, float ** d_folded,
                                    float ** d_weight);

        /**
         * @brief Performs corner-turner of input data and fold them for
         *        each input candidate
         */
        void folding(ResourceType& gpu, std::vector<util::CandidateRebin> &rebin, NumericalRep *d_in, float *d_folded,
                     float *d_weight, int *nbins, int isubint, float *data_mean, float *data_sigma,
                     float &gpu_time, std::vector<util::GpuStream>& exec_stream);

        /**
         * @brief Normalizes and scrunches in time and freq the folded profiles.
         */
        void profiles(size_t ncandidates, float mean, float *d_folded, float *d_weight, float *d_outfprof,
                      float* d_outprof, float & gpu_time, std::vector<util::GpuStream> &exec_stream);
        /**
          * @brief Normalizes and scrunches in time and freq the folded profiles.
          */
          void run_optimization(data::Scl const& ori_data, data::Scl & opt_data, float *d_folded,
                                float* d_perturbed, float *d_outfprof,
                                std::vector<util::CandidateRebin> &rebin, std::vector<double> const &time_sum,
                                std::vector <double> const & freq_sum,float &total_gpu_time,
                                std::vector<util::GpuStream>& exec_stream, int p_trial, int p_rf,
                                int pdot_trial, int pdot_rf, int dm_trial, int dm_rf, float sigma, int verbose);
        /**
         * @brief Releases the allocated device memory
         */
        void free_dev_memory(NumericalRep *h_in, float *d_folded, float *d_weight, float *d_outprof, float * d_outfprof);

        /*
         * @brief Run the whole process (folding + optimization)
         */
        std::shared_ptr<data::Ocld> run_folding(ResourceType& device,
                                                std::vector<std::shared_ptr<TimeFrequencyType>> const& data,
                                                data::Scl input_candidates);

        /**
         * data_reshape
         * @brief
         * add control and re-formatting of the input data
         *
         * @details
         * Folding routine assumes input data are organized as a a vector of _nsubints
         * components, each containing (approx) the same number of measures
         * data_reshape accept a generic vector, and if correct, return
         * unchanged. if not the case, it reshape it as required.
         *
         */
        void data_reshape(std::vector<std::shared_ptr<TimeFrequencyType>> const& input_data,
                          std::vector<std::shared_ptr<TimeFrequencyType>> const& data);

    private:
        fldo::Config const& _config;
        bool _enable_split;     //< flag to enable/disable phase shift in folding algorithm
        size_t _mfold_phases;   //< max number of phases (bins) to use in folding
        size_t _nchannels;      //< the number of frequency channels
        uint64_t _nsamp;        //< the total number of samples
        size_t _nsubbands;      //< the number of sub-bands used by folding procedure
        size_t _nsubints;       //< the numner of sub-integrations used by folding procedure
        double _tobs;           //< the total observing time (in sec.)
        double _tsamp;          //< the sampling time (in sec.)
};

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif //ENABLE_CUDA

#endif // SKA_CHEETAH_FLDO_CUDA_FLDO_H
