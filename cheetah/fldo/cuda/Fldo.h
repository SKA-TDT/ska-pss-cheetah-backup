/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_CUDA_FLDO_H
#define SKA_CHEETAH_FLDO_CUDA_FLDO_H
#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/fldo/Types.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

template<typename FldoTraits
        ,typename Enable=void>
class Fldo
{
    public:
        typedef cheetah::Cpu Architecture;
        typedef panda::PoolResource<Architecture> ResourceType;
        typedef cuda::Config Config;
        typedef FldoTraits Traits;

    private:
        typedef typename FldoTraits::TimeFrequencyType TimeFrequencyType;

    public:
        Fldo(fldo::Config const&) {
#ifdef ENABLE_CUDA
            throw panda::Error("fldo cuda origami algo does not exist for this data type");
#else  // ENABLE_CUDA
            throw panda::Error("fldo cuda(origami) algo has not been compiled in. recompile with cmake -DENABLE_CUDA=true option");
#endif // ENABLE_CUDA
        };

        std::shared_ptr<data::Ocld> operator()(ResourceType&
                                             , std::vector<std::shared_ptr<TimeFrequencyType>> const&
                                             , data::Scl const&)
        {
            std::shared_ptr<data::Ocld> output = data::Ocld::make_shared();
            return output;
        }
};

} // namespace ska
} // namespace cheetah
} // namespace fldo
} // namespace cuda

#ifdef ENABLE_CUDA
#include "cheetah/fldo/cuda/detail/Fldo.cuh"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

// 8-bit implemntations
template<typename FldoTraits>
class Fldo<FldoTraits
        , typename std::enable_if<std::is_same<typename FldoTraits::TimeFrequencyType::value_type, uint8_t>::value>::type>
    : public detail::Fldo<FldoTraits>
{
        typedef detail::Fldo<FldoTraits> BaseT;

    public:
        typedef cheetah::Cuda Architecture;
        typedef typename BaseT::ArchitectureCapability ArchitectureCapability;

        typedef panda::PoolResource<Architecture> ResourceType;
        typedef typename BaseT::Config Config;
        typedef FldoTraits Traits;

    public:
        using detail::Fldo<FldoTraits>::Fldo;

        /**
         * @brief call the cuda kernel to calcualte fldo
         */
        inline
        std::shared_ptr<data::Ocld> operator()(ResourceType& res
                                             , std::vector<std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>>> const& data
                                             , data::Scl const& scl)
        {
            return BaseT::operator()(res, data, scl);
        }
};

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif //ENABLE_CUDA


#endif // SKA_CHEETAH_FLDO_CUDA_FLDO_H
