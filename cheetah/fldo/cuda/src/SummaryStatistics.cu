/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/cuda_utils/cuda_thrust.h"
#include <cmath>
#include <limits>

//
// This example computes several statistical properties of a data
// series in a single reduction.  The algorithm is described in detail here:
// http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Parallel_algorithm
//
// Adapted from Thrust example code
//

// structure used to accumulate the moments and other
// statistical properties encountered so far.
namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

template <typename T>
struct summary_stats_data
{
    T n;
    T mean;
    T M2;

    // initialize to the identity element
    void initialize()
    {
      n = mean = M2 = 0;
    }

    T variance()   { return M2 / (n - 1); }
    T variance_n() { return M2 / n; }
};

// stats_unary_op is a functor that takes in a value x and
// returns a variace_data whose mean value is initialized to x.
template <typename T>
struct summary_stats_unary_op
{
    __host__ __device__
    summary_stats_data<T> operator()(const unsigned char& x) const
    {
         summary_stats_data<T> result;
         result.n    = 1;
         result.mean = (float)x - 128.;
         result.M2   = 0;

         return result;
    }
};

template <typename T>
struct summary_stats_unary_float_op
{
    __host__ __device__
    summary_stats_data<T> operator()(const T& x) const
    {
         summary_stats_data<T> result;
         result.n    = 1;
         result.mean = (float)x ;
         result.M2   = 0;

         return result;
    }
};

// summary_stats_binary_op is a functor that accepts two summary_stats_data
// structs and returns a new summary_stats_data which are an
// approximation to the summary_stats for
// all values that have been agregated so far
template <typename T>
struct summary_stats_binary_op
    : public thrust::binary_function<const summary_stats_data<T>&,
                                     const summary_stats_data<T>&,
                                           summary_stats_data<T> >
{
    __host__ __device__
    summary_stats_data<T> operator()(const summary_stats_data<T>& x, const summary_stats_data <T>& y) const
    {
        summary_stats_data<T> result;

        // precompute some common subexpressions
        T n  = x.n + y.n;
        //T n2 = n  * n;

        T delta  = y.mean - x.mean;
        //printf("y.mean: %f x.mean: %f x.n: %d\n", y.mean, x.mean, x.n);
        T delta2 = delta  * delta;

        //Basic number of samples n
        result.n   = n;
        result.mean = x.mean + delta * y.n / n;

        result.M2  = x.M2 + y.M2;
        result.M2 += delta2 * x.n * y.n / n;
        return result;
    }
};

/*
 * void statistics(unsigned char *raw_in, int N, float *mean, float *rms, cudaStream_t stream)
 *
 * @brief Calculates data statistics (mean and variance) of the input data
 * (unsigned char).
 *
 * @param raw_in        device pointer to input data
 * @param N             the number of data to process
 * @param mean          the calculcated mean value
 * @param rms           the calculcated rms value
 * @param stream        the cuda stream to run kernels
 *
 */
void statistics(unsigned char *raw_in, int N, float *mean, float *rms, cudaStream_t stream)
{
    typedef float T;

    // initialize host array

    // transfer to device
    thrust::cuda::par.on(stream);
    thrust::device_ptr<unsigned char> dev_in = thrust::device_pointer_cast(raw_in);

    // setup arguments
    summary_stats_unary_op<T>  unary_op;
    summary_stats_binary_op<T> binary_op;
    summary_stats_data<T>      init;

    init.initialize();

    // compute summary statistics
    summary_stats_data<T> result = thrust::transform_reduce(dev_in, dev_in + N, unary_op, init, binary_op);
    *mean = result.mean;
    *rms = sqrt(result.variance_n());
    return;
}

/*
 * void statistics_float(float *raw_in, int N, float *mean, float *rms, cudaStream_t stream)
 *
 * @brief Calculates data statistics (mean and variance) of the input data
 * (unsigned char).
 *
 * @param raw_in        device pointer to input data
 * @param N             the number of data to process
 * @param mean          the calculcated mean value
 * @param rms           the calculcated rms value
 * @param stream        the cuda stream to run kernels
 *
 */
void statistics_float(float *raw_in, int N, float *mean, float *rms, cudaStream_t stream)
{
    typedef float T;

    // initialize host array

    // transfer to device
    thrust::cuda::par.on(stream);
    thrust::device_ptr<T> dev_in = thrust::device_pointer_cast(raw_in);

    // setup arguments
    summary_stats_unary_float_op<T>  unary_op;
    summary_stats_binary_op<T> binary_op;
    summary_stats_data<T>      init;

    init.initialize();

    // compute summary statistics
    summary_stats_data<T> result = thrust::transform_reduce(dev_in, dev_in + N, unary_op, init, binary_op);
    *mean = result.mean;
    *rms = sqrt(result.variance_n());
    return;
}

void find_max(float *raw_in, int start, int N, float &sn_max, float &pulse_width, int &index)
{
    typedef float T;
    thrust::device_ptr<float> d_in = thrust::device_pointer_cast(raw_in);

    //thrust::device_vector<float>::iterator iter = thrust::max_element(d_in, d_in + N);
    thrust::device_ptr<float> max_sn_pos;
    thrust::device_ptr<float> width_pos;
    max_sn_pos = thrust::max_element(d_in + start, d_in + start + N);

    index =  max_sn_pos - (d_in + start);
    sn_max = *max_sn_pos;
    width_pos = d_in + start + index + N;
    pulse_width = *width_pos;
}
} // util
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
