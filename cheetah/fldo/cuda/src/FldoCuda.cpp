/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/Fldo.h"
#include "cheetah/fldo/cuda/Config.h"
#include "cheetah/fldo/cuda/detail/FldoCuda.h"
#include "cheetah/fldo/cuda/detail/Optimization.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include <climits>
#include <fstream>

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {

#ifdef ENABLE_CUDA

template<typename NumericalT>
FldoCuda<NumericalT>::FldoCuda(fldo::Config const& config)
    : _config(config)
{
    //cuda configuration
    fldo::cuda::Config const& cuda_config = _config.cuda_algo_config();
    //initialize class data members
    //max number of phases used in folding
    _mfold_phases = cuda_config.phases();
    _nsubbands = cuda_config.nsubbands();
    _nsubints = cuda_config.nsubints();
    //enable_split flag to enable/disable phase splitting in folding algorithm
    _enable_split = cuda_config.enable_split();
}

template<typename NumericalT>
std::shared_ptr<data::Ocld> FldoCuda<NumericalT>::operator()(ResourceType& gpu,
                                             std::vector<std::shared_ptr<TimeFrequencyType>> const& input_data,
                                             data::Scl const& candidates_list)
{

    // local copy formatted as espected by folder routines
    std::vector<std::shared_ptr<TimeFrequencyType>> data;
    data::Scl input_candidates = candidates_list;

    // standard control if no data in input return NULL
    if ((0 == input_data.size()) || (0 == input_data[0]->number_of_spectra() )) {
        PANDA_LOG_ERROR << "Fldo: data buffer size == 0";
        return data::Ocld::make_shared();
    }

    // Control on candidate list size
    PANDA_LOG_DEBUG << "Fldo: Total candidates number == " << input_candidates.size();
    PANDA_LOG       << "Fldo: Total candidates number == " << input_candidates.size();

    // Candidates number > 0
    if (0 == input_candidates.size() ) {
        PANDA_LOG_ERROR << "Fldo: candidate buffer size == 0";
        return data::Ocld::make_shared();
    }

    // control and re-formatting of the input data
    //
    // Folding routine assumes input data are organized as a a vector of _nsubints
    // components, each containing (approx) the same number of measures.
    // data_reshape accepts a generic vector, and if correct, returns it
    // unchanged. If not the case, it reshapes it as required.
    //

    {
        // compute total data sample size along the axis time
        uint64_t totalsize = 0;
        uint64_t minsize  = ULLONG_MAX;
        uint64_t maxsize  = 0;
        for (int i = 0; i < (int) input_data.size(); i++) {
            totalsize += input_data[i]->number_of_spectra();
            // verify if all chunks are the same. The chunk size =
            // numer_of_time_sample_in_chunk * numer_of_channels
            if (minsize > input_data[i]->data_size()){
                minsize = input_data[i]->data_size();
            }
            if (maxsize < input_data[i]->data_size()) {
                maxsize = input_data[i]->data_size();
            }
        }

        //check if the configured number of channels matches with the one inside data
        //NOTE: we assume all elements of input_data have the same channels number
        _nchannels = input_data[0]->number_of_channels();
        if(_nchannels < fldo::cuda::min_frequencies || _nchannels > fldo::cuda::max_frequencies) {
            PANDA_LOG_ERROR << "number of channels outside range for this algorithm ("
                            << min_frequencies << ", " << max_frequencies << "): " << _nchannels;
            return data::Ocld::make_shared();
        }

        //calculate the total number of samples
        uint64_t nsamples = totalsize * _nchannels;

        // we want the data number to be a multiple also of the fldo::max_prebin value
        if ((nsamples % (_nchannels * _nsubints * fldo::cuda::max_prebin)) != 0) {
            nsamples = (uint64_t)((nsamples/(((float)_nsubints *_nchannels) * fldo::cuda::max_prebin)))
                                 * (_nsubints*_nchannels * fldo::cuda::max_prebin);
            if (nsamples < 1) {
                 PANDA_LOG_ERROR << "Not enough data to process at max rebin value" << fldo::cuda::max_prebin;
                 return data::Ocld::make_shared();
            }
        }
        _nsamp = nsamples;

        // if maxsize == minsize we have the correct input data structure.
        // if also input_data.size() == _nsubints all work is done.
        if ((maxsize == minsize) && (input_data.size() == _nsubints) && (_nsamp/(_nsubints) == minsize)) {
            PANDA_LOG_DEBUG << "Fldo: input data already formatted (" << totalsize << " measures)";
            // dumb (and fast) copy
            data = input_data;
        } else {
            // we build the correctly formatted data
            PANDA_LOG_DEBUG << "Fldo: input data not correctly formatted (" << totalsize << " measures)";

            const size_t measures = _nsamp / _nsubints / _nchannels; // number of time-samples in a sub-integration
            PANDA_LOG_DEBUG << "measures: " << measures;
            // here we are sure all components of input_data have the same channels number
            const size_t channels = _nchannels;
            int input_ind = 0;              // index inside input_data[]
            int input_insideind = 0;        // index inside input_data[]-><data>
            size_t destination_start = 0;   // index inside data[]->data()
            for (int i = 0; i < (int) _nsubints; i++) {
                // we build data[i] containing a full sub integration of
                // length measures
                data.emplace_back(new TimeFrequencyType(data::DimensionSize<data::Time>(measures), data::DimensionSize<data::Frequency>(channels)));
                // start_time has to be re-calculated for each sub-int
                // Wrong : data[i]->start_time(input_data[input_ind]->start_time());
                // these does work.  (input_data[0] is correct as input_data has no time holes).
                time_t miot = input_data[0]->start_time().to_time_t();
                miot += (measures*i*(input_data[0]->sample_interval().value()));
                data[i]->start_time( miot);
                data[i]->sample_interval(input_data[input_ind]->sample_interval());
                data[i]->set_channel_frequencies(input_data[input_ind]->channel_frequencies().begin(),
                                                 input_data[input_ind]->channel_frequencies().end());
                // we get measures numbers from input_data.
                // NOTE we use the more efficient std::copy. This assume
                //      the current (1/2017) implementation of TimeData.
                //      A more general approach, using iterators, is much slower.
                // TODO move this while inside data_reshape routine
                // if we have enough data in input_data[input_ind] we
                // copy them.
                size_t left = measures * channels;       // data still to be transferred
                while (0 < left) {
                    size_t data_size = (input_data[input_ind]->end() - (input_data[input_ind]->begin() + input_insideind ));
                    if (left >= data_size) {
                        //PANDA_LOG_DEBUG << "(1) subint: " << i << " left: " << left << " data_size: " << data_size
                        //                << " input_ind: " << input_ind << " input_insideind: " << input_insideind;
                        // we have enough data inside input_data[input_ind]
                        std::copy((input_data[input_ind]->begin() + input_insideind),
                                    (input_data[input_ind]->begin() + input_insideind + data_size),
                                    (data[i]->begin() + destination_start));
                        ++input_ind;            // we have no more items in input_data[input_ind]
                        input_insideind = 0;
                        if (left == data_size) {
                            destination_start = 0;
                        } else {
                            destination_start += data_size;  // update destination index
                        }
                        left -= data_size;
                        //PANDA_LOG_DEBUG << "(1): destination_start = " << destination_start;
                    } else  {
                        // we have only current_copy data inside input_data[input_ind]
                        //PANDA_LOG_DEBUG << "(2) subint: " << i << " left: " << left << " data_size: " << data_size
                         //               << " input_ind: " << input_ind << " input_insideind: " << input_insideind;
                        std::copy ( (input_data[input_ind]->begin() + input_insideind),
                                    (input_data[input_ind]->begin() + input_insideind + left),
                                    (data[i]->begin() + destination_start));
                        destination_start = 0;      // update destination index
                        input_insideind += left;    // we have no more items in input_data[input_ind]
                        left = 0;
                    }
                }
            }
        }
    }

    // compute total data sample size along the time axis
    uint64_t totalsize = 0;
    for (int i = 0; i < (int) data.size(); ++i) {
        totalsize += data[i]->number_of_spectra();
    }
    PANDA_LOG << "Fldo: Total number of time samples " << totalsize
              << " in "                        << data.size() << " chunks"
              << " each with "                 << data[0]->number_of_channels()
              << " frequency channels ";

    return run_folding(gpu, data, input_candidates);
}

/**
 * void FldoCuda<NumericalT>::rebin_candidates(std::vector<util::CandidateRebin> &rebin, TimeType tsamp, data::Scl & scl_data)
 *
 * @details
 * This method sorts the candidates on period ascending order and divides
 * the candidates in different rebin groups.
 * Note: if the periods are the same,  the candidates are ordered on dm (and then on pdot) ascending order.
 * Candidates belonging to the same rebin group have a period less then the
 * period associated to the specific rebin group.
 * The characteristic period value of each rebin group is calculated starting
 * from the max number of phases bins (128), the sampling time and the
 * rebinning factor (1, 2, 4, 8,..)
 *
 * @param rebin         the vector of CandidateRebin structures
 * @param tsamp         the data sampling time
 * @param scl_data      the list of pulsar candidates
 *
 * @return      no value
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::rebin_candidates(std::vector<util::CandidateRebin> &rebin, TimeType const tsamp, data::Scl& scl_data)
{
    PANDA_LOG_DEBUG << "Call rebin_candidates";
    //sort the candidates on period basis using lambda function (ascending order)
    //std::sort (scl_data.begin(), scl_data.end(), [](CandidateType first, CandidateType second) {
    //                                    return (first.period() < second.period());
    //                                 }
    //                                 );
    std::sort (scl_data.begin(), scl_data.end(), [](CandidateType first, CandidateType second) -> bool {
                                        if (first.period() < second.period()) {
                                            return true;
                                        } else if (first.period() > second.period()) {
                                            return false;
                                        }
                                        if (first.dm() < second.dm()) {
                                            return true;
                                        } else if (first.dm() > second.dm()) {
                                            return false;
                                        }
                                        if (first.pdot() < second.pdot()) {
                                            return true;
                                        } else if (first.pdot() > second.pdot()) {
                                            return false;
                                        }
                                        return false;
                                     }
                                     );

    // print candidates after sorting
    for (data::VectorLike<std::vector<data::Scl::CandidateType>>::ConstIterator it = scl_data.begin() ;
         it != scl_data.end(); ++it) {
             PANDA_LOG_DEBUG << "Sorted candidate "
                             << (*it).ident()
                             << " period "
                             << (*it).period()
                             << " pdot "
                             << (*it).pdot()
                             << " dm "
                             << (*it).dm() ;
    }
    float rebin_period[fldo::cuda::max_nrebin];
    //To calculate the max spin period, we use the configured _mfold_phases value.
    //Generally its default value corresponds to 128 because it represents the better compromise
    //between performance and period time.
    //In particular for the max period evaluation we use _mfold_phases - 1 (or _mfold_phases -2) because
    //the folding kernel uses for no-split case (a) e split case (b):
    //(a) the last one  phase (128 if _mfold_phases =1 128) to store the phase 0
    //(b) the last two phases (127 and 128 if _mfold_phases = 128) to store the phases 1 and 0
    //In this way the gpu kernel does not use the modulo operator (see FoldInputDataKernel.cu)

    int max_num_of_phases = _mfold_phases - 2;
    // We take into account the nosplit option. In this
    // case we have (_mfold_phases - 1) !!
    if (_enable_split == false) {
        max_num_of_phases = _mfold_phases - 1;
    }
    float max_period = max_num_of_phases * tsamp.value() * fldo::cuda::max_prebin * 0.5;

    // implement the rebinning limit on period depending on the
    // sampling time
    for (int index = 0; index < (int) fldo::cuda::max_nrebin - 1; index ++) {
        rebin_period[index] = max_num_of_phases * (1 << index) * tsamp.value() * 0.5;
        PANDA_LOG_DEBUG <<"period[ " << index << "]: " << rebin_period[index];
    }
    rebin_period[fldo::cuda::max_nrebin - 1] = max_period;
    int cand_idx = 0;                   // the index inside the sorted list of candidates
    // get the pointer to the candidate list beginning
    data::VectorLike<std::vector<data::Scl::CandidateType>>::ConstIterator it = scl_data.begin() ;

    // initialize the rebin array
    //loop on rebin values to identify the candidates whose period falls
    //into current rebin time interval
    //A rebin entry is valid if the element first and last are != -1
    for (int rebin_idx = 0; rebin_idx < (int) fldo::cuda::max_nrebin; rebin_idx ++) {
        //initialize the rebin value for each element of the vector
        rebin[rebin_idx].rebin = 1 << rebin_idx;
        //loop on candidates list to divide them into the different groups
        //based on the rebin value. Each rebin group is identified by a
        //maximum period. All the candidates with period less than this
        //value, belong to the same rebin group.
        while (it != scl_data.end()) {
            //convert the candidate period in sec.
            CandidateType::TimeType const cand_period ((*it).period());
            // get the numerical candidate period in sec.
            // OSS: we don't rely on CandidateType::TimeType unit. If it changes, the next line
            // always give us the value of period in sec.
            float const cand_period_value = boost::units::quantity_cast<float>(static_cast<boost::units::quantity<boost::units::si::time>>(cand_period));
            if (cand_period_value >= rebin_period[rebin_idx]) {
                break;
            }
            if (rebin[rebin_idx].first > -1) {
                rebin[rebin_idx].last ++;
            } else {
                rebin[rebin_idx].rebin = 1 << rebin_idx;
                rebin[rebin_idx].first = cand_idx;
                rebin[rebin_idx].last = cand_idx;
            }
            //point to the next candidate
            ++it;
            //increment the candidate counter
            ++cand_idx;
        }
    }
    //only for check purpose.
    for (int index = 0; index < (int) fldo::cuda::max_nrebin; index ++) {
        PANDA_LOG_DEBUG << "rebin:  " << rebin[index].rebin
                        << " first: " << rebin[index].first
                        << " last: "  << rebin[index].last;
    }
}

/**
 * void Fldo::load_constant_devmem(std::vector<util::CandidateRebin> &rebin, std::vector<float> chan_freqs,
 *                                 data::Scl &scl_data)
 *
 * @brief
 * Allocates the device constant memory to store the fixed values use to
 * calculate the dispersion correction.
 *
 * @param rebin         vector with the CandidateRebin structures
 * @param delta_freq    vector with the tabulated values k_DM *(1/v1^2 - 1/v2^2)
 * @param scl_data      list of pulsar candidates
 * @param cand_phases   vector with the number of phases for each candidate
 *
 * @return on failure throw a runtime_error exception
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::load_constant_devmem(std::vector<util::CandidateRebin> &rebin, std::vector<double> delta_freq,
                                    data::Scl const &scl_data, std::vector <int> &cand_phases)
{
    //get the number of pulsar candidates
    size_t const ncandidates = scl_data.size();
    std::vector <float>  dm(ncandidates);              // vector with candidates dispersion measure
    std::vector <double> period(ncandidates);          // vector with candidates period
    std::vector <double> pdot(ncandidates);            // vector with candidates period derivative
    std::vector <double> nu(ncandidates);              // vector with candidates frequency
    std::vector <double> nudot(ncandidates);           // vector with the candidates frequency variation

    try {
        size_t const nchan_per_subband = _nchannels/_nsubbands;
        // we follow Chris advice
        int index = 0;          // index in candidate proprerties vector
        //load candidates values inside the corresponding vectors
        for (data::VectorLike<std::vector<data::Scl::CandidateType>>::ConstIterator it = scl_data.begin();
             it != scl_data.end(); ++it) {
            // get the candidate period numerical value in sec.
            // OSS: we don't rely on candidate period unit. If it changes, the next line
            // always give us the value of period in sec.
            double const cand_period_value = boost::units::quantity_cast<double>(static_cast<boost::units::quantity<boost::units::si::time>>((*it).period()));
            double const cand_pdot = ((*it).pdot()).value();
            period[index] = cand_period_value;
            pdot[index] = cand_pdot;
            dm[index] = ((*it).dm()).value();
            nudot[index] = -cand_pdot/(cand_period_value * cand_period_value);
            nu[index] = 1./cand_period_value;
            ++index;    // we follow Chris advice
        }
        PANDA_LOG_DEBUG << "Arrays of candidates values completed";
        //update candidates values for those of them that have a number of
        //phases < 32. In this case the number of phase bins of the candidate is
        //proportionally increased to get a number of final phase bins > 32.
        for (int i = 0; i < (int) fldo::cuda::max_nrebin; ++i) {
            if ((rebin[i].rebin == 1 << i) && (rebin[i].first > -1)) {
                //initialize the phases array with the rebinned number of phases
                //for each candidate
                double period_val = 0.;
                for (int ncand = rebin[i].first; ncand <= rebin[i].last; ncand ++) {
                    //store the period into a temporary variable
                    period_val = period[ncand];
                    cand_phases[ncand] = 2 * ((int) (period[ncand] / (_tsamp * rebin[i].rebin)));
                    PANDA_LOG_DEBUG << "load_constant_devmem: cand_phases["
                                    << ncand
                                    << "]: "
                                    << cand_phases[ncand];
                    // only if phase split is not enabled, we set a max limit
                    // on the number of natural phases
                    if ((cand_phases[ncand] > (int)_mfold_phases - 1) && (_enable_split == false)) {
                        cand_phases[ncand] = (int)_mfold_phases - 1;
                    }
                    PANDA_LOG_DEBUG << " Candidate: "       << ncand
                                    << " original cand_phases: "  << cand_phases[ncand]
                                    << " period: "          << period[ncand]
                                    << " dm: "              << dm[ncand]
                                    << " pdot: "            << pdot[ncand]
                                    << " rebin: "           << rebin[i].rebin;
                    nudot[ncand] = -pdot[ncand]/(period_val * period_val);
                    nu[ncand] = 1./period_val;
                }
            }
        }
        util::load_constant_data(delta_freq.data(), nu.data(), nudot.data(), dm.data(),
                                 cand_phases.data(), _nchannels, nchan_per_subband, _nsubints, _tsamp, ncandidates);
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in load_constant_mem(): "
                        << e.what();
        throw e;
    }
    catch (...)
    {
        throw panda::Error("Catch unknown exception in load_constant_mem()");
        PANDA_LOG_ERROR << "Catch unknown exception in load_constant_mem()";
    }
}

/**
 * void FldoCuda<NumericalT>::allocate_global_devmem(size_t ncandidates, std::vector<util::CandidateRebin> &rebin,
 *                                       NumericalRep **d_in, float **d_outfprof, float ** d_outprof,
 *                                       float ** d_folded, float ** d_weight)
 * @brief
 * Allocates device global memory to store the folded data, the profiles in frequency and
 * time for each candidate.
 *
 * @param ncandidates   the number of candidates to optimize
 * @param rebin         a vector of CandidateRebin structures
 * @param h_in          host memory page-locked and accessible to the device
 * @param d_outfprof    device global memory with profiles scrunched in frequency
 * @param d_outprof     device global memory with profiles scrunched in frequency and time
 * @param d_folded      device global memory with folded profiles
 * @param d_weight      device global memory with phases weights
 *
 * @return on failure throws a runtime_error exception
 */

template<typename NumericalT>
void FldoCuda<NumericalT>::allocate_global_devmem(size_t ncandidates, std::vector<util::CandidateRebin> &rebin,
                                      NumericalRep **d_in, NumericalRep ** h_in, float **d_outfprof,
                                      float ** d_outprof, float ** d_folded, float ** d_weight)
{
    try
    {
        // evaluate the pitch_dim for each rebin value
        uint64_t nsamp_per_subint = _nsamp/_nsubints;       // number of samples in a sub-integration
        int nrow = nsamp_per_subint / _nchannels;           // number of samples/sub-integration/channel
        size_t  pitch_size = 0;                             // optimal GPU dimension for memory allocation

        //!!always allocate memory for rebin = 1 to store the input original data
        nrow = nsamp_per_subint / _nchannels;
        // call cudaMallocPitch() to meet the alignment requirements for coalescing as the
        // address is updated from row to row.
        CUDA_ERROR_CHECK(cudaMallocPitch((void **)&(rebin[0].d_out), &pitch_size, nrow * sizeof(float), _nchannels));
        // calculate the total dimension in bytes of the input data matrix
        rebin[0].msize = pitch_size * _nchannels;
        CUDA_ERROR_CHECK(cudaMemset((void*)rebin[0].d_out, 0, rebin[0].msize));
        // get the number of row of the matrix
        rebin[0].pitch_dim = pitch_size/sizeof(float);

        //loop on each valid rebinning value to allocate GPU memory for each rebinned matrix of input data
        //OSS: skip the first because already allocated!!
        for (std::vector<util::CandidateRebin>::iterator it = rebin.begin() + 1; it != rebin.end(); ++it) {
            if ((*it).first > -1) {
                nrow = nsamp_per_subint / (_nchannels * (*it).rebin);
                CUDA_ERROR_CHECK(cudaMallocPitch((void **)&(*it).d_out, &pitch_size, nrow * sizeof(float), _nchannels));
                // calculate the total dimension in bytes of the rebinned matrix
                (*it).msize = pitch_size * _nchannels;
                CUDA_ERROR_CHECK(cudaMemset((void*)(*it).d_out, 0, (*it).msize));
                // get the number of row of the matrix
                (*it).pitch_dim = pitch_size/sizeof(float);
                PANDA_LOG_DEBUG << "Allocated " << (*it).msize
                                << "  bytes for input rebinned data. "
                                << "rebin value: "
                                << (*it).rebin
                                << " original row size: "
                                << nrow
                                << " final row size: "
                                << (*it).pitch_dim;
            }
        }

        // map the host memory into the CUDA address space
        uint64_t mem_size = 0;
        //get the dimension in bytes of the host memory to allocate
        mem_size = rebin[0].pitch_dim * _nchannels * sizeof(char);
        PANDA_LOG_DEBUG << "host memory size to map: "
                        <<  mem_size
                        << " (nrow: "
                        << rebin[0].pitch_dim
                        << " x ncols: "
                        <<  _nchannels
                        << " x "
                        << sizeof(char)
                        << " bytes)";

        // set flag to allocate pinned host memory that is accessible to the device.
        //NB: to call cudaHostAlloc() with cudaHostAllocMapped, the flag cudaDeviceMapHost
        //has to be set (this is done in Fldo()).
        CUDA_ERROR_CHECK(cudaHostAlloc((void **) h_in, mem_size, cudaHostAllocMapped));
        // the device pointer to the memory may be obtained by calling cudaHostGetDevicePointer()
        CUDA_ERROR_CHECK(cudaHostGetDevicePointer((void **)d_in, (void *)*h_in, 0));
        PANDA_LOG_DEBUG << "host memory map success";

        // allocate global device memory to store profiles of candidates
        //
        // OSS: we use max number of phases (or bins) because the number of
        // phases is different for each candidate. This value is calculated using the candidate
        // period and the sampling time.  We then allocate the max memory even if it's not
        // used.
        size_t profile_len = _mfold_phases * _nsubints * _nsubbands * sizeof(float);
        mem_size = (uint64_t)profile_len * ncandidates;

        //CUDA_ERROR_CHECK(cudaMalloc((void **)&d_folded, mem_size ));
        CUDA_ERROR_CHECK(cudaMalloc((void **)d_folded, mem_size ));
        CUDA_ERROR_CHECK(cudaMemset((void*)*d_folded, 0, mem_size));
        PANDA_LOG_DEBUG << "allocate_global_devmem: allocated "
                        << mem_size
                        << " bytes to store sub-integration folded data";
        CUDA_ERROR_CHECK(cudaMalloc((void **)d_weight, mem_size ));
        CUDA_ERROR_CHECK(cudaMemset((void*)*d_weight, 0, mem_size));
        PANDA_LOG_DEBUG <<"allocate_global_devmem: allocated device memory for weights!";

        //dimension in bytes of the device memory to the profiles
        //scrunched in freq (d_outfprof) for all candidates
        mem_size = ncandidates * _nsubints * _mfold_phases * sizeof(float);
        //allocate the device memory an reset its value
        CUDA_ERROR_CHECK(cudaMalloc((void **)d_outfprof, mem_size ));
        CUDA_ERROR_CHECK(cudaMemset((void*)*d_outfprof, 0, mem_size));

        //calculate dimension in bytes of the device memory to store the profiles
        //scrunched in freq and time (d_outprof) for all candidates
        mem_size = ncandidates * _mfold_phases * sizeof(float);
        //allocate the device memory an reset its value
        CUDA_ERROR_CHECK(cudaMalloc((void **)d_outprof, mem_size ));
        CUDA_ERROR_CHECK(cudaMemset((void*)*d_outprof, 0, mem_size));
        PANDA_LOG_DEBUG << "allocate_global_devmem: allocated " << mem_size
                        << " bytes to store out profiles data";
        PANDA_LOG_DEBUG << "global memory allocation success";

    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in allocate_global_devmem(): "
                        << e.what();
        throw e;
    }
    catch (...)
    {
        throw panda::Error("Caught unknown exception in allocate_global_devmem()");
        PANDA_LOG_ERROR << "Caught unknown exception in allocate_global_devmem()";
    }
}

/**
 *
 * void FldoCuda<NumericalT>::free_dev_memory(NumericalRep *h_in, float *d_folded, float *d_weight, float *d_outprof, float * d_outfprof)
 *
 * @brief Frees the allocated device memory.
 *
 * @param h_in          host memory page-locked and accessible to the device
 * @param d_folded      device global memory with folded profiles
 * @param d_weight      device global memory with phases weights
 * @param d_outfprof    device global memory with profiles scrunched in frequency
 * @param d_outprof     device global memory with profiles scrunched in frequency and time
 *
 * @return on failure throws a runtime_error exception
 *
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::free_dev_memory(NumericalRep *h_in, float *d_folded, float *d_weight, float *d_outprof, float * d_outfprof)
{
    if (h_in) {
        CUDA_ERROR_CHECK(cudaFreeHost(h_in));
    }
    if (d_folded) {
        CUDA_ERROR_CHECK(cudaFree(d_folded));
    }
    if (d_weight) {
        CUDA_ERROR_CHECK(cudaFree(d_weight));
    }
    if (d_outprof) {
        CUDA_ERROR_CHECK(cudaFree(d_outprof));
    }
    if (d_outfprof) {
        CUDA_ERROR_CHECK(cudaFree(d_outfprof));
    }
}

/**
 * void FldoCuda<NumericalT>::folding(ResourceType& gpu, std::vector<util::CandidateRebin> &rebin, NumericalRep *d_in,
 *                        float *d_folded, float *d_weight, int *cand_phases, int isubint,
 *                        std::vector<util::GpuSteam>& exec_stream)
 *
 * @brief Calls the kernels for data transposition, rebinning and folding.
 *
 * @param d_in          the device pointer to mapped memory with original data
 * @param d_folded      the device pointer to memory with folded data
 * @param d_weight      the device pointer to memory with phases weights
 * @param cand_phases         the array with the phases bins of the candidates
 * @param isubint       the sub-integration number
 * @param total_gpu_time        the update total time on gpu
 *
 * @return On failure throws a runtime_error exception.
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::folding(ResourceType& gpu, std::vector<util::CandidateRebin> &rebin, NumericalRep *d_in,
                       float *d_folded, float *d_weight, int *cand_phases, int isubint, float* data_mean,
                       float *data_sigma, float& total_gpu_time, std::vector<util::GpuStream>& exec_stream)
{
    float gpu_time = 0.0f;      // gpu execution time
    int first_bin_idx = 0;      // index of the first valid rebinning element
    try
    {
        // create cuda timer events
        util::GpuEvent gpu_timer = util::GpuEvent();
        //TODO: control if we really  need device synchronization!
        CUDA_ERROR_CHECK(cudaDeviceSynchronize());
        // start the kernel timer
        gpu_timer.gpu_event_start();
        first_bin_idx = util::first_valid_binidx(rebin);
        //number of time sample/sub-integration
        uint64_t nsamp_subslot = _nsamp/(_nsubints * _nchannels);
        PANDA_LOG_DEBUG << "folding " << nsamp_subslot << " time samples"
                        << " in isubint " << isubint;
        //corner turn the inout data to have time samples on fast-asix
        util::corner_turner_and_rebin(gpu.device_properties(), first_bin_idx, nsamp_subslot, _nchannels, rebin, d_in);
        // calculate the data statistics
        {
            float mean0 = 0;            //mean of sub-integration data
            float sigma0= 0;            //sigma of sub-integration data

            //allocate a separate kernel stream to execute statistics
            util::GpuStream sstream = util::GpuStream();

            //execute statistics when the corner turner of the input matrix has ended. Check for
            //errors.
            CUDA_ERROR_CHECK(cudaStreamWaitEvent(sstream.stream(), rebin[first_bin_idx].event, 0));

            //OSS: sigma0 and mean0 are the statistics of the matrix padded with 0,
            //so these values are to be fixed
            util::statistics_float(rebin[first_bin_idx].d_out, _nchannels * rebin[first_bin_idx].pitch_dim,
                                   &mean0, &sigma0, sstream.stream());
            PANDA_LOG_DEBUG << "mean0: "
                            << mean0
                            << " sigma0: "
                            << sigma0;

            // The allocated d_out array is memory aligned and it's padded with 0. We need to rescale
            // the mean value to take into account the difference between the real
            // number of data per subslot (nsamp_subslot/prebin) and the padded one (pitch_dim)
            float meanv = mean0 *  rebin[first_bin_idx].pitch_dim * rebin[first_bin_idx].rebin/nsamp_subslot;
            *data_mean  += meanv;

            // true dimension of the data for statistix:  nsamp_subslot/rebin[first_bin_idx].rebin;
            // dimension used to calculate statistix: rebin[first_bin_idx].pitch_dim;
            float frac = ((float)rebin[first_bin_idx].pitch_dim*rebin[first_bin_idx].rebin)/nsamp_subslot;

            // calculate the variance of the true data
            float variance_r = frac * sigma0 * sigma0 - mean0*mean0 *(frac -1) -(meanv - mean0)*(meanv-mean0);
            //PANDA_LOG       << "mean0: "
            PANDA_LOG_DEBUG << "mean0: "
                            << mean0
                            << " sigma0: "
                            << sigma0
                            << " meanv: "
                            << meanv
                            << " variance_r: "
                            << variance_r;
            *data_sigma += variance_r;
            // stop the kernel timer
            // !! here all stream are synchronized!!
            gpu_timer.gpu_event_stop();
            gpu_time = gpu_timer.gpu_elapsed_time();
            total_gpu_time += gpu_time;
            PANDA_LOG_DEBUG << "GPU matrix statistics takes "
                            << gpu_time
                            << " msec  --  "
                            << "mean0: "
                            << mean0
                            << " sigma0: "
                            << sigma0;
        }

        //go ahead for folding...
        int valid_nbinning = first_bin_idx;
        PANDA_LOG_DEBUG << " valid_nbinning: "
                        << valid_nbinning;
        //loop on all valid binning values
        // OSS: first we load all the rebin kernels and then folding one
        for (int nbinning = valid_nbinning; nbinning < (int) fldo::cuda::max_nrebin ; nbinning ++) {
            if (rebin[nbinning].first == -1) {
                // check if some higher binning values has no valid entry (i.e. candidates available)
                continue;
            }
            // for all the rebinning values but the first, we need to rebin the input matrix
            if (nbinning > valid_nbinning) {
                CUDA_ERROR_CHECK(cudaStreamWaitEvent(rebin[nbinning].stream, rebin[valid_nbinning].event, 0));
                util::rebin_input_data(valid_nbinning, nbinning, _nchannels, rebin);
                valid_nbinning = nbinning;
            }
        }
        valid_nbinning = first_bin_idx;
        for (int nbinning = valid_nbinning; nbinning < (int) fldo::cuda::max_nrebin ; nbinning ++) {
            if (rebin[nbinning].first == -1) {
            // check if some higher binning  values has no valid entry  (i.e. candidates available)
                continue;
            }
            //executes folding for each candidate belonging to the same rebin value
            for (int ncand = rebin[nbinning].first; ncand <= rebin[nbinning].last; ncand ++) {
                // OSS: the kernels for each candidate are assigned to different streams
                int nstream = ncand % exec_stream.size();
                // for all the rebinning we need to wait the end of input  data binning
                CUDA_ERROR_CHECK(cudaStreamWaitEvent(exec_stream[nstream].stream(), rebin[nbinning].event, 0));
                //call kernel to do folding
                PANDA_LOG_DEBUG << "folding kernel for candidate "
                                << ncand
                                << " sub-integration: "
                                << isubint
                                << " nstream: "
                                << nstream
                                << " rebin: "
                                << rebin[nbinning].rebin
                                << " nphases: "
                                << cand_phases[ncand];

                // get the number of samps in each sub-integration
                uint64_t nsamp_subslot = _nsamp/(_nsubints * _nchannels * rebin[nbinning].rebin);
                util::fold_input_data(gpu.device_properties(), d_folded, d_weight, cand_phases,
                                      rebin[nbinning], ncand, isubint, _nchannels, _nsubbands, nsamp_subslot,
                                      _mfold_phases, _tobs, _enable_split, exec_stream[nstream].stream());
            }
        }
        //check for kernel errors
        CUDA_ERROR_CHECK(cudaGetLastError())
        gpu_timer.gpu_event_stop();
        gpu_time = gpu_timer.gpu_elapsed_time();
        total_gpu_time += gpu_time;
        //PANDA_LOG       << "folding_worker takes "
        PANDA_LOG_DEBUG << "folding_worker takes "
                        << gpu_time
                        << " msec";
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in folding(): " << e.what();
        throw e;
    }
    catch (...)
    {
        PANDA_LOG_ERROR << "Caught an unknown exception in folding()";
        throw panda::Error("Caught an unknown exception in folding()");
    }
}

/**
 * void FldoCuda<NumericalT>::profiles(size_t ncandidates, float mean, float *d_folded, float *d_weight, float *d_outfprof,
 *                         float* d_outprof, std::vector<util::GpuStream> &exec_stream)
 *
 * @brief Executes normalization and builds the reduced profile.
 *
 * @param ncandidates           number of input candidates
 * @param mean                  input data statistics
 * @param d_folded              pointer to dev memory with folded data
 * @param d_weight              pointer to dev memory with phases weights
 * @param d_outfprof            pointer to dev memory with profiles summed up in freq and weighted
 * @param d_outprof             pointer to dev memory with reduced profile
 * @param total_gpu_time        the update total time on gpu
 * @param exec_stream           the vector with the gpu stream to run kernels concurrently
 *
 * @return On failure throws a runtime_error exception.
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::profiles(size_t ncandidates,float mean, float *d_folded, float *d_weight, float *d_outfprof,
                        float* d_outprof, float& total_gpu_time, std::vector<util::GpuStream> &exec_stream)
{
    try
    {
        //allocate timer enabled events to get the GPU execution time
        util::GpuEvent timer = util::GpuEvent();

        float gpu_time = 0.0f;      //GPU time execution

        timer.gpu_event_start();
        util::build_scrunched_profiles(ncandidates, _mfold_phases, _nsubbands, _nsubints, mean, d_folded,
                                       d_weight, d_outfprof, d_outprof, exec_stream);
        timer.gpu_event_stop();
        //get elapsed gup time for normalization + profile
        gpu_time = timer.gpu_elapsed_time();
        PANDA_LOG_DEBUG << "normalization + profile takes " << gpu_time << " ms";
        //update the total gpu execution time
        total_gpu_time += gpu_time;
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in profiles(): " << e.what();
        throw e;
    }
    catch (...)
    {
        PANDA_LOG_ERROR << "Caught an unknown exception in profiles()";
        throw panda::Error("Caught an unknown exception in profiles()");
    }
}

/**
 * void FldoCuda<NumericalT>::run_folding(ResourceType& gpu, std::vector<std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>>> const& data,
 *                                                                 data::Scl input_candidates)
 * @brief
 * Executes the folding of the input data, builds the integrated profile
 * and optimizes the candidates.
 *
 * @param gpu                   the device resource allocated
 * @param data                  the vector with input data
 * @param input_candidates      the list with input candidates
 *
 * @return On failure throws a runtime_error exception.
 */
template<typename NumericalT>
std::shared_ptr<data::Ocld> FldoCuda<NumericalT>::run_folding(ResourceType& gpu,
                                                  std::vector<std::shared_ptr<TimeFrequencyType>> const& data,
                                                  data::Scl input_candidates)
{

    std::shared_ptr<data::Ocld> output = data::Ocld::make_shared();
    // get the sampling data rate from data
    TimeType const tsamp(data[0]->sample_interval());
    // store the numerical value of sampling time in seconds.
    // OSS: we don't rely on TimeType unit. If it changes, the next line always give us the value of
    // sampling time in sec.
    _tsamp = boost::units::quantity_cast<double>(static_cast<boost::units::quantity<boost::units::si::time>>(tsamp));
    _tobs = (_nsamp/_nchannels) * _tsamp;
    //get the number of candidates
    size_t const ncandidates = input_candidates.size();

    //declare the vectors used to store data into device constatnt memory
    std::vector <double> freq_sum(_nsubbands);          // vector with the sub-band mean frequency
    std::vector <double> time_sum(_nsubints);           // vector with the sub-integration mean time
    std::vector <double> delta_freq(_nchannels);        // tabulated k_dm * 1/(v0^2 - v^2) values for all channels
    std::vector <int> cand_phases(ncandidates);         // vector with the number of phases of each candidate

    // get the array with all the frequencies
    // NB: here we suppose that all elements of the data vector contain the
    // same number of channels and channel frequencies.
    // No assumption on frequency offset!
    // We take into account only the first one array of frequencies.
    std::vector<FrequencyType> chan_freqs = data[0]->channel_frequencies();
    double freq_chan_1 = chan_freqs[0].value() * chan_freqs[0].value();
    for (size_t ich = 0; ich < chan_freqs.size() ; ich ++) {
        double freq_chan_2 = chan_freqs[ich].value() * chan_freqs[ich].value();
        delta_freq[ich] =  (fldo::k_dm * (1./freq_chan_1 - 1./freq_chan_2));
    }

    size_t nchan_per_subband = _nchannels/_nsubbands;
    for (size_t isband = 0; isband < _nsubbands; isband ++) {
        for (size_t ich = 0; ich < nchan_per_subband; ich++) {
            size_t chan_id = ich + nchan_per_subband * isband;
            freq_sum[isband] += chan_freqs[chan_id].value();
        }
        freq_sum[isband]/= nchan_per_subband;
    }
    const size_t ts_sample = _nsamp / _nsubints / _nchannels;   //time samples per sub-integration
    for (size_t isubint = 0; isubint < _nsubints; isubint ++) {
        time_sum[isubint] = (-0.5 * _tobs + _tsamp * isubint  * ts_sample +_tsamp * (ts_sample - 1) * 0.5);
    }
    NumericalRep *h_in = NULL;         // host memory page-locked and accessible to the device
    NumericalRep *d_in = NULL;         // device pointer to mapped memory
    float *d_folded = NULL;             // pointer to device memory with folded data
    float *d_weight = NULL;             // pointer to device memory with phases weights
    float *d_outfprof = NULL;           // pointer to device memory with profiles scrunched in freq
    float *d_outprof = NULL;            // pointer to device memory with profiles scrunched in freq and time
    float mean = 0.;                    // mean of the input data
    float sigma = 0.;                   // sigma of the input data
    float total_gpu_time = 0.;          // total gpu time execution
    try {
        std::vector <util::CandidateRebin> rebin(fldo::cuda::max_nrebin);
        //build the vector of CandidateRebin structure. Each structure
        //contains info about candidates with the same re-binning factor.
        rebin_candidates(rebin, tsamp, input_candidates);
        //set the device id before allocating device memory
        cudaSetDevice(gpu.device_id());
        //load the constant memory
        load_constant_devmem(rebin, delta_freq, input_candidates, cand_phases);
        //allocate device global memory to store input and output data
        allocate_global_devmem(ncandidates, rebin, &d_in, &h_in, &d_outfprof, &d_outprof, &d_folded, &d_weight);
        //create 16 streams to handle kernel concurrency during candidate
        //processing.
        //TODO: check the number of max stream
        //std::vector<util::GpuStream> exec_stream(ncandidates);
        std::vector<util::GpuStream> exec_stream(32);

        // calculate the number of time samples per sub-integration
        int nsamp_per_subint = _nsamp/_nsubints;
        PANDA_LOG_DEBUG << "numer of samples/subint: " << nsamp_per_subint;
        // execute the folding on data belonging to the same sub-integration
        // TODO: overlap data transfer and kernel execution
        for (int isubint = 0 ; isubint < (int) _nsubints ; isubint ++) {
            PANDA_LOG_DEBUG << "Folding sub-integration " << isubint;
            //copy input data sub-integration in GPU memory
            std::copy(data[isubint]->begin(),  data[isubint]->end() , h_in);
            folding(gpu, rebin, d_in, d_folded, d_weight, cand_phases.data(), isubint, &mean,
                    &sigma, total_gpu_time, exec_stream);
        }

#ifdef NOT_USED
//!! ONLY FOR DEBUG TO CHECK OUTPUT!
        size_t memsize = _mfold_phases * _nsubints * _nsubbands * ncandidates;

        float *h_folded = nullptr;
        CUDA_ERROR_CHECK(cudaHostAlloc((void **) &h_folded, memsize*sizeof(float), cudaHostAllocDefault));
        memset(h_folded, 0, memsize * sizeof(float));
        // copy the folded gpu data to RAM
        CUDA_ERROR_CHECK(cudaMemcpy(h_folded, d_folded, memsize * sizeof(float), cudaMemcpyDeviceToHost));
        for (size_t ncand = 0; ncand < ncandidates; ncand ++) {
            std::fstream file_stream;
            std::stringstream ss;
            ss << "folded_" << ncand << ".out";
            //file_stream.open(panda::test::test_file(ss.str()), std::ios::out);
            file_stream.open(ss.str(), std::ios::out);
            for (int isubint = 0; isubint < (int)_nsubints; isubint ++) {
                file_stream << "nsubint:  " << isubint << std::endl;
                for (int isub = 0; isub < (int)_nsubbands; isub ++) {
                    file_stream << "nsubband:  " << isub << std::endl;
                    for (int nbin = 0; nbin < (int)_mfold_phases; nbin ++) {
                        if (nbin < cand_phases[ncand]) {
                            file_stream << nbin <<"  " << h_folded[nbin + isub * _mfold_phases +
                                                           isubint * _nsubbands * _mfold_phases +
                                     ncand * _nsubints * _nsubbands *_mfold_phases] << std::endl;
                        }
                    }
                }
            }
        }
#endif


        // build the reduced profile
        profiles(ncandidates, mean, d_folded, d_weight, d_outfprof, d_outprof, total_gpu_time, exec_stream);
        PANDA_LOG       << "Total GPU time of execution (ms): " << total_gpu_time;

#ifdef NOT_USED
//!! ONLY FOR DEBUG TO CHECK OUTPUT!
        // allocate host memory to copy profile (only for test analysis)
        memsize = _mfold_phases * ncandidates;
        float *h_outprof = nullptr;
        CUDA_ERROR_CHECK(cudaHostAlloc((void **) &h_outprof, memsize*sizeof(float), cudaHostAllocDefault));
        memset(h_outprof, 0, memsize * sizeof(float));
        CUDA_ERROR_CHECK(cudaMemcpy(h_outprof, d_outprof, memsize * sizeof(float), cudaMemcpyDeviceToHost));
        for (size_t ncand = 0; ncand < ncandidates; ncand ++) {
            std::fstream file_stream;
            std::stringstream ss;
            ss << "reduced_profile_" << ncand << ".out";
            //file_stream.open(panda::test::test_file(ss.str()), std::ios::out);
            file_stream.open(ss.str(), std::ios::out);
            for (int nbin = 0; nbin < (int)_mfold_phases; nbin ++) {
                if (nbin < cand_phases[ncand]) {
                    file_stream << nbin <<"  " << h_outprof[nbin + ncand * _mfold_phases] << std::endl;
                }
            }
            file_stream.close();
        }
#endif
        // optimization algoritm: it shall return the output list of candidates
        // copy original order candidates
        data::Scl opt_candidates = input_candidates;
        run_optimization(input_candidates, opt_candidates, d_folded, d_weight, d_outfprof,
                         rebin, time_sum, freq_sum, total_gpu_time, exec_stream, 10, 1, 1, 1, 10, 1, sigma, 0);
        run_optimization(input_candidates, opt_candidates, d_folded, d_weight, d_outfprof,
                         rebin, time_sum, freq_sum, total_gpu_time, exec_stream, 1, 1, 10, 1, 10, 3, sigma, 0);
        run_optimization(input_candidates, opt_candidates, d_folded, d_weight, d_outfprof,
                         rebin, time_sum, freq_sum, total_gpu_time, exec_stream, 10, 3, 10, 3, 1, 1, sigma, 1);
        // free the allocate device memory
        free_dev_memory(h_in, d_folded, d_weight, d_outprof, d_outfprof);
    }
    catch (std::runtime_error &e)
    {
        free_dev_memory(h_in, d_folded, d_weight, d_outprof, d_outfprof);
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in run_folding(): "
                        << e.what();
        throw e;
    }
    catch(...)
    {
        free_dev_memory(h_in, d_folded, d_weight, d_outprof, d_outfprof);
        PANDA_LOG_ERROR << "Caught an unknown exception in run_folding()";
        throw panda::Error("Caught an unknown exception in run_folding()");
    }
    return output;
}

/**
 *
 * void FldoCuda<NumericalT>::run_optimization(data::Scl const& ori_data, data::Scl & opt_data, float *d_folded,
 *                    float* d_perturbed, float *d_outfprof, std::vector<util::CandidateRebin> &rebin,
 *                    std::vector<double> const &time_sum, std::vector <double> const & freq_sum,
 *                    float& total_gpu_time,
 *                    std::vector<util::GpuStream>& exec_stream, int p_trial, int p_rf,
 *                    int pdot_trial, int pdot_rf, int dm_trial, int dm_rf, float sigma, int verbose)
 *
 * @brief Build the profile of all candidates applying perturbations to the
 * pulsar period, dispersion and pdot.
 *
 * @param ori_data              the list with pulsar candidates optimized values
 * @param opt_data              the list with pulsar candidates optimized values
 * @param d_folded              device memory with folded data
 * @param d_perturbed
 * @param d_outfprof            device memory with profiles scrunched in freq
 * @param rebin                 vector with CandidateRebin structures
 * @param time_sum              vector with sub-integration central time
 * @param freq_sum              vector with sub-band central freq.
 * @param p_trial               number of for period optimization
 * @param p_rf                  reduce factor of the period step with
 * @param pdot_trial            number of steps in pdot optimization
 * @param pdot_rf               reduce factor of the pdot step width
 * @param dm_trial              number of steps in dm optimization
 * @param dm_rf                 reduce factor of the dm step width
 * @param sigma
 * @param verbose               flag of full output or not
 *
 * @return On failure throws a runtime_error exception.
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::run_optimization(data::Scl const& ori_data, data::Scl & opt_data, float *d_folded,
                      float* d_perturbed, float *d_outfprof, std::vector<util::CandidateRebin> &rebin,
                      std::vector<double> const & time_sum, std::vector <double> const & freq_sum,
                      float& total_gpu_time, std::vector<util::GpuStream>& exec_stream, int p_trial, int p_rf,
                      int pdot_trial, int pdot_rf, int dm_trial, int dm_rf, float sigma, int verbose)
{
    dim3 threadsPerBlock;               // number of threads for each block
    dim3 blocksPerGrid;                 // number of blocks for each grid;
    util::GpuEvent gpu_timer = util::GpuEvent(); // create cuda timer events

    int nperiod = 0, npdot = 0, ndm = 0;
    nperiod = p_trial * 2 - 1;          //number of period trials
    npdot   = pdot_trial * 2 - 1;       //number of pdot trials
    ndm     = dm_trial * 2 - 1;         //number of dm trials
    // calculate number of trials for period, pdot and dm
    int trials_num = nperiod * npdot * ndm;  //total numer of trials for each candidate
    //allocate a vector to store the phase shift for each candidate and trial
    size_t const ncandidates = ori_data.size();
    size_t ph_size = ncandidates * _nsubbands * _nsubints;
    std::vector<float>phase_shift(ph_size);
    try {
        // start the kernel timer
        gpu_timer.gpu_event_start();
        //allocate memory to store results
        size_t opt_prof_size =  trials_num * ncandidates * _mfold_phases;
        // pointer to device memory with optimized profiles scrunched in freq
        panda::nvidia::CudaDevicePointer<float>d_opt_profile(opt_prof_size);
        CUDA_ERROR_CHECK(cudaMemset(d_opt_profile(), 0, opt_prof_size * sizeof(float)));
        CUDA_ERROR_CHECK(cudaDeviceSynchronize());
        std::vector<float>trial_param(1);
        util::perturb_candidates(ori_data, opt_data, d_folded, d_perturbed, d_outfprof,
                                 d_opt_profile(), exec_stream, time_sum,
                                 freq_sum, trial_param, _nchannels, _nsubints, _nsubbands, _mfold_phases,
                                 _tobs, p_trial, p_rf, pdot_trial, pdot_rf, dm_trial, dm_rf);
        double _sigma = sqrt(sigma / _nsubints);                        //standard deviation of the population of raw time series
        int first_prebin_idx = first_valid_binidx(rebin);       // find the first valid prebin index
        int first_prebin = rebin[first_prebin_idx].rebin;       // find the first valide prebin value
        // Note: if the input data used to calculate the statistics have been
        // rebinned, we have to take into account the square root of the first
        // rebin value.
        // to get the correct sigma value, we need to scale the calculated
        // sigma with the first prebin value. The first prebin value defines the input
        // data array dimension on which the statistics is evaluated
        _sigma *= sqrt((float)first_prebin / _nsamp);
        // allocate memory to store results (sn_max and its index) for alla candidates
        std::vector<float> sn_max(ncandidates);
        std::vector<float> pulse_width(ncandidates);
        std::vector<int>index_max(ncandidates);
        // allocate memory to copy back the optimized values of 3-tuple (period, pdot, dm) for each candidate
        util::compute_max_sn(ncandidates, trials_num, _mfold_phases, _sigma, d_opt_profile(), sn_max, pulse_width, index_max,  exec_stream);
        gpu_timer.gpu_event_stop();
        //get elapsed gup time for optimization + normalization + profile
        total_gpu_time += gpu_timer.gpu_elapsed_time();
        PANDA_LOG_DEBUG << "perturbation + S/N takes " << gpu_timer.gpu_elapsed_time() << " ms";
        for (size_t ncand = 0; ncand < ncandidates; ncand ++) {
            //get the index of optimized tuple inside the trial_param vector
            int idx = index_max[ncand]  * 3 + ncand * trials_num * 3;
            data::Scl::CandidateType::MsecTimeType period_val(trial_param[idx] * boost::units::si::seconds);
            data::Scl::CandidateType::SecPerSecType pdot_val = trial_param[idx + 1];
            data::Scl::CandidateType::Dm dm_val = trial_param[idx + 2] * data::parsecs_per_cube_cm;
            opt_data[ncand].period(period_val);
            opt_data[ncand].pdot(pdot_val);
            opt_data[ncand].dm(dm_val);
            // we publish data only on last iteration
            if (verbose < 1) {
                PANDA_LOG_DEBUG << "Candidate: "<< ncand << " (" << opt_data[ncand].ident() << ")"
                                << " index: " << index_max[ncand]
                                << " S/N: " << sn_max[ncand]
                                << " width: " << pulse_width[ncand]
                                << " period: " << trial_param[idx]
                                << " pdot: " << trial_param[idx + 1]
                                << " dm: " << trial_param[idx + 2];
            } else {
                PANDA_LOG << "Candidate: "<< ncand << " (" << opt_data[ncand].ident() << ")"
                                << " index: " << index_max[ncand]
                                << " S/N: " << sn_max[ncand]
                                << " width: " << pulse_width[ncand]
                                << " period: " << trial_param[idx]
                                << " pdot: " << trial_param[idx + 1]
                                << " dm: " << trial_param[idx + 2];
            }
        }
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in run_optimization(): "
                        << e.what();
        throw e;
    }
    catch (...)
    {
        throw panda::Error("Catch unknown exception in run_optimization()");
        PANDA_LOG_ERROR << "Catch unknown exception in run_optimization()";
    }

}

/**
 * data_reshape
 * @brief
 * add control and re-formatting of the input data
 *
 * @details
 * Folding routine assumes input data are organized as a a vector of _nsubints
 * components, each containing (approx) the same number of measures
 * data_reshape accept a generic vector, and if correct, return
 * unchanged. if not the case, it reshape it as required.
 *
 */
template<typename NumericalT>
void FldoCuda<NumericalT>::data_reshape(std::vector<std::shared_ptr<TimeFrequencyType>> const& input_data,
                            std::vector<std::shared_ptr<TimeFrequencyType>> const& data)
{
    (void) input_data;
    (void) data;
    // TODO move here the real work
    //data = input_data;
}

template class FldoCuda<uint8_t>;
//template class FldoCuda<uint16_t>;

#endif //ENABLE_CUDA

} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
