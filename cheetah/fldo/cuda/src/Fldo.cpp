/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/Fldo.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace detail {

#ifdef ENABLE_CUDA

FldoBase::RunnerFactory::RunnerFactory(fldo::Config const& config)
    : _config(config)
{
}

void FldoBase::RunnerFactory::operator()(ResourceType const& gpu)
{
    cudaDeviceProp device_prop = gpu.device_properties();
    if (!device_prop.canMapHostMemory) {
        panda::Error e("Device ");
        e << gpu.device_id() << " cannot map host memory!";
        throw e;
    }
    unsigned int cudaFlags = 0;
    CUDA_ERROR_CHECK(cudaGetDeviceFlags(&cudaFlags));
    if ((cudaFlags & cudaDeviceMapHost) != cudaDeviceMapHost) {
        CUDA_ERROR_CHECK(cudaSetDeviceFlags(cudaDeviceMapHost));
    }
}

#endif //ENABLE_CUDA

} // namespace detail
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
