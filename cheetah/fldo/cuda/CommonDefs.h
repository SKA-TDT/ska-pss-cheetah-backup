/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @brief
 *   Some limits and constants for the FLDO cuda based algorithm.
 *
 * @details
 *   Limits cames from current CUDA implementation.
 *   A table with limits is in doc directory
 *   max_period comes from a pre-bin limitation we intruduced to simplify
 *   the code. To be removed.
 *
 *
 */

#include "cheetah/fldo/CommonDefs.h"


#ifndef SKA_CHEETAH_FLDO_CUDA_COMMONDEF_H
#define SKA_CHEETAH_FLDO_CUDA_COMMONDEF_H

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {


static constexpr size_t max_frequencies_low =  8192;   // MID and LOW have different max
static constexpr size_t max_frequencies_mid =  4096;   // number of channels
static constexpr size_t max_frequencies     =  max_frequencies_mid;// 8192 exeedes the inner memory size and gives
                                               // a run time error.
                                               // TODO switch to a different approach.
static constexpr size_t min_frequencies     =  64;
static constexpr size_t o_frequencies       =  max_frequencies;
static constexpr size_t max_seq_measures    =  187500;
static constexpr size_t o_seq_measures      =  max_seq_measures;
static constexpr size_t max_candidates      =  256;
static constexpr size_t o_candidates        =  128;

// intermediate data definitions
static constexpr size_t max_phase_recomp    =  1;           // recompute phases at each iteration
static constexpr size_t o_phase_recomp      =  max_phase_recomp;
static constexpr double o_tsampling         =  50.e-6;
static constexpr double max_timeobs         =  1800.;
static constexpr size_t max_nsamples        =  137438953472;// 2^24*max_frequencies_low
static constexpr size_t min_nsamples        =  16384;       // 2^8*min_frequencies
static constexpr size_t max_nrebin          =  10;
static constexpr size_t max_prebin          =  (1 << (max_nrebin - 1));
static constexpr size_t o_prebin            =  1;
static constexpr double max_period          =  0.049;


} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska


#endif // SKA_CHEETAH_FLDO_CUDA_COMMONDEF_H
