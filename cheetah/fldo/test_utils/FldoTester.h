/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_TEST_UTILS_FLDOTESTER_H
#define SKA_CHEETAH_FLDO_TEST_UTILS_FLDOTESTER_H

#include "cheetah/fldo/Fldo.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "panda/AlgorithmInfo.h"
#include "panda/test/TestHandler.h"
#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace fldo {
namespace test {

/**
 * @brief
 * Generic functional test for the Fldo algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requiremnst it needs to run.
 *
 *  e.g.
 * @code
 * struct MyAlgoTraits1 : public FldoTesterTraits<ResourceCapabilities, Architecture> {
 *      /// wrapper to execute the algorithm on requested provided device
 *      /// @return A dataype matching the tester requirements to verify the results
 *      ResultsType apply_algorithm(DeviceType&) const;
 * };
 * @endcode
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<MyAlgoTraits1, MyAlgoTraits2> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, FldoTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */

template<class FldoAlgo>
class FldoTesterTraits : public utils::test::PoolAlgorithmTesterTraits<typename panda::AlgorithmInfo<FldoAlgo>::Architecture
                                                                   ,typename panda::AlgorithmInfo<FldoAlgo>::ArchitectureCapability>
{
    private:
        typedef utils::test::PoolAlgorithmTesterTraits<typename FldoAlgo::Architecture
                                                     , typename panda::AlgorithmInfo<FldoAlgo>::ArchitectureCapability> BaseT;
    public:
        typedef typename BaseT::PoolType PoolType;
        typedef std::shared_ptr<data::Ocld> ResultType;

    private:
        struct FldoHandler : public panda::test::TestHandler
        {
                typedef panda::test::TestHandler BaseT;

            public:
                FldoHandler()
                    : BaseT(false)
                {}
                void operator()(ResultType data) { _data = data; BaseT::operator()(); }
                ResultType const& data() const { return _data; }

            private:
                ResultType _data;
        };

        struct TestConfig : public fldo::Config
        {
                typedef typename FldoTesterTraits::PoolType PoolType;

            public:
                TestConfig()
                    : _pool(nullptr)
                {
                    deactivate_all();
                }

                PoolType& pool() const { assert(_pool); return *_pool; }
                void pool(PoolType& pool) { _pool = &pool; }

            protected:
                PoolType* _pool;
        };

    public:
        typedef typename FldoAlgo::Traits::TimeFrequencyType TimeFrequencyType;
        typedef typename TimeFrequencyType::value_type NumericalRep;

        typedef fldo::Fldo<FldoHandler&, NumericalRep, TestConfig> Api;

    public:
        Api& api(PoolType&);
        FldoHandler& handler();

    private:
        TestConfig _config;
        FldoHandler _handler;
        std::unique_ptr<Api> _api;
};


template <typename TestTraits>
class FldoTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
        typedef cheetah::utils::test::AlgorithmTester<TestTraits> BaseT;

    protected:
        void SetUp();
        void TearDown();

    public:
        FldoTester();
        ~FldoTester();

    private:
};

TYPED_TEST_CASE_P(FldoTester);

} // namespace test
} // namespace fldo
} // namespace cheetah
} // namespace ska
#include "cheetah/fldo/test_utils/detail/FldoTester.cpp"


#endif // SKA_CHEETAH_FLDO_TEST_UTILS_FLDOTESTER_H
