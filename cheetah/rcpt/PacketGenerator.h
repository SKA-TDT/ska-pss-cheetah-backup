/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_RCPT_PACKETGENERATOR_H
#define SKA_CHEETAH_RCPT_PACKETGENERATOR_H
#include "cheetah/rcpt/BeamFormerPacket.h"
#include "cheetah/rcpt/BeamFormerDataTraits.h"
#include "panda/Buffer.h"
#include "panda/Engine.h"
#include <type_traits>
#include <vector>

namespace ska {
namespace cheetah {
namespace rcpt {

/**
 * @brief
 *     Packs data into a UDP stream Packet Header format of the BeamFormer
 *
 * @details
 * 
 */

template<class DataGenerator>
class PacketGenerator
{
    public:
        typedef BeamFormerDataTraits::DataType::NumericalRep SampleDataType;

    public:
        PacketGenerator(DataGenerator& model
                        ,data::DimensionSize<data::Frequency> number_of_channels   /// number of chnnnels for model to fill
                        ,data::DimensionSize<data::Time> number_of_time_samples = data::DimensionSize<data::Time>(100U) /// define the size of the blocks to ask to be filled from the generator
                       );
        ~PacketGenerator();

        /**
         * @brief send out a single packet
         * @param handler the handler to call when the packet has been sent
         */
        template<typename Handler>
        void send_one(Handler&&);

        ska::panda::Buffer<char> next();
        long interval() const;
        void abort();

    private:
        typedef BeamFormerPacketMid Packet;
        typedef decltype(std::declval<Packet>().packet_count()) CounterType;

        DataGenerator& _model;                // The data generation model
        typename BeamFormerDataTraits::DataType _data;
        typename BeamFormerDataTraits::DataType::ConstIterator _data_iterator;
        CounterType _counter;                 // (6-Byte) packet counter.
        long _interval;                       // The interval between packets in microsec.

        const std::size_t _max_buffers;
        std::size_t _buffer_index;
        std::vector<ska::panda::Buffer<char>> _buffers;
        ska::panda::Engine _engine;
};


} // namespace rcpt
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt/detail/PacketGenerator.cpp"

#endif // SKA_CHEETAH_RCPT_PACKETGENERATOR_H 
