/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/emulator/Config.h"


namespace ska {
namespace cheetah {
namespace sps {
namespace emulator {


Config::Config()
    : utils::Config("emulator")
    , _rate(1.0)
    , _active(false)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("candidate_rate", boost::program_options::value<float>(&_rate)->default_value(_rate) , "Number of candidates per second")
    ("active", boost::program_options::value<bool>(&_active)->default_value(_active) , "Enable emulation mode");
}

float Config::candidate_rate() const
{
    return _rate;
}

void Config::candidate_rate(float cands_per_second)
{
    _rate = cands_per_second;
}

bool Config::active() const
{
    return _active;
}

void Config::activate()
{
    _active = true;
}


} // namespace emulator
} // namespace sps
} // namespace cheetah
} // namespace ska
