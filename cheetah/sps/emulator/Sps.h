/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_EMULATOR_SPS_H
#define SKA_CHEETAH_SPS_EMULATOR_SPS_H

#include "cheetah/sps/emulator/Config.h"
#include "cheetah/sps/Config.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/ddtr/DedispersionTrialPlan.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace sps {
namespace emulator {

/**
 * @brief Produces a stream of random SpCandidate's
 * @details
 */

template<class SpsTraits>
class Sps
{
        typedef typename SpsTraits::value_type NumericalRep;

    public:
        typedef panda::Cpu Architecture;
        typedef typename SpsTraits::BufferType BufferType;
        typedef typename SpsTraits::TimeFrequencyType TimeFrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;

    private:
        typedef typename SpsTraits::DmTrialsType DmTrialsType;

    public:
        Sps(sps::Config const&);
        Sps(Sps const&) = delete;
        Sps(Sps&&);
        ~Sps();

        template<typename DmHandler, typename SpHandler>
        void operator()(panda::PoolResource<cheetah::Cpu>&, BufferType&, DmHandler&, SpHandler&);

        std::size_t set_dedispersion_strategy(std::size_t memory_limit, TimeFrequencyType const&);


        /**
         * @brief the number of time samples required to be copied from the end of the previous buffer
         *        in to the current one - returns 0 i.e no special restrictions
         */
        static constexpr std::size_t buffer_overlap() { return 0; }

    private:
        std::size_t _dedispersion_samples;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        sps::Config const& _config;
        std::size_t _max_delay;
        std::vector<double> _dm_factors;
};


} // namespace emulator
} // namespace sps
} // namespace cheetah
} // namespace ska

#include "detail/Sps.cpp"

#endif // SKA_CHEETAH_SPS_EMULATOR_SPS_H
