/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_DETAIL_SPSTASK_H
#define SKA_CHEETAH_SPS_DETAIL_SPSTASK_H

#include "cheetah/sps/emulator/Sps.h"
#include "cheetah/data/DmTime.h"
#include "panda/AlgorithmTuple.h"
#include "panda/arch/nvidia/Nvidia.h"
#include "panda/FinishTask.h"
#include <memory>

// astroacceelrate is dangerous - include last or suffer the consequences
#include "cheetah/sps/astroaccelerate/Sps.h"

namespace ska {
namespace cheetah {
namespace sps {

/**
 * @brief Single pulse search asynchronous task
 * @details
 *
 */
template<typename DmHandler, typename SpHandler, typename CommonTraits>
class SpsTask : public std::enable_shared_from_this<SpsTask<DmHandler, SpHandler, CommonTraits>>
{
    private:
        // to extend to different architectures add the alorithm inside this tuple
        typedef sps::emulator::Sps<CommonTraits> EmulatorType;
        typedef panda::AlgorithmTuple<astroaccelerate::Sps<CommonTraits>, EmulatorType> ImplementationsType;

    protected:
        typedef typename CommonTraits::BufferType BufferType;
        typedef typename CommonTraits::BufferFillerType BufferFillerType;
        typedef typename CommonTraits::TimeFrequencyType TimeFrequencyType;

    public:
        typedef typename ImplementationsType::Architectures Architectures;

    public:
        SpsTask(DmHandler dm_handler, SpHandler sm_handler);
        SpsTask(SpsTask const&) = delete;
        virtual ~SpsTask();

        virtual std::shared_ptr<panda::ResourceJob> submit(BufferType buffer);

        virtual void finish(BufferFillerType& buffer_filler);

        /**
         * @brief call the dm handler directly with the provided data
         */
        template<typename DataType>
        void call_dm_handler(DataType&& d) const;

        /**
         * @brief call the sp handler directly with the provided data
         */
        template<typename DataType>
        void call_sp_handler(DataType&& d) const;

        virtual std::size_t buffer_overlap() const;

        virtual std::size_t set_dedispersion_strategy(
                                std::size_t memory_min
                               ,TimeFrequencyType const& data
                            );

    protected:
        //ImplementationsType _algos;
        DmHandler _dm_handler;
        SpHandler _sp_handler;
};

template<typename DmHandler, typename SpHandler, typename CommonTraits
        ,typename PoolType, typename... Algos>
class SpecificSpsTask : public SpsTask<DmHandler, SpHandler, CommonTraits>
{
        typedef SpsTask<DmHandler, SpHandler, CommonTraits> BaseT;
        typedef panda::AlgorithmTuple<Algos...> ImplementationsType;
        typedef typename BaseT::BufferType BufferType;
        typedef typename BaseT::BufferFillerType BufferFillerType;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;

    public:
        typedef typename ImplementationsType::Architectures Architectures;

    public:
        SpecificSpsTask( DmHandler dm_handler
                       , SpHandler sm_handler
                       , PoolType& pool
                       , Algos&&... algos);
        SpecificSpsTask(SpecificSpsTask const&) = delete;

        virtual std::shared_ptr<panda::ResourceJob> submit(BufferType buffer) override;

        /**
         * @brief execute Sps task on a given accelerator
         */
        template<typename Arch>
        void operator()(panda::PoolResource<Arch>&, BufferType buffer);

        /**
         * @brief the number of samples to be copied from the end of one buffer in tot the beginning of the next
         * @details
         *    tprocessed is the number of time samples required to be able to process the specified dm ranges (via the config)
         */
        std::size_t buffer_overlap() const override;

        /**
         * @brief specify parameters to generate a suitable dedispersion compute strategy
         * @returns the minimum sample size required for the specified parameters
         */
        std::size_t set_dedispersion_strategy(
                                std::size_t memory_min
                               ,TimeFrequencyType const& data
                            ) override;


        /**
         * @brief block until all buffers are flushed and processed
         */
        void finish(BufferFillerType& buffer_filler) override;

        /**
         * @brief get the correct shred_from_this type
         */
        std::shared_ptr<SpecificSpsTask> shared_from_this()
        {
            return std::static_pointer_cast<SpecificSpsTask>(BaseT::shared_from_this());
        }

    private:
        ImplementationsType _algos;
        PoolType* _pool;
};

} // namespace sps
} // namespace cheetah
} // namespace ska

#include "cheetah/sps/detail/SpsTask.cpp"
#endif // SKA_CHEETAH_SPS_DETAIL_SPSTASK_H
