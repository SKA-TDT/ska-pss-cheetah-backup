/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/Sps.h"
#include "panda/ResourcePool.h"
#include "panda/TypeTraits.h"
#include "panda/Log.h"


namespace ska {
namespace cheetah {
namespace sps {


template<class ConfigType, typename NumericalRep>
Sps<ConfigType, NumericalRep>::Sps(ConfigType const& config, DmHandler dm_handler, SpHandler sp_handler)
    : _pool(config.pool())
    , _agg_buf_filler( [this](typename BufferFillerType::AggregationBufferType buffer)
                       {
                           _task_ptr->submit(std::move(buffer));
                       }
                       ,0, config.rfiexcision_config())
    , _current_number_of_channels(0)
    , _algo_count(0)
    , _config(config)
{
    if(config.astroaccelerate_config().active()) {
#ifdef ENABLE_ASTROACCELERATE
        _task_ptr.reset(new AlgoLauncher<astroaccelerate::Sps<CommonTraits>>( std::move(dm_handler)
                                                              , std::move(sp_handler)
                                                              , _pool
                                                              , std::move(astroaccelerate::Sps<CommonTraits>(config))));
        ++_algo_count;
#else //ENABLE_ASTROACCELERATE
       PANDA_LOG_WARN << "Sps: request for astroaccelearte, but cheetah has not been build with CUDA support. Please use cmake compile flag -DENABLE_CUDA=true";
#endif //ENABLE_ASTROACCELERATE

    }

    if(config.emulator_config().active()) {
        _task_ptr.reset(new AlgoLauncher<emulator::Sps<CommonTraits>>( std::move(dm_handler)
                                                                     , std::move(sp_handler)
                                                                     , _pool
                                                                     , std::move(emulator::Sps<CommonTraits>(config)))
                       );
        ++_algo_count;
    }
    if(_algo_count > 1 ) { // at the moment this is currently an error
        throw panda::Error("Multiple sps algorithms have been selected");
    }
    else if(_algo_count == 0)
    {
        _task_ptr.reset(new TaskType(std::move(dm_handler), std::move(sp_handler)));
    }
    _dedisp_samples = config.dedispersion_samples();
}


template<class ConfigType, typename NumericalRep>
Sps<ConfigType, NumericalRep>::~Sps()
{
    _task_ptr->finish(_agg_buf_filler);
    PANDA_LOG_DEBUG << "~Sps() destructor";
}

template<class ConfigType, typename NumericalRep>
void Sps<ConfigType, NumericalRep>::agg_buffer_init(TimeFrequencyType const& data)
{
    if (data.number_of_channels() != _current_number_of_channels)
    {
        std::size_t buffer_memory_max = panda::nvidia::min_gpu_memory(_pool); // TODO poll all device types in pool matching algo reqs
        _dedisp_samples = _task_ptr->set_dedispersion_strategy(buffer_memory_max, data);

        std::size_t overlap = _task_ptr->buffer_overlap(); // in numbers of spectra
        if(overlap == 0) {
            overlap = _config.maximum_delay_offset(data);
        }
        std::size_t min_memory_size = sizeof(NumericalRep) * (1 + overlap) * data.number_of_channels();
        if(min_memory_size >= buffer_memory_max && buffer_memory_max != 0) {
            PANDA_LOG_ERROR << "configuration error: dedispersion plan requires at least" << overlap + 1
                            << " samples (min mem required=" << min_memory_size
                            << " , mem available(suitable pool devices)=" << buffer_memory_max << ")";
            throw panda::Error("dedispersion sample buffer size is too small");
        }

        if(overlap >= _dedisp_samples) {
            PANDA_LOG_ERROR << "configuration error: samples requested < minimum required for dedispersion plan(" << overlap << ")";
            throw panda::Error("dedispersion sample buffer size is too small");
        }
        PANDA_LOG << "setting dedispersion buffer size to " << _dedisp_samples << " spectra";
        _agg_buf_filler.resize(data.number_of_channels() * _dedisp_samples);

        PANDA_LOG << "setting buffer overlap to " << overlap << " spectra";
        _agg_buf_filler.set_overlap( overlap * data.number_of_channels() );

        _current_number_of_channels = data.number_of_channels();
    }
}

template<class ConfigType, typename NumericalRep>
template<typename DataT>
void Sps<ConfigType, NumericalRep>::agg_buffer_fill(DataT& data)
{
    TimeFrequencyType& tf_data = static_cast<TimeFrequencyType&>(panda::is_pointer_wrapper<typename std::remove_reference<DataT>::type>::extract(data));
    if(_algo_count>0) {
        agg_buffer_init(tf_data);
        _agg_buf_filler << data;
    } else {
        (void) data;
        PANDA_LOG_WARN << "No available Sps algorithm";
    }
}

template<class ConfigType, typename NumericalRep>
void Sps<ConfigType, NumericalRep>::operator()(TimeFrequencyType& data)
{
    this->template agg_buffer_fill(data);
}

template<class ConfigType, typename NumericalRep>
void Sps<ConfigType, NumericalRep>::operator()(data::RfimFlaggedData<TimeFrequencyType>& data)
{
    this->template agg_buffer_fill(data);
}

template<class ConfigType, typename NumericalRep>
template<typename T>
void Sps<ConfigType, NumericalRep>::operator()(std::shared_ptr<T> const& data)
{
    (*this)(*data);
}

} // namespace sps
} // namespace cheetah
} // namespace ska
