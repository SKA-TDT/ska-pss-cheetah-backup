/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/detail/SpsCuda.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <vector>
#include <memory>
#include <iostream>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {


#ifdef ENABLE_ASTROACCELERATE
template<typename DmHandler, typename SpHandler>
void SpsCuda::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , DmHandler& dm_handler
                    , SpHandler& sp_handler
                    )
{
    PUSH_NVTX_RANGE("sps_astroaccelerate_SpsCuda_operator",0);
    PANDA_LOG << "astroaccelerate::SpsCuda::operator() invoked (on device "<< gpu.device_id() << ")";
    /* NOTE: This code is now pointless/wrong. The maxshift it not a good predictor of the minimum
     * processable data length. This requires a new method on ::astroaccelerate::DedispersionStrategy
     */
    if (agg_buf.data_size() < (std::size_t) _dedispersion_strategy->get_maxshift())
    {
        const std::string msg("SpsCuda: data buffer size < maxshift ");
        PANDA_LOG_ERROR << msg << "(" << agg_buf.data_size() << "<" << _dedispersion_strategy->get_maxshift() << ")";
        throw panda::Error(msg);
    }

    auto new_samples = agg_buf.data_size()/ _dedispersion_strategy->get_nchans();

    if(new_samples != _samples)
    {
        // TODO make thread safe (dedispersion_strategy can be changed by another thread)
        PANDA_LOG_WARN << "Aggregation buffer size has changed from "
            << _samples << " to " << new_samples << " samples";
        _dedispersion_strategy->resize(new_samples, _dedispersion_strategy->get_gpu_memory());
        calculate_internals(_dm_trial_metadata->fundamental_sampling_interval());
        _samples = new_samples;
        PANDA_LOG_DEBUG << "SpsCuda will now output "
            << _dedispersion_strategy->get_dedispersed_time_samples()
            << " time samples per call";
    }

    /**
     * Based on the strategy we here create a DmTrials object to be filled by SpsCuda.
     */
    data::DimensionIndex<data::Time> offset_samples(agg_buf.offset_first_block()/_dedispersion_strategy->get_nchans());
    auto const& start_time = agg_buf.composition().front()->start_time(offset_samples);
    PANDA_LOG_DEBUG << "Start time of current buffer: " << start_time;

    /**
     * This is wrong as the start_time is not being updated to take account of the buffer overlap
     */
    auto dmtrials = data::DmTrials<Cpu,float>::make_shared(_dm_trial_metadata, start_time);
    std::vector<float> sps_cands;
    DataType* tf_data = reinterpret_cast<DataType*>(agg_buf.data());

    ::astroaccelerate::AstroAccelerate<void> sps_astroaccelerate(*_dedispersion_strategy);
    PANDA_LOG_DEBUG << "Calling astroaccelerate::AstroAccelerate::run_dedispersion_sps";
    PUSH_NVTX_RANGE("astroaccelerate_run_dedispersion_sps",1);

    sps_astroaccelerate.run_dedispersion_sps(
        gpu.device_id()
        ,tf_data
        ,*_dm_time
        ,sps_cands);

    POP_NVTX_RANGE; //astroaccelerate_run_dedispersion_sps
    PANDA_LOG_DEBUG << "astroaccelerate::AstroAccelerate::run_dedispersion_sps complete";


    //Copy and convert SPS output candidates into SpCandidate instances
    PUSH_NVTX_RANGE("astroaccelerate_cands_to_cheetah_cands",2);
    auto sp_candidate_list = std::make_shared<data::SpCcl<uint8_t>>(agg_buf.composition(), offset_samples);
    sp_candidate_list->reserve(sps_cands.size()/4);
    for (std::size_t idx=0; idx<sps_cands.size(); idx+=4)
    {
        if(sps_cands[idx] == 0 && sps_cands[idx + 1 ]==0 && sps_cands[idx+2] ==0) break;
        sp_candidate_list->emplace_back(
                          sps_cands[idx] * data::parsecs_per_cube_cm
                        , data::SpCcl<uint8_t>::SpCandidateType::MsecTimeType(sps_cands[idx+1] * boost::units::si::seconds) // tstart
                        , data::SpCcl<uint8_t>::SpCandidateType::MsecTimeType(sps_cands[idx+3] * boost::units::si::seconds) // width
                        , sps_cands[idx+2] // sigma
                        );
    }

    POP_NVTX_RANGE; //astroaccelerate_cands_to_cheetah_cands

    PANDA_LOG_DEBUG << "run_dedispersion_sps returned "
        << sp_candidate_list->size() << " candidates";

    //Copy and convert DmTime data into DmTrials object
    PUSH_NVTX_RANGE("astroaccelerate_trials_to_cheetah_trials",3);
    PANDA_LOG_DEBUG << "Copying astroaccelerate::DmTime object contents into DmTrials instance";
    std::size_t true_trial = 0;
    for (std::size_t range=0; range < _dm_time->number_of_dm_ranges(); ++range)
    {
        for (int trial=0; trial < _dedispersion_strategy->get_ndms()[range]; ++trial)
        {
            assert((*dmtrials)[true_trial].size() == _dm_time->nsamples()[range]);
            std::copy((*_dm_time)[range][trial],
                      (*_dm_time)[range][trial]+_dm_time->nsamples()[range],
                      (*dmtrials)[true_trial].begin());
            ++true_trial;
        }
    }
    POP_NVTX_RANGE; // astroaccelerate_trials_to_cheetah_trials
    dm_handler(dmtrials);
    sp_handler(sp_candidate_list);
    PANDA_LOG << "astroaccelerate::SpsCuda::operator() complete";
    POP_NVTX_RANGE; //sps_astroaccelerate_SpsCuda_operator
}
#endif //  ENABLE_ASTROACCELERATE

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
