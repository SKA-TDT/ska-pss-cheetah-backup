/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/astroaccelerate/Sps.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/DmTrials.h"
#include "cheetah/data/SpCcl.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/cuda_utils/nvtx.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <vector>
#include <memory>
#include <algorithm>
#include <iostream>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {


#ifdef ENABLE_ASTROACCELERATE
template<class SpsTraits, typename Enable>
template<typename DmHandler, typename SpHandler, typename OtherBufferType>
void Sps<SpsTraits, Enable>::operator()(panda::PoolResource<cheetah::Cpu>&
                    , OtherBufferType&
                    , DmHandler&
                    , SpHandler&
                    )
{
    throw panda::Error("astroaccelerate::Sps can only handle uint8_t - please deactivate this algorithm in your config");
}

 // --------------- 8bit implemantation ----------------------
template<class SpsTraits>
template<typename DmHandler, typename SpHandler>
void Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::operator()(panda::PoolResource<panda::nvidia::Cuda>& gpu
                    , BufferType& agg_buf
                    , DmHandler& dm_handler
                    , SpHandler& sp_handler
                    )
{
    auto it = this->_cuda_runner.find(gpu.device_id());
    if(it==this->_cuda_runner.end()) {
        TimeFrequencyType const& data=*(agg_buf.composition().front());
        set_dedispersion_strategy(this->_cuda_runner.at(0U).dedispersion_strategy().get_gpu_memory(), data, gpu.device_id());
        it=this->_cuda_runner.find(gpu.device_id());
    }
    (*it).second(gpu, agg_buf, dm_handler, sp_handler);
}

template<class SpsTraits>
template<typename DmHandler, typename SpHandler, typename OtherBufferType>
void Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::operator()(panda::PoolResource<cheetah::Cuda>&
                    , OtherBufferType&
                    , DmHandler&
                    , SpHandler&
                    )
{
    throw panda::Error("astroaccelerate::Sps can only handle uint8_t - please deactivate this algorithm in your config");
}

template<class SpsTraits>
Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::Sps(sps::Config const& config)
    : BaseT(config.astroaccelerate_config(), config)
{
}

template<class SpsTraits>
std::size_t Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::buffer_overlap() const
{
    return _cuda_runner.at(0U).buffer_overlap();
}

template<class SpsTraits>
std::size_t Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data)
{
    _cuda_runner.clear();
    return set_dedispersion_strategy(min_gpu_memory, tf_data, 0); // use 0 as the reference dedispersion object
}

template<class SpsTraits>
std::size_t Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>>::set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const& tf_data, unsigned device_id)
{
    if(_cuda_runner.count(device_id) == 0) {
        _cuda_runner.insert(std::make_pair(device_id, SpsCuda(_algo_config)));
    }
    return _cuda_runner.at(device_id).set_dedispersion_strategy(min_gpu_memory, tf_data);
}


#endif //  ENABLE_ASTROACCELERATE

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska
