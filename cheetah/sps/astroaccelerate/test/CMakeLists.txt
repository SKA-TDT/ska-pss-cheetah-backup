include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(
   gtest_sps_astroaccelerate_src
   src/SpsTest.cpp
   src/gtest_sps_astroaccelerate.cu
)

if(ASTROACCELERATE_FOUND)
    cuda_add_executable(gtest_sps_astroaccelerate ${gtest_sps_astroaccelerate_src})
    target_link_libraries(gtest_sps_astroaccelerate ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
    add_test(gtest_sps_astroaccelerate gtest_sps_astroaccelerate --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
endif(ASTROACCELERATE_FOUND)
