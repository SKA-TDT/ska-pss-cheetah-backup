/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H
#define SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H

#include "cheetah/sps/astroaccelerate/Config.h"
#include "cheetah/sps/Config.h"
#include "cheetah/utils/Mock.h"
#include "cheetah/data/TimeFrequency.h"

#ifdef ENABLE_ASTROACCELERATE

#include "cheetah/sps/astroaccelerate/detail/SpsCuda.h"
#include "cheetah/sps/detail/CommonTypes.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DmTime.h"
#include "panda/arch/nvidia/DeviceCapability.h"
#include <memory>
#include <map>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

/**
 * @brief Single pulse search asynchronous task for CUDA
 * @details note only uint8_t types are currently supported. All others will cause a runtime exception
 */

template<class SpsTraits, typename Enable=void>
class Sps
{
    public:
        typedef cheetah::Cpu Architecture;

    public:
        Sps(sps::Config const&) {};

        template<typename DmHandler, typename SpHandler, typename BufferType>
        void operator()(panda::PoolResource<cheetah::Cpu>&, BufferType&, DmHandler&, SpHandler&);
};


// 8-bit only supported
template<class SpsTraits>
using EnableIfIsUint8T = typename std::enable_if<std::is_same<uint8_t, typename SpsTraits::value_type>::value
                                               ||std::is_same<uint16_t, typename SpsTraits::value_type>::value // ugly hack for uint16_t
                                                >::type;
static_assert(std::is_same<void, EnableIfIsUint8T<sps::CommonTypes<sps::Config, uint8_t>>>::value, "expecting to recognise uint8_t");

template<class SpsTraits>
class Sps<SpsTraits, EnableIfIsUint8T<SpsTraits>> : private utils::AlgorithmBase<Config, sps::Config>
{
    private:
        typedef utils::AlgorithmBase<Config, sps::Config> BaseT;
        typedef sps::CommonTypes<sps::Config, uint8_t> Common;

    public:
        // mark the architecture this algo is designed for
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<3, 5, panda::nvidia::giga> ArchitectureCapability;

    private:
        typedef uint8_t DataType;

    public:
        typedef data::TimeFrequency<Cpu, uint8_t> TimeFrequencyType;

    private:
        typedef typename Common::BufferType BufferType;

    public:
        Sps(sps::Config const& config);
        Sps(Sps&&) = default;
        Sps(Sps const&) = delete;

        /**
         * @brief call the dedispersion/sps algorithm using the provided device
         */
        template<typename DmHandler, typename SpHandler>
        void operator()(panda::PoolResource<cheetah::Cuda>&, BufferType&, DmHandler&, SpHandler&);

        /*
         * catch non uint8_t data
         */
        template<typename DmHandler, typename SpHandler, typename OtherBufferType>
        void operator()(panda::PoolResource<cheetah::Cuda>&, OtherBufferType&, DmHandler&, SpHandler&);

        /**
         * @brief specify parameters to generate a suitable dedispersion compute strategy
         * @returns the minimum sample size required for the specified parameters
         */
        std::size_t set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const&);

        /**
         * @brief the number of time samples required to be copied from the end of the previous buffer
         *        in to the current one
         */
        std::size_t buffer_overlap() const;

    protected:
        std::size_t set_dedispersion_strategy(std::size_t min_gpu_memory, TimeFrequencyType const&, unsigned device_id);

    private:
        std::map<unsigned, SpsCuda> _cuda_runner;
};


} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#else // ENABLE_ASTROACCELERATE

namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {

template<class SpsTraits>
class Sps : public utils::Mock<cheetah::Cuda, sps::Config const&>
{

        typedef data::TimeFrequency<Cpu, uint8_t> TimeFrequencyType;

    public:
        using utils::Mock<cheetah::Cuda, sps::Config const&>::Mock;

        std::size_t set_dedispersion_strategy(std::size_t, TimeFrequencyType const&) { return 0; }
};

} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

#endif // ENABLE_ASTROACCELERATE

#include "cheetah/sps/astroaccelerate/detail/Sps.cpp"
#endif // SKA_CHEETAH_SPS_ASTROACCELERATE_SPS_H
