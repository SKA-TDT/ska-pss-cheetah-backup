/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/test_utils/SpsTester.h"
#include "cheetah/sps/Sps.h"
#include "cheetah/data/RfimFlaggedData.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/DispersedPulse.h"
#include "cheetah/generators/RfiScenario.h"
//#include "pss/astrotypes/sigproc/SigProc.h"

namespace ska {
namespace cheetah {
namespace sps {
namespace test {


template<int ScenarioNum, typename TimeFrequencyType>
class TestRfiData: public data::RfimFlaggedData<TimeFrequencyType>
{
    private:
        typedef data::RfimFlaggedData<TimeFrequencyType> BaseT;

    public:
        TestRfiData(TimeFrequencyType& data);
        ~TestRfiData();

    private:
        generators::RfiScenario<ScenarioNum, typename TimeFrequencyType::NumericalRep> _generator;

};

template<int ScenarioNum, typename TimeFrequencyType>
TestRfiData<ScenarioNum, TimeFrequencyType>::TestRfiData(TimeFrequencyType& data)
    : BaseT(data)
{
    _generator.next(*this);
}

template<int ScenarioNum, typename TimeFrequencyType>
TestRfiData<ScenarioNum, TimeFrequencyType>::~TestRfiData()
{
}

template<typename RfiDataType, typename TraitsType, typename PoolType>
void test_single_pulse_with_noise(TraitsType& traits, PoolType& pool)
{
    auto& api = traits.api(pool);
    auto& sps_config = traits.config();
    ddtr::DedispersionConfig dedispersion_config;
    typedef typename ddtr::DedispersionConfig::Dm Dm;
    dedispersion_config.dm_start(Dm(0 * pss::astrotypes::units::parsecs_per_cube_cm));
    dedispersion_config.dm_end(Dm(800 * pss::astrotypes::units::parsecs_per_cube_cm));
    dedispersion_config.dm_step(Dm(100 * pss::astrotypes::units::parsecs_per_cube_cm));
    sps_config.dedispersion_config(dedispersion_config);

    typedef sps::Sps<sps::Config, uint8_t> SpsType;
    typedef typename SpsType::TimeFrequencyType DataType;
    pss::astrotypes::DimensionSize<pss::astrotypes::units::Time> chunk_samples(10000);
    pss::astrotypes::DimensionSize<pss::astrotypes::units::Frequency> number_of_channels(64);
    std::shared_ptr<DataType> data = DataType::make_shared(chunk_samples, number_of_channels);
    // generate random noise data and pass to Sps
    // until we get a callback
    generators::DispersedPulse<typename DataType::DataType> generator;
    typedef sps::Sps<sps::Config, uint8_t> SpsType;
    typedef typename SpsType::TimeFrequencyType DataType;

    // keep sending data until we get a callback
    //std::ofstream ofile("single_pulse.fil", std::ofstream::binary);
    //pss::astrotypes::sigproc::Header sigproc_header;
    //sigproc_header.number_of_channels(number_of_channels);
    //sigproc_header.number_of_bits(8);
    //sigproc_header.fch1(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz));
    //sigproc_header.foff(data::FrequencyType(-1.0 * boost::units::si::mega * data::hz));
    //sigproc_header.sample_interval(boost::units::quantity<pss::astrotypes::units::Seconds, double>(0.5 * boost::units::si::milli * boost::units::si::seconds));
    //ofile << sigproc_header;
    //pss::astrotypes::sigproc::SigProcFormat<pss::astrotypes::units::Time, pss::astrotypes::units::Frequency> sigproc_formatter;
    std::size_t count=0;
    while(!traits.dm_handler_called())
    {
        auto data = DataType::make_shared(chunk_samples, number_of_channels);
        data->sample_interval(boost::units::quantity<pss::astrotypes::units::Seconds, double>(0.5 * boost::units::si::milli * boost::units::si::seconds));
        data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-1.0 * boost::units::si::mega * data::hz));
        std::fill(data->begin(), data->end(), 0);

        generator.next(*data);
        RfiDataType Rfidata(*data);
        api(*data);
        //ofile << sigproc_formatter << *data;
        ++count;
    }
    //ofile.close();
    ASSERT_TRUE(traits.wait_sp_handler_called());
    auto sp_data_ptr = traits.sp_data();
    auto const& sp_data = *sp_data_ptr;

    ASSERT_GE(sp_data.size(), 1U);
}


template <typename TestTraits>
SpsTester<TestTraits>::SpsTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
SpsTester<TestTraits>::~SpsTester()
{
}

template<typename TestTraits>
void SpsTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void SpsTester<TestTraits>::TearDown()
{
}


template<typename ArchitectureTag, typename ArchitectureCapability>
SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::SpsTesterTraits()
    : _dm_called(false)
    , _sp_called(false)
    , _sp_call_count(0u)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
typename SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::Api& SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::api(PoolType& pool)
{
    if(!_api) {
        configure(_config); // call configuration method
        _config.set_pool(pool);
        _config.set_dedispersion_samples(1<<16);
        _api.reset(new Api(_config,
                     [this](std::shared_ptr<DmType> data)
                     {
                        _dm_called = true;
                        _dm_data = data;
                     },
                     [this](std::shared_ptr<SpType> data)
                     {
                        try {
                            std::lock_guard<std::mutex> lock(_sp_data_mutex);
                            {
                                _sp_called = true;
                                ++_sp_call_count;
                                _sp_data.push_back(data);
                            }
                        } catch(...) {}
                        _sp_wait.notify_all();
                     }));
    }
    return *_api;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
sps::Config& SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::config()
{
    return _config;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
bool SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::dm_handler_called() const
{
    return _dm_called;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
bool SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::sp_handler_called() const
{
    return _sp_called;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
bool SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::wait_sp_handler_called() const
{
    std::unique_lock<std::mutex> lock(_sp_data_mutex);
    _sp_wait.wait_for(lock, std::chrono::seconds(5), [this] { return _sp_called; });
    return _sp_called;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
std::shared_ptr<typename SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::SpType> SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::sp_data() const
{
    std::lock_guard<std::mutex> lock(_sp_data_mutex);
    auto data_ptr = _sp_data.front();
    _sp_data.pop_front();
    return data_ptr;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
std::size_t SpsTesterTraits<ArchitectureTag, ArchitectureCapability>::sp_handler_call_count() const
{
    return _sp_call_count;
}

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, test_handlers)
{
    TypeParam traits;
    auto& api = traits.api(pool);

    // generate random noise data and pass to Sps
    // until we get a callback
    generators::GaussianNoiseConfig generator_config;
    generators::GaussianNoise<uint8_t> generator(generator_config);
    typedef sps::Sps<sps::Config, uint8_t> SpsType;
    typedef typename SpsType::TimeFrequencyType DataType;

    // keep sending data until we get a callback
    std::size_t count=0;
    std::size_t chunk_samples=10000;
    while(!traits.sp_handler_called())
    {
        auto data = DataType::make_shared(data::DimensionSize<data::Time>(chunk_samples), data::DimensionSize<data::Frequency>(64));
        data->sample_interval(DataType::TimeType(15.0 * boost::units::si::milli * boost::units::si::seconds));
        data->set_channel_frequencies_const_width(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz), data::FrequencyType(-1.0 * boost::units::si::mega * data::hz));
        generator.next(*data);
        api(*data);
        ++count;
    }
    ASSERT_TRUE(traits.dm_handler_called());

    // now we expec the buffer to flush data as it is destroyed. so the pool has to stay alive whilst this happns
}

/*
POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_without_noise)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<0, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}
*/

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_with_noise_1)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<1, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_with_noise_2)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<2, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_with_noise_3)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<3, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_with_noise_4)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<4, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}

POOL_ALGORITHM_TYPED_TEST_P(SpsTester, single_pulse_with_noise_5)
{
    TypeParam traits;
    test_single_pulse_with_noise<TestRfiData<5, data::TimeFrequency<Cpu, uint8_t>>>(traits, pool);
}

/*
POOL_ALGORITHM_TYPED_TEST_P(SpsTester, test_single_pulse_without_noise)
{
    TypeParam traits;
    auto& api = traits.api(pool);
    auto& sps_config = traits.config();

    ddtr::DedispersionConfig dedispersion_config;
    typedef typename ddtr::DedispersionConfig::Dm Dm;
    dedispersion_config.dm_start(Dm(0 * pss::astrotypes::units::parsecs_per_cube_cm));
    dedispersion_config.dm_end(Dm(800 * pss::astrotypes::units::parsecs_per_cube_cm));
    dedispersion_config.dm_step(Dm(100 * pss::astrotypes::units::parsecs_per_cube_cm));
    sps_config.dedispersion_config(dedispersion_config);

    typedef sps::Sps<sps::Config, uint8_t> SpsType;
    typedef typename SpsType::TimeFrequencyType DataType;
    pss::astrotypes::DimensionSize<pss::astrotypes::units::Time> chunk_samples(10000);
    pss::astrotypes::DimensionSize<pss::astrotypes::units::Frequency> number_of_channels(64);
    std::shared_ptr<DataType> data = DataType::make_shared(chunk_samples, number_of_channels);
    // generate random noise data and pass to Sps
    // until we get a callback
    generators::DispersedPulse<typename DataType::DataType> generator;
    typedef sps::Sps<sps::Config, uint8_t> SpsType;
    typedef typename SpsType::TimeFrequencyType DataType;

    // keep sending data until we get a callback
    //std::ofstream ofile("single_pulse.fil", std::ofstream::binary);
    pss::astrotypes::sigproc::Header sigproc_header;
    sigproc_header.number_of_channels(number_of_channels);
    sigproc_header.number_of_bits(8);
    sigproc_header.fch1(data::FrequencyType(1000.0 * boost::units::si::mega * data::hz));
    sigproc_header.foff(data::FrequencyType(-1.0 * boost::units::si::mega * data::hz));
    sigproc_header.sample_interval(boost::units::quantity<pss::astrotypes::units::Seconds, double>(0.5 * boost::units::si::milli * boost::units::si::seconds));
    //ofile << sigproc_header;
    //pss::astrotypes::sigproc::SigProcFormat<pss::astrotypes::units::Time, pss::astrotypes::units::Frequency> sigproc_formatter;
    std::size_t count=0;
    while(!traits.dm_handler_called())
    {
        auto data = DataType::make_shared(chunk_samples, number_of_channels);
        data->sample_interval(sigproc_header.sample_interval());
        data->set_channel_frequencies_const_width(*sigproc_header.fch1(), *sigproc_header.foff());
        std::fill(data->begin(), data->end(), 0);

        generator.next(*data);
        api(*data);
        //ofile << sigproc_formatter << *data;
        ++count;
    }
    //ofile.close();
    ASSERT_TRUE(traits.sp_handler_called());
    auto const& sp_data = *traits.sp_data();

    ASSERT_GE(sp_data.size(), 1U); // there is only one candidate but we should expect many more potentials
}
// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
*/
REGISTER_TYPED_TEST_CASE_P(SpsTester, test_handlers, single_pulse_with_noise_1, single_pulse_with_noise_2, single_pulse_with_noise_3, single_pulse_with_noise_4, single_pulse_with_noise_5);

} // namespace test
} // namespace sps
} // namespace cheetah
} // namespace ska
