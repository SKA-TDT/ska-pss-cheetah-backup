/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_SPS_H
#define SKA_CHEETAH_SPS_SPS_H

#include "cheetah/sps/Config.h"
#include "cheetah/sps/detail/SpsTask.h"
#include "cheetah/sps/detail/CommonTypes.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/RfimFlaggedData.h"
#include "cheetah/sps/detail/RfiExcisionFactory.h"
#include "panda/AlgorithmTuple.h"
#include "panda/AggregationBufferFiller.h"
#include <functional>
#include <memory>

namespace ska {
namespace cheetah {
namespace sps {

/*
template<typename ConfigType, typename NumericalT>
using SpsAlgos=ddtr::DdtrModule<CommonTypes<ConfigType, NumericalT>
                               , emulator::Sps
#ifdef ENABLE_ASTROACCELERATE
                               , astroaccelerate::Sps
#endif // ENABLE_ASTROACCELERATE
                               >;
*/

/**
 * @brief
 *     Single Pulse Search top level interface
 *
 * @details
 *
 */

template<class ConfigType, typename NumericalT>
class Sps
{
        typedef CommonTypes<ConfigType, NumericalT> CommonTraits;


    public:
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef typename CommonTraits::DmTrialsType DmTrialType;
        typedef typename CommonTraits::SpType SpType;
        typedef typename CommonTraits::BufferFillerType BufferFillerType;

        typedef std::function<void(std::shared_ptr<DmTrialType>)> DmHandler;
        typedef std::function<void(std::shared_ptr<SpType>)> SpHandler;

    private:
        typedef typename ConfigType::PoolType PoolType;
        template<typename... Algos>
        using AlgoLauncher = SpecificSpsTask<DmHandler, SpHandler, CommonTraits, PoolType, Algos...>;

    public:
        /**
         * @brief Constructor takes two Handlers
         * @details
         * @param DmHandler A functor to be called with the DmTime data product
         * @param SpHandler A functor to be called with the Sps results
         */
        Sps(ConfigType const&, DmHandler dm_handler, SpHandler sm_handler);
        ~Sps();

        /**
         * @brief
         *     Call to activate the Dedispersion and Single Pulse Search
         *
         * @details
         *     Takes the next block of TimeFrequency data
         *     and returns immediately. When enough data is buffered
         *     it will launch an async task calling the handlers
         *     when done.
         */
        void operator()(TimeFrequencyType&);
        void operator()(data::RfimFlaggedData<TimeFrequencyType>&);

        template<typename T>
        void operator()(std::shared_ptr<T> const&);

    private:
        void agg_buffer_init(TimeFrequencyType const&);

        template<typename DataT>
        void agg_buffer_fill(DataT&);

        PoolType _pool;

        typedef SpsTask<DmHandler, SpHandler, CommonTraits> TaskType;
        std::shared_ptr<TaskType> _task_ptr;

        std::size_t _dedisp_samples;
        BufferFillerType _agg_buf_filler;

        std::size_t _current_number_of_channels;
        std::size_t _algo_count;
        ConfigType const& _config;
};

} // namespace sps
} // namespace cheetah
} // namespace ska
#include "cheetah/sps/detail/Sps.cpp"

#endif // SKA_CHEETAH_SPS_SPS_H
