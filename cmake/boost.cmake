if(PANDA_CMAKE_MODULE_PATH)
    include("${PANDA_CMAKE_MODULE_PATH}/boost.cmake")
else()
    message(FATAL_ERROR "Dependency Error: include boost.cmake after panda has been found")
endif()

# add extra boost components
find_package(Boost COMPONENTS filesystem system program_options REQUIRED)
if(Boost_MAJOR_VERSION EQUAL 1 AND Boost_MINOR_VERSION LESS 58)
    # -- pick up boost::endian from the local copy
    list(APPEND Boost_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/thirdparty/boost")
endif()
if(Boost_MAJOR_VERSION EQUAL 1 AND Boost_MINOR_VERSION LESS 54)
    # -- pick up boost::geometry from local copy from version 1.55
    list(INSERT Boost_INCLUDE_DIR 0 "${PROJECT_SOURCE_DIR}/thirdparty/boost/1.55")
endif()
if(Boost_MAJOR_VERSION EQUAL 1 AND Boost_MINOR_VERSION LESS 56)
    # -- pick up boost::align from the local copy
    list(APPEND Boost_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/thirdparty/boost/1.56")
endif()
