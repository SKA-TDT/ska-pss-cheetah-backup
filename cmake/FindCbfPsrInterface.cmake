# Find the CBF Low packet includes
#
#  The following variables can be set to explicitly specify the locations of astrotypes components in order of precedence
#  CBF_PSR_INTERFACE_INCLUDE_DIR - where to find astrotypes includes


# -- search for installed libs

macro(SEARCH_CBF_PSR_INTERFACE_HEADERS)

    INCLUDE(FindPackageHandleCompat)

    FIND_PATH(CBF_PSR_INTERFACE_INCLUDE_DIR "ska/cbf_psr_interface/CbfPsrHeader.h"
                      PATHS ${THIRDPARTY_DIR}/cbf-psr-interface/src/)

    if(CBF_PSR_INTERFACE_INCLUDE_DIR)
        message("Found CBF_PSR_INTERFACE_INCLUDE_DIR : ${CBF_PSR_INTERFACE_INCLUDE_DIR}")
        SET(CBF_PSR_INTERFACE_HEADERS_FOUND true)
    endif()

endmacro(SEARCH_CBF_PSR_INTERFACE_HEADERS)


if(NOT CBF_PSR_INTERFACE_HEADERS_FOUND) # If no astrotypes install is found, use the included third-party astrotypes
    #message(STATUS "Using thirdparty astrotypes lib. Building astrotypes ....")
    SEARCH_CBF_PSR_INTERFACE_HEADERS()
endif()


# Handle components
# I HAVE to supply a required variable - so give it one. All other required variables are dealt with above.
# This call to FPHSV is just to correctly handle the components
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LowPacketHeaders FOUND_VAR LowPacketHeaders_FOUND REQUIRED_VARS CBF_PSR_INTERFACE_INCLUDE_DIR)
