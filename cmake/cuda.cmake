#---------------------------------------------------------
# CUDA development toolkit and configuration
#
# Interface Variables:
# CUDA_NVCC_FLAGS  : additional flags to send to the nvcc compiler
#---------------------------------------------------------
if(PANDA_CMAKE_MODULE_PATH)
    include("${PANDA_CMAKE_MODULE_PATH}/cuda.cmake")
else()
    message(FATAL_ERROR "Dependency Error: include cuda.cmake after panda has been found")
endif()

if(ENABLE_CUDA)
  include_directories(${CUDA_TOOLKIT_INCLUDE})

  # add sdk samples useful headerfiles like cuda_helpers.h
  if(CUDA_SMP_INC)
	include_directories(${CUDA_SMP_INC})
  endif(CUDA_SMP_INC)
  if(CUDA_VERSION_MAJOR EQUAL "11")
    add_definitions(-DTHRUST_IGNORE_DEPRECATED_CPP_DIALECT)
  endif()
  # Pass options to NVCC ( -ccbin /path  --compiler-options -lfftw3f --compiler-options -lm --verbose)
  if(CUDA_VERSION_MAJOR GREATER "9")
   list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_75,code=sm_75) # RTX2080 (CUDA >=10)
  endif()
  if(CUDA_VERSION_MAJOR GREATER "8" AND CUDA_VERSION_MAJOR LESS "11")
    list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_70,code=sm_70) # V100 (CUDA >=9)
  endif()
  if(CUDA_VERSION_MAJOR GREATER "7" AND CUDA_VERSION_MAJOR LESS "11")
    list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_61,code=sm_61) # GTX1080 (CUDA >=8)
  endif()
  list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_60) # P100
  list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_52,code=sm_52) # TitanX
  list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_50,code=sm_50) # Maxwell
  #list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_37,code=sm_37) # K80
   list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_35,code=sm_35) # K40
  #list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_30,code=sm_30) # GT-730

  option(CUDA_USE_NVTX "set to true to enable NVTX macros for CUDA profiling" OFF)
  if(CUDA_USE_NVTX)
    set(CUDA_USE_NVTX true)
    add_definitions(-DUSE_NVTX)
    find_library(CUDA_NVTOOLSEXT_LIB
      NAMES nvToolsExt
      HINTS ${CUDA_TOOLKIT_ROOT_DIR}/lib64)
    list(APPEND CUDA_DEPENDENCY_LIBRARIES ${CUDA_NVTOOLSEXT_LIB})
  else(CUDA_USE_NVTX)
    set(CUDA_USE_NVTX false)
  endif(CUDA_USE_NVTX)

endif(ENABLE_CUDA)
